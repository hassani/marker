!
! ifort write_pfile.f90 -o write_pfile
!
use, intrinsic :: iso_fortran_env, i32 => int32 , i64 => int64 , &
                                   rSP => real32, rDP => real64
implicit none

integer, parameter :: ua(4)=[11, 12, 13, 14],  ub(4)=[21, 22, 23, 24],  &
                      szi(4)=[i32,i32,i64,i64], szr(4)=[rSp,rDp,rSp,rDp]
                      
character(len=*), parameter :: fa(4) = ['pa32sp','pa32dp','pa64sp','pa64dp']
character(len=*), parameter :: fb(4) = ['pb32sp','pb32dp','pb64sp','pb64dp']

integer, parameter :: nDime = 3, nCell = 6, nNode = 8, nstep = 10, &
                      nFacet = 12, nEdge = 12, &
                      sizeCells = 24, sizeFacets = 36, sizeEdges = 24
                      
integer :: cells(sizeCells), facets(sizeFacets), edges(sizeEdges), &
           domId(nCell), faceId(nFacet), lineId(nEdge)
           
real    :: X(nDime,nNode), V(nDime,nNode), T(nNode)
real    :: S(6,nCell), P(nCell)

integer :: i, istep, itime = 0
real    :: time = 0.0, dtime = 1.0, dv = 2e-6, ds = .1e10, dp=.1e10, dT=5, dx=0.05

real   , dimension(nstep) :: r
logical, dimension(nstep) :: random_desc1, random_desc2, random_desc3, random_desc4
logical                   :: random

cells = [5, 7, 8, 1,   7, 8, 1, 3,   7, 2, 1, 6,   7, 2, 3, 1,    5, 7, 1, 6,    8, 4, 1, 3]
domId(:) = 1
facets = [4, 1, 3,  3, 1, 2,  8, 7, 5,  7, 6, 5,  1, 4, 8,  5, 1, 8,  2, 1, 6,  &
          1, 5, 6,  2, 7, 3,  6, 7, 2,  3, 8, 4,  7, 8, 3]
faceId = [1,1,  2, 2,  3,3,  4,4,  5,5,  6,6]
edges = [1, 2,  2, 3,  3, 4,  4, 1,  5, 6,  6, 7,  7, 8,  8, 5,  1, 5,  2, 6,  3, 7,  4, 8] 
lineId = [(i,i=1,nEdge)]

X(:,1) = [0.,  0.,  0.]
X(:,2) = [1.,  0.,  0.]
X(:,3) = [1.,  0.,  1.]
X(:,4) = [0.,  0.,  1.]  
X(:,5) = [0.,  1.,  0.]  
X(:,6) = [1.,  1.,  0.]  
X(:,7) = [1.,  1.,  1.]  
X(:,8) = [0.,  1.,  1.]

V(:,:) = 0.0
V(1,3) = 2e-6 ; V(1,4) = 2e-6; V(1,7) = 2e-6; V(1,8) = 2e-6
T(:) = [(i,i=1,nNode)]*100.0
P(:) = [(i,i=1,nCell)]*1e10
S(:,:) = 0.0
S(6,:) = 1.2e10

!random = .true.
random = .false.

if ( random ) then
   call random_number(r) ; random_desc1 = r <= 0.5  
   call random_number(r) ; random_desc2 = r <= 0.5 
   call random_number(r) ; random_desc3 = r <= 0.5  
   call random_number(r) ; random_desc4 = r <= 0.5  
else
   random_desc1 = .true.
   random_desc2 = .true.
   random_desc3 = .true.
   random_desc4 = .true.
end if

random_desc1(1) = .false.
random_desc2(1) = .false.
random_desc3(1) = .false.
random_desc4(1) = .false.

call openFiles()

do istep = 1, nstep

   ! begin a new step
   call writeBeginStep ()
   
   ! write 2 constant values (time and itime)
   call writeTime ( same_desc = random_desc1(istep) )
   
   ! write mesh
   call writeMesh ( same_mesh = random_desc2(istep) )

   ! write node coordinates
   call writeNodalCoord ( same_coord = .false. )
      
   ! write nodal values
   call writeNodalValues ( same_desc = random_desc3(istep) )

   ! write cell values
   call writeElementValues ( same_desc = random_desc4(istep) )
      
   ! end step
   call writeEndStep ()

   call simulNewStep()

end do

call closeFiles()

contains

!=============================================================================================   
   subroutine simulNewStep 
!=============================================================================================  

   time = time + dtime
   itime = itime + 1
   X(1,3) = X(1,3)+dx ; X(1,4) = X(1,4)+dx; X(1,7)=X(1,7)+dx; X(1,8)=X(1,8)+dx
   v(1,3) = v(1,3)+dv ; v(1,4) = v(1,4)+dv; v(1,7)=v(1,7)+dv; v(1,8)=v(1,8)+dv
   P = P + dp
   S(6,:) = S(6,:) + ds
   T = T + dT
   
   end subroutine simulNewStep 

!=============================================================================================   
   subroutine writeNodalValues ( same_desc )
!=============================================================================================  
   logical, intent(in) :: same_desc
   
   character(len=:), allocatable :: name
   integer                       :: i

   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ub(i))' '
      if ( same_desc ) then 
         write(ua(i),'(a)')'$Fields, geomEntities = nodes, nField = 2, desc = no'
         write(ub(i))      '$Fields, geomEntities = nodes, nField = 2, desc = no'
      else
         write(ua(i),'(a)')'$Fields, geomEntities = nodes, nField = 2,  nGeomEntities ='//&
                            i2a(nNode)
         write(ub(i))      '$Fields, geomEntities = nodes, nField = 2,  nGeomEntities ='//&
                            i2a(nNode)
         name = 'V'//i2a(istep)
         write(ua(i),'(a)')'name='//name//',    physicalQuantity=velocity,    unit=m/s, nComp=3'
         write(ub(i))      'name='//name//',    physicalQuantity=velocity,    unit=m/s, nComp=3'
         name = 'T'//i2a(istep)
         write(ua(i),'(a)')'name='//name//',    physicalQuantity=temperature, unit=C,   nComp=1'
         write(ub(i))      'name='//name//',    physicalQuantity=temperature, unit=C,   nComp=1'
      end if
           
   end do  
   write(ua(1),'(*(g0,1x))')real(V,rSp), real(T,rSp)
   write(ua(2),'(*(g0,1x))')real(V,rDp), real(T,rDp)
   write(ua(3),'(*(g0,1x))')real(V,rSp), real(T,rSp)
   write(ua(4),'(*(g0,1x))')real(V,rDp), real(T,rDp)

   write(ub(1))real(V,rSp), real(T,rSp)
   write(ub(2))real(V,rDp), real(T,rDp)
   write(ub(3))real(V,rSp), real(T,rSp)
   write(ub(4))real(V,rDp), real(T,rDp)
  
   end subroutine writeNodalValues

!=============================================================================================   
   subroutine writeElementValues ( same_desc )
!=============================================================================================  
   logical, intent(in) :: same_desc

   character(len=:), allocatable :: name
   integer                       :: i   

   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ub(i))' '

      if ( same_desc ) then 
         write(ua(i),'(a)')'$Fields, geomEntities = cells, nField = 2, desc = no'
         write(ub(i))      '$Fields, geomEntities = cells, nField = 2, desc = no'
      else
         write(ua(i),'(a)')'$Fields, geomEntities = cells, nField = 2,  nGeomEntities ='//&
                            i2a(nCell)
         write(ub(i))      '$Fields, geomEntities = cells, nField = 2,  nGeomEntities ='//&
                            i2a(nCell)
         name = 'S'//i2a(istep)
         write(ua(i),'(a)')'name='//name//',    physicalQuantity=stress,    unit=Pa, nComp=6'
         write(ub(i))      'name='//name//',    physicalQuantity=stress,    unit=Pa, nComp=6'
         name = 'p'//i2a(istep)
         write(ua(i),'(a)')'name='//name//',    physicalQuantity=pressure, unit=Pa,   nComp=1'
         write(ub(i))      'name='//name//',    physicalQuantity=pressure, unit=Pa,   nComp=1'
      end if
           
   end do  
   write(ua(1),'(*(g0,1x))')real(S,rSp), real(P,rSp)
   write(ua(2),'(*(g0,1x))')real(S,rDp), real(P,rDp)
   write(ua(3),'(*(g0,1x))')real(S,rSp), real(P,rSp)
   write(ua(4),'(*(g0,1x))')real(S,rDp), real(P,rDp)

   write(ub(1))real(S,rSp), real(P,rSp)
   write(ub(2))real(S,rDp), real(P,rDp)
   write(ub(3))real(S,rSp), real(P,rSp)
   write(ub(4))real(S,rDp), real(P,rDp)
   
   end subroutine writeElementValues   
   
!=============================================================================================   
   subroutine writeNodalCoord ( same_coord )
!=============================================================================================  
   logical, intent(in) :: same_coord
   
   integer :: i
   
   if ( same_coord ) return
   
   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Mesh:Coordinates, nDime = '//i2a(nDime)// &
                        ', nNode ='//i2a(nNode)//', unit = m'
      write(ub(i))' '
      write(ub(i))     '$Mesh:Coordinates, nDime = '//i2a(nDime)// &
                       ', nNode ='//i2a(nNode)//', unit = m'
   end do  
   
   write(ua(1),'(*(g0,1x))')real(X,rSp)
   write(ua(2),'(*(g0,1x))')real(X,rDp)
   write(ua(3),'(*(g0,1x))')real(X,rSp)
   write(ua(4),'(*(g0,1x))')real(X,rDp)

   write(ub(1))real(X,rSp)
   write(ub(2))real(X,rDp)
   write(ub(3))real(X,rSp)
   write(ub(4))real(X,rDp)

   end subroutine writeNodalCoord
   
   
!=============================================================================================   
   subroutine writeMesh ( same_mesh )
!=============================================================================================  
   logical, intent(in) :: same_mesh
   
   integer :: i, code

   if ( same_mesh ) return

!
!- Cells:
!  
   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Mesh:Connectivity, geomEntities=cells, nGeomEntities=' // &
                         i2a(nCell)//',size ='//i2a(sizeCells)
      write(ub(i))' '
      write(ub(i))      '$Mesh:Connectivity, geomEntities=cells, nGeomEntities='// &
                         i2a(nCell)//',size ='//i2a(sizeCells)
   end do  
   
   write(ua(1),'(*(g0,1x))')int(cells,i32)
   write(ua(2),'(*(g0,1x))')int(cells,i32)
   write(ua(3),'(*(g0,1x))')int(cells,i64)
   write(ua(4),'(*(g0,1x))')int(cells,i64)
   
   write(ub(1))int(cells,i32)
   write(ub(2))int(cells,i32)
   write(ub(3))int(cells,i64)
   write(ub(4))int(cells,i64)

   code = 10
   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Mesh:VtkTypeId, geomEntities=cells, nGeomEntities=' // &
                         i2a(nCell)//',size =1'
      write(ub(i))' '
      write(ub(i))      '$Mesh:VtkTypeId, geomEntities=cells, nGeomEntities='// &
                         i2a(nCell)//',size =1'
   end do  
   write(ua(1),'(*(g0,1x))')int(code,i32)
   write(ua(2),'(*(g0,1x))')int(code,i32)
   write(ua(3),'(*(g0,1x))')int(code,i64)
   write(ua(4),'(*(g0,1x))')int(code,i64)
   
   write(ub(1))int(code,i32)
   write(ub(2))int(code,i32)
   write(ub(3))int(code,i64)
   write(ub(4))int(code,i64)     

   code = 1
   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Mesh:DomainId, geomEntities=cells, nGeomEntities=' // &
                         i2a(nCell)//',size =1'
      write(ub(i))' '
      write(ub(i))      '$Mesh:DomainId, geomEntities=cells, nGeomEntities='// &
                         i2a(nCell)//',size =1'
   end do  
   write(ua(1),'(*(g0,1x))')int(code,i32)
   write(ua(2),'(*(g0,1x))')int(code,i32)
   write(ua(3),'(*(g0,1x))')int(code,i64)
   write(ua(4),'(*(g0,1x))')int(code,i64)
   
   write(ub(1))int(code,i32)
   write(ub(2))int(code,i32)
   write(ub(3))int(code,i64)
   write(ub(4))int(code,i64)     
!
!- Facets:
!          
   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Mesh:Connectivity, geomEntities=facets, nGeomEntities=' // &
                         i2a(nFacet)//',size ='//i2a(sizeFacets)   
      write(ub(i))' '
      write(ub(i))      '$Mesh:Connectivity, geomEntities=facets, nGeomEntities='// &
                         i2a(nFacet)//',size ='//i2a(sizeFacets)
   end do  
   
   write(ua(1),'(*(g0,1x))')int(facets,i32)
   write(ua(2),'(*(g0,1x))')int(facets,i32)
   write(ua(3),'(*(g0,1x))')int(facets,i64)
   write(ua(4),'(*(g0,1x))')int(facets,i64)
   
   write(ub(1))int(facets,i32)
   write(ub(2))int(facets,i32)
   write(ub(3))int(facets,i64)   
   write(ub(4))int(facets,i64)

   code = 5
   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Mesh:VtkTypeId, geomEntities=facets, nGeomEntities='// &
                         i2a(nFacet)//',size =1'
      write(ub(i))' '
      write(ub(i))      '$Mesh:VtkTypeId, geomEntities=facets, nGeomEntities='// &
                         i2a(nFacet)//',size =1'
   end do  
   write(ua(1),'(*(g0,1x))')int(code,i32)
   write(ua(2),'(*(g0,1x))')int(code,i32)
   write(ua(3),'(*(g0,1x))')int(code,i64)
   write(ua(4),'(*(g0,1x))')int(code,i64)
   
   write(ub(1))int(code,i32)
   write(ub(2))int(code,i32)
   write(ub(3))int(code,i64)
   write(ub(4))int(code,i64)     

   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Mesh:DomainId, geomEntities=facets, nGeomEntities='//i2a(nFacet)
      write(ub(i))' '
      write(ub(i))      '$Mesh:DomainId, geomEntities=facets, nGeomEntities='//i2a(nFacet)
   end do  
   write(ua(1),'(*(g0,1x))')int(faceId,i32)
   write(ua(2),'(*(g0,1x))')int(faceId,i32)
   write(ua(3),'(*(g0,1x))')int(faceId,i64)
   write(ua(4),'(*(g0,1x))')int(faceId,i64)
   write(ub(1))int(faceId,i32)
   write(ub(2))int(faceId,i32)
   write(ub(3))int(faceId,i64)
   write(ub(4))int(faceId,i64)
!
!- Edges:
!   
   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Mesh:Connectivity, geomEntities=edges, nGeomEntities='// &
                         i2a(nEdge)//', size = '//i2a(sizeEdges)
      write(ub(i))' '
      write(ub(i))      '$Mesh:Connectivity, geomEntities=edges, nGeomEntities='// &
                         i2a(nEdge)//', size = '//i2a(sizeEdges)
   end do  
   
   write(ua(1),'(*(g0,1x))')int(edges,i32)
   write(ua(2),'(*(g0,1x))')int(edges,i32)
   write(ua(3),'(*(g0,1x))')int(edges,i64)
   write(ua(4),'(*(g0,1x))')int(edges,i64)
   
   write(ub(1))int(edges,i32)
   write(ub(2))int(edges,i32)
   write(ub(3))int(edges,i64)
   write(ub(4))int(edges,i64)
   
   code = 3
   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Mesh:VtkTypeId, geomEntities=edges, nGeomEntities='// &
                         i2a(nFacet)//',size =1'
      write(ub(i))' '
      write(ub(i))      '$Mesh:VtkTypeId, geomEntities=edges, nGeomEntities='// &
                         i2a(nFacet)//',size =1'
   end do  
   write(ua(1),'(*(g0,1x))')int(code,i32)
   write(ua(2),'(*(g0,1x))')int(code,i32)
   write(ua(3),'(*(g0,1x))')int(code,i64)
   write(ua(4),'(*(g0,1x))')int(code,i64)
   
   write(ub(1))int(code,i32)
   write(ub(2))int(code,i32)
   write(ub(3))int(code,i64)
   write(ub(4))int(code,i64)    

   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Mesh:DomainId, geomEntities=edges, nGeomEntities='//i2a(nFacet)
      write(ub(i))' '
      write(ub(i))      '$Mesh:DomainId, geomEntities=edges, nGeomEntities='//i2a(nFacet)
   end do  
       
   write(ua(1),'(*(g0,1x))')int(lineId,i32) 
   write(ua(2),'(*(g0,1x))')int(lineId,i32)
   write(ua(3),'(*(g0,1x))')int(lineId,i64)
   write(ua(4),'(*(g0,1x))')int(lineId,i64)
   
   write(ub(1))int(lineId,i32)
   write(ub(2))int(lineId,i32)
   write(ub(3))int(lineId,i64)
   write(ub(4))int(lineId,i64) 
     
   end subroutine writeMesh


!=============================================================================================   
   subroutine writeBeginStep ( )
!=============================================================================================  

   integer :: i

   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$Begin_step, num='//i2a(istep)
      write(ub(i))' '
      write(ub(i))      '$Begin_step, num='//i2a(istep)
   end do  
   
   end subroutine writeBeginStep

!=============================================================================================   
   subroutine writeEndStep ( )
!=============================================================================================  

   integer :: i

   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$End_step, num='//i2a(istep)
      write(ub(i))' '
      write(ub(i))      '$End_step, num='//i2a(istep)
   end do  
   
   end subroutine writeEndStep
      
!=============================================================================================   
   subroutine writeTime ( same_desc )
!=============================================================================================  
   logical, intent(in) :: same_desc

   character(len=:), allocatable :: name
   integer                       :: i
   
   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ub(i))' '
      if ( same_desc ) then
         write(ua(i),'(a)')'$Fields, geomEntities = -, nField = 2, desc = no'
         write(ub(i))      '$Fields, geomEntities = -, nField = 2, desc = no'
      else
         write(ua(i),'(a)')'$Fields, geomEntities = -, nField = 2'
         write(ub(i))      '$Fields, geomEntities = -, nField =2'
         name = 'time'//i2a(istep)
         write(ua(i),'(a)')'name='//name//', physicalQuantity=time, unit=s, nComp=1'
         write(ub(i))      'name='//name//', physicalQuantity=time, unit=s, nComp=1'
         name = 'time_step'//i2a(istep)
         write(ua(i),'(a)')'name='//name//', physicalQuantity= -, unit= -, nComp=1'   
         write(ub(i))      'name='//name//', physicalQuantity= -, unit= -, nComp=1'
      end if
   end do
   

   !Recall that $Values takes only real values. Integer must then be converted to real:
   write(ua(1),'(*(g0,1x))')real(time,rSp),real(itime,rSp)
   write(ua(2),'(*(g0,1x))')real(time,rDp),real(itime,rDp)
   write(ua(3),'(*(g0,1x))')real(time,rSp),real(itime,rSp)
   write(ua(4),'(*(g0,1x))')real(time,rDp),real(itime,rDp)
      
   write(ub(1))real(time,rSp),real(itime,rSp)
   write(ub(2))real(time,rDp),real(itime,rDp)
   write(ub(3))real(time,rSp),real(itime,rSp)
   write(ub(4))real(time,rDp),real(itime,rDp)
      
   end subroutine writeTime

    
!=============================================================================================   
   subroutine openFiles
!=============================================================================================   

   integer :: i
   
   do i = 1, 4
      print*,'opening the ascii file: ',fa(i)   
      open(ua(i), file=fa(i), form='formatted', status='replace')

      print*,'opening the binary file: ',fb(i)      
      open(ub(i), file=fb(i), form='unformatted', status='replace')

      write(ua(i),'(a)')'$Ascii, sizeOfInt=' //i2a(szi(i))//', sizeOfReal='//i2a(szr(i))
      write(ub(i))      '$Binary, sizeOfInt='//i2a(szi(i))//', sizeOfReal='//i2a(szr(i))
   end do   
      
   end subroutine openFiles

!=============================================================================================   
   subroutine closeFiles
!=============================================================================================   

   integer :: i
   
   do i = 1, 4
      write(ua(i),'(a)')' '
      write(ua(i),'(a)')'$End'
      close(ua(i))
      write(ub(i))' '
      write(ub(i))'$End'
      close(ub(i))
   end do
      
   end subroutine closeFiles
   

!=============================================================================================   
   FUNCTION i2a ( i ) result( res )
!=============================================================================================
   integer         ,             intent(in) :: i
   character(len=:), allocatable            :: res
!---------------------------------------------------------------------------------------------
!  Converts integer to string (from Vladimir F., https://stackoverflow.com)
!----------------------------------------------------------------------------------- R.H.04/22

!- local variables: --------------------------------------------------------------------------
   character(len=range(i)+2) :: tmp
!---------------------------------------------------------------------------------------------

   write(tmp,'(i0)') i ;  res = trim(tmp)
  
  END FUNCTION i2a       
   
end

