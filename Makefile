
export dirpk2        = /Users/hassaniriad/1-Codes/pk2_jul20
export dircalmat     = /Users/hassaniriad/1-Codes/pk2_jul20/app/calmat_jul20


define colorecho
      @tput setaf 1
      @echo $1
      @tput sgr0
endef

help:
#h |--------------------------------------------------------------------------|
#h | Usage:  make comp = <yourCompiler>  [opt = "<yourOptions>"] [clean]      |                          
#h |                                                                          |
#h |                                                                          |
#h | where                                                                    |
#h |   . <yourCompiler>: is the name of your compiler (note: this version was |
#h |                     tested with ifort, gfortran and nagfor).             |
#h |   . <yourOptions> : is a string corresponding to a set of compilation    |
#h |                     options (default is opt="-O3").                      |
#h |                                                                          |
#h | Examples:                                                                |
#h |   . make                                                                 |
#h |        prints this help.                                                 |
#h |   . make comp=ifort clean                                                |
#h |        deletes the object files (from the directory obj/ifort),          |
#h |                the module files (from the directory mod/ifort).          |
#h |   . make comp=gfortran                                                   |
#h |        compiles with -O3 optimization flag.                              |
#h |   . make comp=ifort opt="-O3 -fp-model fast=2 -parallel"                 |
#h |         compiles with the specified options.                             |
#h |   . make comp=ifort opt=check                                            |
#h |         compiles with checking options (predefined according to one of   |
#h |         the three compilers cited above).                                |
#h |--------------------------------------------------------------------------|
ifeq ($(comp),)
   help:     #// Show this help.
	@sed -ne '/@sed/!s/#h //p' $(MAKEFILE_LIST)
else

	@echo " "
	$(call colorecho, "==> Compiling marker:")
	@echo " "
	@( cd src; $(MAKE) -f makefile_marker )
	 
clean:
	@echo " "
	$(call colorecho,"==> Cleaning marker:")
	@echo " "
	rm -f ./obj/$(comp)/*.o 
	rm -f ./mod/$(comp)/*.mod 

endif
