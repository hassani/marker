!---------------------------------------------------------------------------------------------
! marker, version 2023.4.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       markerForOldPfile_m
!
! Description: 
!       Reads the adeli outputs in the old (and soon obsolete) ascii file format and
!       extracts/computes variables attached to a given node/point/element
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        04/23
!
! Changes:
!        04/23
!
!---------------------------------------------------------------------------------------------

#include "error.fpp"

MODULE markerForOldPfile_m
   
   use globalParameters_m
   use utilmarker_m
   
   implicit none

   private
   public :: markerForOldPfile_exec, markerForOldPfile_initialize
   
   integer  (Ikind)              :: nvarsc
   character(len=:), allocatable :: buf

CONTAINS

!=============================================================================================   
   SUBROUTINE markerForOldPfile_exec
!============================================================================================= 

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'markerForOldPfile_exec'
   logical                     :: is_readyToPrint
!---------------------------------------------------------------------------------------------
      
   do while ( stat <= IZERO )
      currentStep = currentStep + IONE
!
!-    Lecture de la topologie :
!      
      call markerForOldPfile_Topo ()
      if ( stat /= IZERO ) exit   

      write(*,'(/,a,i0,a,a)')'=> Reading step #',currentStep,' from ',filein

      is_readyToprint = .false.      
!
!-    Lecture du temps :
!
      call markerForOldPfile_Time ()      
      if ( stat /= IZERO ) exit   
!
!-    Lecture des valeurs nodales :
!
      call markerForOldPfile_NodalValues ()
      if ( stat /= IZERO ) exit   
!
!-    Lecture des valeurs elementaires et calculs des invariants :
!
      call markerForOldPfile_ElemValues ()
      if ( stat /= IZERO ) exit   

      call utilmarker_InvariantsNEigenvalues ()
      if ( stat /= IZERO ) exit   
 
      is_readyToPrint = .true.    
       
      if ( currentStep == 1 ) then
         call utilmarker_findElem ()
         if ( stat > IZERO ) exit
      end if
            
      call utilmarker_write () 
      if ( stat > IZERO ) exit   
!
!-    Les mêmes variables pour tous les pas de temps:
!
      vals(NODV)%is_new = .false.
      vals(ELMV)%is_new = .false.
      vals(CSTV)%is_new = .false.
      vals(ELMI)%is_new = .false.
   end do
      
   if ( .not. is_readyToPrint ) then
      if ( stat == RERROR ) then
         call stat%AddMsg ( ' (the step #'//util_intToChar(currentStep)//' is incomplete?)' )
      else if ( stat == EOF ) then
         call utilmarker_error ( err = WARNING, where = HERE, line = pfile%Getline(),     &
              msg = 'Last step (num='//util_intToChar(currentStep)//') is incomplete' )
      end if
   end if   

   if ( currentStep > IONE .or. stat == IZERO ) &
      call femval_displayVark1 ( vals, all = .true., &
                                 title = 'Set of variables present in your output file:' )    

   error_TraceNreturn(stat>IZERO, HERE, stat)  
 
   END SUBROUTINE markerForOldPfile_exec 


!=============================================================================================   
   SUBROUTINE markerForOldPfile_initialize
!============================================================================================= 
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'markerForOldPfile_initialize'
   integer  (Ikind)            :: i, nval
   type     (str_t)            :: valNames(4)   
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE   
   sizeOfInt = 4 ; sizeOfReal = 8

   !present_nodalValues   = .true.
   !present_ElementValues = .true.
      
   call pfile%read ( nvarsc, stat, 'header (nvars)' )
   error_TraceNreturn(stat/=IZERO, HERE, stat)  
             
   valNames(NODV)%str = 'Nodal values'               
   valNames(ELMV)%str = 'Element values'               
   valNames(CSTV)%str = 'Constant values (time & step)'  
   valNames(ELMI)%str = 'Element invariants'               
                
   call femval_init ( valNames, sizeOfReal, stat, vals )
   error_TraceNreturn(stat>IZERO, HERE, stat)

   call msh_init ( sizeOfInt, sizeOfReal, stat, mesh )
   error_TraceNreturn(stat>IZERO, HERE, stat)
   
   mesh%coordUnit  = 'm'
   mesh%coordUnit0 = 'm'
   call mesh % InitUnitConversion ( physQuantCatalog, convert, stat )
!
!- pour les valeurs elementaires:
!
   ! les 3 premieres sont tensorielles (6 comp.), les autres sont scalaires
   nval = nvarsc - 18 + 3
   call vals(ELMV)%alloc ( nvar = nval, nent = 0_Ikind, name = 'Elemental values'  )
   
   vals(ELMV)%geomEntities = 'cells'
   
   do i = 1, 18
      call pfile%read ( buf, stat, 'variable names' )
      error_TraceNreturn(stat/=IZERO, HERE, stat)      
   end do
   vals(ELMV)%varNcomp  (1:3) = 6   
   
   vals(ELMV)%varNames  (1)%str = 'contraintes'
   vals(ELMV)%physNames (1)%str = 'stress'
   vals(ELMV)%physUnits (1)%str = 'Pa'
   vals(ELMV)%physUnits0(1)%str = 'Pa'

   vals(ELMV)%varNames  (2)%str = 'deformations'
   vals(ELMV)%physNames (2)%str = 'strain'
   vals(ELMV)%physUnits (2)%str = '-'
   vals(ELMV)%physUnits0(2)%str = '-'

   vals(ELMV)%varNames  (3)%str = 'taux_de_deformations'
   vals(ELMV)%physNames (3)%str = 'strain_rate'
   vals(ELMV)%physUnits (3)%str = '1/s'
   vals(ELMV)%physUnits0(3)%str = '1/s'
   
   do i = 19, nvarsc
      call pfile%read ( vals(ELMV)%varNames(i-18+3)%str, stat, 'variable names' )
      error_TraceNreturn(stat/=IZERO, HERE, stat)
      vals(ELMV)%varNcomp(i-18+3) = 1
   end do
   
   ! def. plastique effective
   vals(ELMV)%physNames (4)%str = 'strain'
   vals(ELMV)%physUnits (4)%str = '-'
   vals(ELMV)%physUnits0(4)%str = '-'

   ! rapport au seuil
   vals(ELMV)%physNames (5)%str = '-'
   vals(ELMV)%physUnits (5)%str = '-'
   vals(ELMV)%physUnits0(5)%str = '-'

   ! indice de plastification
   vals(ELMV)%physNames (6)%str = '-'
   vals(ELMV)%physUnits (6)%str = '-'
   vals(ELMV)%physUnits0(6)%str = '-'
   
   ! temperature
   vals(ELMV)%physNames (7)%str = 'temperature'
   vals(ELMV)%physUnits (7)%str = 'K'
   vals(ELMV)%physUnits0(7)%str = 'K'

   ! viscosite
   vals(ELMV)%physNames (8)%str = 'viscosity'
   vals(ELMV)%physUnits (8)%str = 'Pa.s'
   vals(ELMV)%physUnits0(8)%str = 'Pa.s'    

   if ( nvarsc == 24 ) then
   ! def. plastique volumique effective 
      vals(ELMV)%physNames (9)%str = 'strain'
      vals(ELMV)%physUnits (9)%str = '-'
      vals(ELMV)%physUnits0(9)%str = '-'   
   end if

   call vals(ELMV) % InitUnitConversion ( physQuantCatalog, convert, stat )  
!
!- pour les valeurs nodales: 3 grandeurs dont 2 vectorielles
!           
   call vals(NODV)%alloc ( nvar = 3_Ikind, nent = 0_Ikind, name = 'Nodal values' )

   vals(NODV)%geomEntities = 'nodes'

   vals(NODV)%varNames  (1)%str = 'vitesses'
   vals(NODV)%physNames (1)%str = 'velocity'
   vals(NODV)%physUnits (1)%str = 'm/s'
   vals(NODV)%physUnits0(1)%str = 'm/s'
   vals(NODV)%varNcomp  (1) = 3

   vals(NODV)%varNames  (2)%str = 'deplacements'
   vals(NODV)%physNames (2)%str = 'displacement'
   vals(NODV)%physUnits (2)%str = 'm'
   vals(NODV)%physUnits0(2)%str = 'm'   
   vals(NODV)%varNcomp  (2) = 3
   
   vals(NODV)%varNames  (3)%str = 'temperatures'
   vals(NODV)%physNames (3)%str = 'temperature'
   vals(NODV)%physUnits (3)%str = 'K'
   vals(NODV)%physUnits0(3)%str = 'K'      
   vals(NODV)%varNcomp  (3) = 1
   
   call vals(NODV) % InitUnitConversion ( physQuantCatalog, convert, stat )  
!
!- pour les valeurs "constantes" : 2 grandeurs (le pas de temps et le temps)
!
   call vals(CSTV)%alloc ( nvar = 2_Ikind, nent = 0_Ikind, name = 'Constant values' )  

   vals(CSTV)%geomEntities = '-'

   vals(CSTV)%varNames  (1)%str = 'pas de temps'
   vals(CSTV)%physNames (1)%str = '-'
   vals(CSTV)%physUnits (1)%str = '-'
   vals(CSTV)%physUnits0(1)%str = '-'
   vals(CSTV)%varNcomp  (1) = 1   
      
   vals(CSTV)%varNames  (2)%str = 'temps'
   vals(CSTV)%physNames (2)%str = 'time'
   vals(CSTV)%physUnits (2)%str = 's'
   vals(CSTV)%physUnits0(2)%str = 's'
   vals(CSTV)%varNcomp  (2) = 1    

   call vals(CSTV) % InitUnitConversion ( physQuantCatalog, convert, stat )  

   vals(NODV)%is_active = .true.
   vals(ELMV)%is_active = .true.
   vals(CSTV)%is_active = .true.
   vals(ELMI)%is_active = .true.
   
!print*,'end '//HERE   
   END SUBROUTINE markerForOldPfile_initialize 
   

!=============================================================================================
   SUBROUTINE markerForOldPfile_topo
!=============================================================================================
!  Lecture de la topologie
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'markerForOldPfile_topo'
   integer  (Ikind), parameter :: tetra = 10 ! vtk code for tetra
   integer  (Ikind)            :: itopo, nelem, npoin, nnode, ndime, nface, npcon, irec(6)
   integer  (Ikind)            :: i, n, itmp, dimCoord(2)
   integer  ( i64 )            :: msize
   character(len=1)            :: chtmp
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE   

   if ( stat /= IZERO ) return
   
   call pfile%read ( itopo, stat, '"itopo"' )
   error_TraceNreturn(stat/=IZERO, HERE, stat)     
   
   if ( itopo == IONE ) then

      !print*,'lecture de la topologie'

      call pfile%read ( irec(1:6), stat, '"nelem, npoin, nnode, ..."' )
      error_TraceNreturn(stat/=IZERO, HERE, stat)  
      
      nelem = irec(1)      
      npoin = irec(2)
      nnode = irec(3)
      ndime = irec(4)
      nface = irec(5)
      npcon = irec(6)
      
      msize = nelem*nnode
      
      dimCoord = [ndime, npoin]
      
      call mesh%alloc ( nCell=nelem, sizeConnect=msize, dimCoord=dimCoord )

      call pfile%read ( 1_Ikind, chtmp, stat, '"TOPOLOGIE DU MAILLAGE"' )
      error_TraceNreturn(stat/=IZERO, HERE, stat) 

      do i = 1, nelem
         call pfile%read ( irec(1:6), stat, 'Cell-to-Node connectivity' )
         error_TraceNreturn(stat/=IZERO, HERE, stat) 
         call mesh%set ( connect=irec(3:6), domain=irec(2), type=tetra, elemId=i, stat=stat)
         error_TraceNreturn(stat/=IZERO, HERE, stat) 
      end do      

      call pfile%read ( 1_Ikind, chtmp, stat, '"CONTOURS DES MATERIAUX"' )
      error_TraceNreturn(stat/=IZERO, HERE, stat) 

      n = npcon/10 ; if (mod(npcon,10_Ikind) > IZERO) n = n + IONE
      do i = 1, n
         call pfile%read ( itmp, stat, 'Edge-to-Node connectivity' )
         error_TraceNreturn(stat/=IZERO, HERE, stat) 
      end do
      
      call pfile%read ( 1_Ikind, chtmp, stat, '"FACETTES"' )
      error_TraceNreturn(stat/=IZERO, HERE, stat) 

      do i = 1, nface
         call pfile%read ( itmp, stat, 'Facet-to-Node connectivity' )
         error_TraceNreturn(stat/=IZERO, HERE, stat) 
      end do

      !print*,'fin lecture topologie'
      
      call vals(NODV)%alloc ( nent = npoin, allocv = .true. )
      call vals(ELMV)%alloc ( nent = nelem, allocv = .true. )
      
   end if

!print*,'end '//HERE   
   END SUBROUTINE markerForOldPfile_topo
   
   
!=============================================================================================
   SUBROUTINE markerForOldPfile_time
!=============================================================================================

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'markerForOldPfile_time'
   character(len=1)            :: chtmp   
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE   

   if ( stat /= IZERO ) return

   call pfile%read ( 1_Ikind, chtmp, stat, '"COORDONNEES"')
   error_TraceNreturn(stat/=IZERO, HERE, stat)
   
   call vals(CSTV)%read ( file = pfile, stat = stat )
   error_TraceNreturn(stat/=IZERO, HERE, stat)

!print*,'end '//HERE   
   END SUBROUTINE markerForOldPfile_time


!=============================================================================================
   SUBROUTINE markerForOldPfile_nodalValues
!=============================================================================================
!  Lecture des valeurs nodales
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'markerForOldPfile_nodalValues'
   integer  (Ikind), parameter :: nvnod = 10
   integer  (Ikind)            :: i
   real     (Rkind)            :: vnodl(nvnod+1)
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE   

   if ( stat /= IZERO ) return
   
   do i = 1, mesh%npoin
!
!-    lecture des valeurs du noeud #i :
!
      ! l'enregistrement comprend :
      ! 1   : numero du noeud
      ! 2-4 : coordonnees -- 5-7 : vitesses -- 8-10 : deplacements -- 11 : temperature

      call pfile%read ( vnodl, stat, 'Nodal Coordinates & Nodal Values' )
      error_TraceNreturn(stat/=IZERO, HERE, stat)
      
      ! coordonnees             
      call mesh%set ( coord=vnodl(2:4), nodeId=i, stat=stat) 
      error_TraceNreturn(stat/=IZERO, HERE, stat)
      
      ! vitesse    
      call vals(NODV)%setElm ( values=vnodl( 5: 7), varId=1_Ikind, entId = i ) 
      
      ! deplacement
      call vals(NODV)%setElm ( values=vnodl( 8:10), varId=2_Ikind, entId = i ) 

      ! temperature
      call vals(NODV)%setElm ( values=vnodl(11:11), varId=3_Ikind, entId = i ) 
      
   end do

!print*,'end '//HERE      
   END SUBROUTINE markerForOldPfile_nodalValues


!=============================================================================================
   SUBROUTINE markerForOldPfile_elemValues
!=============================================================================================
!  Lecture des valeurs elementaires
!---------------------------------------------------------------------------------------------

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'markerForOldPfile_elemValues'
   integer  (Ikind)            :: i, j, k
   real     (Rkind)            :: velem(nvarsc+1)
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE

   if ( stat /= IZERO ) return
   
   do i = 1, mesh%nCell
!
!-    lecture des valeurs de l'element #i :
!
      ! l'enregistrement comprend :
      ! 1   : numero de l'element
      ! 2-7 : contraintes -- 8-13 : deformations -- 14-19 : taux de deformations
      ! 20  : deformation plastique cumulee -- 21 : rapport au seuil plastique
      ! 22  : indice de plastification
      ! 23  : temperature moyenne -- 24 : viscosite effective -- 25 : def. plast. vol.

      call pfile%read ( velem, stat, 'Element Values' )
      error_TraceNreturn(stat/=IZERO, HERE, stat)

      velem(24) = log10(velem(24) + 1e-30) ! j'ajoute 1e-30 en cas de viscosite nulle (!)

      ! stress
      call vals(ELMV)%setElm ( values=velem( 2: 7), varId=1_Ikind, entId = i ) 
      
      ! strain
      call vals(ELMV)%setElm ( values=velem( 8:13), varId=2_Ikind, entId = i ) 

      ! strain rate
      call vals(ELMV)%setElm ( values=velem(14:19), varId=3_Ikind, entId = i ) 

      j = 3
      do k = 20, nvarsc+IONE
         j = j + IONE
         call vals(ELMV)%setElm ( values=velem(k:k), varId=j, entId = i )
      end do

   end do

!print*,'end '//HERE   
   END SUBROUTINE markerForOldPfile_elemValues
   
      
END MODULE markerForOldPfile_m
   
