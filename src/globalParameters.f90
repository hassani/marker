!---------------------------------------------------------------------------------------------
! marker, version 2023.4.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       globalParameters_m
!
! Description: 
!       Declare some shared variables
! 
! Notes: 
!       
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        04/23
!
! Changes:
!        
!---------------------------------------------------------------------------------------------

MODULE globalParameters_m
   
   use constantsmarker_m
   
!  Import some DT and procedures from pk2 module:
   use pk2mod_m, only: &
            !  DT:
              str_t, err_t, &
            ! procedures:
              err_SetHaltingMode, err_GetHaltingMode, err_GetWarningMode, &
              util_intToChar, util_countTokens, util_getLhsNRhs, util_stringLow, &
              util_RemoveSpaces1, util_alloc, SignalHandler_SignalCatch
              
!  DT for files, f.e. values and meshes:     
   use file_m
   use femval_m    
   use msh_m 
   use elm_m
   
   use units_m
             
   implicit none   

!  for the version:
   character(len=:), allocatable :: numVersion
   character(len=:), allocatable :: compilDate
   character(len=:), allocatable :: compilOpts

!  for name files: 
   character(len=:), allocatable :: filein     ! generic name of the input p-file
   character(len=:), allocatable :: myPfile    ! complete name of the p-file 
   character(len=:), allocatable :: filedef    ! user's default file
   character(len=:), allocatable :: wildcard
   
!  for a binary pfile: number of bytes for integers and reals:
   integer(Ikind) :: sizeOfInt = 0, sizeOfReal = 0
   
!  for units conversion:
   logical                  :: convert = .true. 
   type(phQ_t), allocatable :: physQuantCatalog(:)

!  for computing invariants and eigen-elements:
   type   (str_t), allocatable :: userListInv(:,:), userListEig(:,:)  
   integer(Ikind)              :: nUserInv, nUserEig

!  for errors handling:
   type   (err_t) :: stat
   integer        :: iostat = 0

!  for the mesh:
   class(msh_t), allocatable :: mesh

!  for the fem values:
   class(femval_t), allocatable :: vals(:)
      
!  for the input file:   
   type(file_t) :: pfile
   integer      :: uout

!  for the chosen point or node:
   real   (Rkind)              :: pointXYZ(3)
   integer(Ikind)              :: nodeId = 0, elemId = 0
   integer(Ikind), allocatable :: elemFound(:)   
   logical                     :: mean = .true.
   class  (elm_t), allocatable :: myElm

!  Other shared variables:
   integer(Ikind) :: firstStep   = 0, &
                     finalStep   = 0, &
                     incremStep  = 0, &
                     nextStep    = 0, &
                     currentStep = 0
                     
END MODULE globalParameters_m