#include "error.fpp"

MODULE elm_m
   
   use pk2mod_m, only: Ikind, Rkind, UERROR, WARNING, IZERO, RZERO, RONE, MDSTR, &
                       err_t, util_intToChar
   
   implicit none
   
   private
   public :: elm_t, shp_t
   
   type :: shp_t
      integer(Ikind) :: npnt = 0
      real   (Rkind), allocatable :: points(:,:), shapef(:,:), derloc(:,:,:)
   end type shp_t
!
!- A DT for an element
!   
   type, abstract :: elm_t
      ! informations about the element:
      character(len=:), allocatable :: name
      integer  (Ikind)              :: nDime = 0, nNode = 0, vtkCode = 0
      real     (Rkind), allocatable :: nodCoord(:,:)
   contains
      ! method for the calculation of shape functions and their local derivatives
      procedure(interf_shape ), deferred, pass(self) :: computeShape
      ! method for the inverse mapping:
      procedure, pass(self) :: inverseMap => elm_inverseMap   
      ! deferred methods:
   end type elm_t   

   type, extends(elm_t) :: elmEdge2_t
   contains
      procedure, pass(self) :: computeShape  => elm_edge2      
   end type elmEdge2_t   

!    type, extends(elm_t) :: elmEdge3_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_edge3      
!    end type elmEdge3_t   
!    
!    type, extends(elm_t) :: elmTria3_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_tria3      
!    end type elmTria3_t   
! 
!    type, extends(elm_t) :: elmTria4_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_tria4      
!    end type elmTria4_t   
! 
!    type, extends(elm_t) :: elmTria6_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_tria6      
!    end type elmTria6_t      
! 
!    type, extends(elm_t) :: elmTria7_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_tria7      
!    end type elmTria7_t   
!    
!    type, extends(elm_t) :: elmQuad4_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_quad4      
!    end type elmQuad4_t 
! 
!    type, extends(elm_t) :: elmQuad8_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_quad8      
!    end type elmQuad8_t 
!    
!    type, extends(elm_t) :: elmQuad9_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_quad9     
!    end type elmQuad9_t 
! 
!    type, extends(elm_t) :: elmTetra4_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_tetra4     
!    end type elmTetra4_t 
! 
!    type, extends(elm_t) :: elmTetra10_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_tetra10     
!    end type elmTetra10_t 
! 
!    type, extends(elm_t) :: elmHexa8_t
!    contains
!       procedure, pass(self) :: computeShape  => elm_hexa8     
!    end type elmHexa8_t 
                        
   abstract interface
      subroutine interf_shape ( self, localPnt, setInfo, res ) 
         import :: Rkind, shp_t, elm_t
         class(elm_t),           intent(in out) :: self 
         real (Rkind), optional, intent(in    ) :: localPnt(:,:)
         logical     , optional, intent(in    ) :: setInfo
         type (shp_t), optional, intent(in out) :: res
      end subroutine interf_shape    
   end interface

   ! constructor/initializer:
   interface elm_t
      module procedure elm_finit
   end interface
          
CONTAINS

            
!=============================================================================================
   FUNCTION elm_finit ( namelem, vtkCode, stat ) result ( res )
!=============================================================================================   
   character(len=*), optional,   intent(in    ) :: namelem
   integer  (Ikind), optional,   intent(in    ) :: vtkCode
   type     (err_t),             intent(in out) :: stat
   class    (elm_t), allocatable                :: res
!--------------------------------------------------------------------------------------------- 
!  Initializes an elm_t variables according to the element name or its vtk code.
!---------------------------------------------------------------------------------- RH 04/23 - 
   
   call elm_init ( namelem, vtkCode, stat , res )
      
   END FUNCTION elm_finit


!=============================================================================================
   SUBROUTINE elm_init ( namelem, vtkCode, stat, res )
!=============================================================================================   
   character(len=*), optional,    intent(in    ) :: namelem
   integer  (Ikind), optional,    intent(in    ) :: vtkCode
   type     (err_t),              intent(in out) :: stat
   class    (elm_t), allocatable, intent(   out) :: res
!--------------------------------------------------------------------------------------------- 
!  Initializes an elm_t variables according to the element name or its vtk code.
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: HERE = 'elm_init'
   character(len=:), allocatable :: name
!---------------------------------------------------------------------------------------------  
   
   if ( present(vtkCode) ) then
      name = elm_fromVtkCodeToElementName ( vtkCode, stat )
      error_TraceNreturn(stat>IZERO, HERE, stat) 
   else if ( present(namelem) ) then
      name = trim(adjustl(namelem))
   else
      stat = err_t ( msg = 'Missing "namelem" or "vtkCode"', stat = UERROR, where = HERE )  
      return
   end if
   
   select case ( name )
      case ("edge2")           
         allocate(elmEdge2_t :: res)  
!       case ("edge3")           
!          allocate(elmEdge3_t :: res)  
!       case ("tria3")           
!          allocate(elmTria3_t :: res)  
!       case ("tria4")           
!          allocate(elmTria4_t :: res)           
!       case ("tria6")           
!          allocate(elmTria6_t :: res)  
!       case ("tria7")           
!          allocate(elmTria7_t :: res)                   
!        case ("quad4")           
!           allocate(elmQuad4_t :: res)                   
!       case ("quad8")           
!          allocate(elmQuad8_t :: res)                   
!       case ("quad9")           
!          allocate(elmQuad9_t :: res)                   
!       case ("tetra4")           
!          allocate(elmTetra4_t :: res)                   
!       case ("tetra10")           
!          allocate(elmTetra10_t :: res)   
!       case ("hexa8")           
!          allocate(elmHexa8_t :: res) 
                     
      case default
         stat = err_t ( stat=UERROR, where='msh_init', msg='Unknown element "'//name//'"')
         return
   end select
   
   call res%computeShape ( setInfo = .true. )
      
   END SUBROUTINE elm_init   


!=============================================================================================
   FUNCTION elm_fromVtkCodeToElementName ( vtkCode, stat ) result ( name )
!=============================================================================================
   integer  (Ikind), intent(in    ) :: vtkCode
   type     (err_t), intent(in out) :: stat      
   character(len=:), allocatable    :: name
!--------------------------------------------------------------------------------------------- 
!  Mapping between certain VTK codes and the corresponding element names in this library
!---------------------------------------------------------------------------------- RH 04/23 -   

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_fromVtkCodeToElementName'
!---------------------------------------------------------------------------------------------   

   select case (vtkCode)
      case ( 3 ) ! VTK_LINE
         name = 'edge2' 
      case ( 21 ) ! VTK_QUADRATIC_EDGE
         name = 'edge3'
      case ( 5 ) ! VTK_TRIANGLE
         name = 'tria3'
      case ( 22 ) ! VTK_QUADRATIC_TRIANGLE
         name = 'tria6'
      case ( 9 ) ! VTK_QUAD 
         name = 'quad4'
      case ( 23 ) ! VTK_QUADRATIC_QUAD
         name = 'quad8'
      case ( 10 ) ! VTK_TETRA
         name = 'tetra4'
      case ( 24 ) ! VTK_QUADRATIC_TETRA
         name = 'tetra10'
      case ( 12 ) ! VTK_HEXAHEDRON
         name = 'hexa8'
      case ( 25 ) ! VTK_QUADRATIC_HEXAHEDRON
         name = 'hexa20'
      case default
         stat = err_t ( stat=UERROR, where=HERE, msg='element of vtk code ' //  &
                        util_intToChar(vtkCode)//' not present in this library' )
   end select

   END FUNCTION elm_fromVtkCodeToElementName


!=============================================================================================
   SUBROUTINE elm_edge2 ( self, localPnt, setInfo, res )
!=============================================================================================   
   class  (elmEdge2_t),           intent(in out) :: self
   real   (  Rkind   ), optional, intent(in    ) :: localPnt(:,:)   
   logical            , optional, intent(in    ) :: setInfo
   type   (  shp_t   ), optional, intent(in out) :: res
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at a set of local points (localPnt) for the (P1)
!  linear egde element (2-noded)
!
!     Local node numbering and coordinates:
!
!            1                   2
!            *-------------------*
!           -1                  +1
!
!  If setInfo is present, set:
!   - the physical dimension (%nDime), 
!   - the number of node (%nNode),
!   - the name of this element (%name),
!   - its vtk code if appropriate (%vtkCode),
!   - the local coordinates of its nodes (%nodCoord)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   integer(Ikind), parameter :: ndime = 1, nnode = 2
   real   (Rkind), parameter :: dm = 0.5_Rkind, u = 1.0_Rkind
   real   (Rkind)            :: x
   integer(Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( present(localPnt) ) then
      npnt = size(localPnt,2)
      call allocateShp ( res%shapef, res%derloc, res%points, ndime, nnode, npnt )

      do i = 1, npnt
         x = localPnt(1,i)  
         res%shapef(  1:nnode,i) = dm * [ u - x, u + x ]
         res%derloc(1,1:nnode,i) = dm * [    -u,     u ]   
         res%points(1        ,i) = x   
      end do
   end if
   
   if ( present(setInfo) ) then
      if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
      self%nDime = ndime ; self%nNode = nnode; self%name = 'edge2'; self%vtkCode = 3
      self%nodCoord(1,:) = [ -u , u ]
   end if
      
   END SUBROUTINE elm_edge2


! !=============================================================================================
!    SUBROUTINE elm_edge3 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmEdge3_t),           intent(in out) :: self
!    real   (  Rkind   ), optional, intent(in    ) :: localPnt(:,:)   
!    logical            , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the (P2)
! !  quadratic egde element (3-noded)
! !
! !     Local node numbering and coordinates:
! !
! !            1         3         2
! !            *-------- * --------*
! !           -1         0        +1
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 1, nnode = 3
!    integer(Ikind)            :: i, npnt
! !---------------------------------------------------------------------------------------------
!    
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       do i = 1, npnt    
!          call elm_P2edge ( localPnt(1,i), self%shapef(:,i), self%derloc(:,:,i) ) 
!          self%points(1,i) = localPnt(1,i)
!       end do      
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'edge3'; self%vtkCode = 21
!       self%nodCoord(1,:) = [ -1.0_Rkind , 1.0_Rkind , 0.0_Rkind ]
!    end if
! 
!    END SUBROUTINE elm_edge3
!    
! !=============================================================================================
!    SUBROUTINE elm_P2edge ( x, shape, derloc )
! !=============================================================================================   
!    real(Rkind),           intent(in    ) :: x
!    real(Rkind),           intent(in out) :: shape(3)
!    real(Rkind), optional, intent(in out) :: derloc(1,3)
! !---------------------------------------------------------------------------------- RH 04/23 - 
!    
! !- local variables: --------------------------------------------------------------------------
!    real(Rkind), parameter :: dm = 0.5_Rkind, u = 1.0_Rkind, d = 2.0_Rkind
! !---------------------------------------------------------------------------------------------
! 
!    shape = [ dm*(x*x - x) , dm*(x*x + x) , u - x*x ]
!    if ( present(derloc) ) derloc(1,:) = [ x - dm, x + dm, -d*x ]
!    
!    END SUBROUTINE elm_P2edge
!    
!       
! !=============================================================================================
!    SUBROUTINE elm_tria3 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmTria3_t),           intent(in out) :: self
!    real   (  Rkind   ), optional, intent(in    ) :: localPnt(:,:)   
!    logical            , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the (P1)
! !  linear triangle (3-noded)
! !
! !     Local node numbering and coordinates:
! !
! !                3 (0,1)
! !                . .
! !                .   .
! !                .     .
! !                .       .              y
! !                .         .            .
! !                1 . . . . . 2          .. x
! !              (0,0)       (1,0)
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 2, nnode = 3   
!    real   (Rkind), parameter :: zr = 0.0_Rkind, u = 1.0_Rkind
!    integer(Ikind)            :: i, npnt
! !---------------------------------------------------------------------------------------------
!    
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       do i = 1, npnt    
!          call elm_P1triangle ( localPnt(1,i), localPnt(2,i), &
!                                self%shapef(:,i), self%derloc(:,:,i) ) 
!          self%points(:,i) = localPnt(i:,i)
!       end do
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'tria3'; self%vtkCode = 5
!       self%nodCoord(1,:) = [ zr , u  , zr ]
!       self%nodCoord(2,:) = [ zr , zr , u  ]
!    end if
! 
!    END SUBROUTINE elm_tria3
!    
! !=============================================================================================
!    SUBROUTINE elm_P1triangle ( x, y, shape, derloc )
! !=============================================================================================   
!    real(Rkind),           intent(in    ) :: x, y
!    real(Rkind),           intent(in out) :: shape(3)
!    real(Rkind), optional, intent(in out) :: derloc(2,3)
! !---------------------------------------------------------------------------------- RH 04/23 - 
!    
! !- local variables: --------------------------------------------------------------------------
!    real(Rkind), parameter :: zr = 0.0_Rkind, u = 1.0_Rkind
! !---------------------------------------------------------------------------------------------
! 
!    shape = [ u - x - y , x , y  ]
!    if ( present(derloc) ) then
!       derloc(1,:) = [   -u      , u , zr ]
!       derloc(2,:) = [       -u  , zr, u  ]
!    end if
!    
!    END SUBROUTINE elm_P1triangle
!    
!    
! !=============================================================================================
!    SUBROUTINE elm_tria4 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmTria4_t),           intent(in out) :: self
!    real   (  Rkind   ), optional, intent(in    ) :: localPnt(:,:)   
!    logical            , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the P1
! !  triangle with additional bubble function (4-noded)
! !
! !     Local node numbering and coordinates:
! !
! !                3 (0,1)
! !                . .
! !                .   .
! !                .     .
! !                .   4 (1/3,1/3)        y
! !                .         .            .
! !                1 . . . . . 2          .. x
! !              (0,0)       (1,0)
! !
! !
! !      The shape functions are:
! !
! !             L_i -  9 * bubble    i = 1,2,3
! !                   27 * bubble    i = 4
! !
! !       where the L_i are the P1 shape functions abd bubble = L_1 * L_2 * L_3
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 2, nnode = 4   
!    real   (Rkind), parameter :: zr=0.0_Rkind, u=1.0_Rkind, t=3.0_Rkind, n=9.0_Rkind   
!    real   (Rkind)            :: b9, bx9, by9, L(3)
!    integer(Ikind)            :: i, npnt
! !---------------------------------------------------------------------------------------------
!    
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       
!       do i = 1, npnt
! !
! !--      P1 shape functions:
! !
!          call elm_P1triangle ( localPnt(1,i), localPnt(2,i), L ) 
! !
! !--      bubble (*9) and its derivatives (*9):
! !
!          b9  = n * L(1) * L(2) * L(3)
!          bx9 = n * L(3) * (L(1) - L(2))
!          by9 = n * L(2) * (L(1) - L(3))
! !
! !--      shape functions and their derivatives:
! !
!          self%shapef(1:3,i) = L - b9
!          self%shapef( 4 ,i) = t*b9
!        
!          self%derloc(1,:,i) = [ -u - bx9 , u - bx9 ,  -bx9 , t*bx9 ]
!          self%derloc(2,:,i) = [ -u - by9 ,   - by9 , u-by9 , t*by9 ]
! 
!          self%points(:,i) = localPnt(:,i)         
!       end do
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'tria4'; self%vtkCode = -999
!       self%nodCoord(1,:) = [ zr , u  , zr , u/t ]
!       self%nodCoord(2,:) = [ zr , zr , u  , u/t ]
!    end if
! 
!    END SUBROUTINE elm_tria4
! 
! 
! !=============================================================================================
!    SUBROUTINE elm_tria6 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmTria6_t),           intent(in out) :: self
!    real   (  Rkind   ), optional, intent(in    ) :: localPnt(:,:)   
!    logical            , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the (P2)
! !  quadratic triangle (6-noded)
! !
! !     Local node numbering and coordinates :
! !
! !                3 (0,1)
! !                . .
! !                .   .
! !                6     5
! !                .       .               y
! !                .         .             .
! !                1 . . 4. . . 2          .. x
! !              (0,0)        (1,0)
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 2, nnode = 6 
!    real   (Rkind), parameter :: zr=0.0_Rkind, dm=0.5_Rkind, u=1.0_Rkind
!    integer(Ikind)            :: i, npnt 
! !---------------------------------------------------------------------------------------------
!    
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       do i = 1, npnt        
!          call elm_P2triangle ( localPnt(1,i), localPnt(2,i), &
!                                self%shapef(:,i), self%derloc(:,:,i) )
!          self%points(:,i) = localPnt(:,i)
!       end do
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'tria6'; self%vtkCode = 22
!       self%nodCoord(1,:) = [ zr , u  , zr ,dm , dm ,zr ]
!       self%nodCoord(2,:) = [ zr , zr , u  ,zr , dm ,dm  ]
!    end if
! 
!    END SUBROUTINE elm_tria6   
! 
! !=============================================================================================
!    SUBROUTINE elm_P2triangle ( x, y, shape, derloc )
! !=============================================================================================   
!    real(Rkind),           intent(in    ) :: x, y
!    real(Rkind),           intent(in out) :: shape(6)
!    real(Rkind), optional, intent(in out) :: derloc(2,6)
! !---------------------------------------------------------------------------------- RH 04/23 - 
!    
! !- local variables: --------------------------------------------------------------------------
!    real(Rkind), parameter :: zr=0.0_Rkind, dm=0.5_Rkind, u=1.0_Rkind, d=2.0_Rkind, q=4.0_Rkind
!    real(Rkind)            :: L1
! !---------------------------------------------------------------------------------------------
! 
!    L1 = u - x - y;
! 
!    shape(1:3) = [ L1*(d*L1 - u), x*(d*x - u), y*(d*y -u) ]
!    shape(4:6) = [ q*x*L1       , q*x*y      , q*y*L1     ]
!       
!    if ( present(derloc) ) then
!       derloc(1,1:3) = [ u - q*L1     , q*x -u     , zr         ]
!       derloc(2,1:3) = [ u - q*L1     , zr         , q*y - u    ]
!       derloc(1,4:6) = [ q*(L1 - x)   , q*y        ,-q*y        ]
!       derloc(2,4:6) = [-q*x          , q*x        , q*(L1 - y) ]
!    end if
!    
!    END SUBROUTINE elm_P2triangle
! 
! 
! !=============================================================================================
!    SUBROUTINE elm_tria7 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmTria7_t),           intent(in out) :: self
!    real   (  Rkind   ), optional, intent(in    ) :: localPnt(:,:)   
!    logical            , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the 
! !  P2-bubble triangle (7-noded)
! !
! !     Local node numbering and coordinates :
! !
! !                3 (0,1)
! !                . .
! !                .   .
! !                6     5
! !                .   7   .               y
! !                .         .             .
! !                1 . . 4. . . 2          .. x
! !              (0,0)        (1,0)
! !
! !      The shape functions are:
! !
! !             N_i +  3 * bubble    i = 1,2,3
! !             N_i - 12 * bubble    i = 4,5,6
! !                   27 * bubble    i = 7
! !
! !      where N_i are the shape functions of the P2 element and bubble is 
! !                 bubble = L_1 * L_2 * L_3 = (1 - x - y) * x * y
! !      with L_i being the shape functions of the P1 element 
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 2, nnode = 7 
!    real   (Rkind), parameter :: v0=0.0_Rkind, v05= 0.5_Rkind, v1 =1.0_Rkind, v2=2.0_Rkind, &
!                                  v3=3.0_Rkind, v12=12.0_Rkind, v27=27.0_Rkind, &
!                                  c(6) = [v3, v3, v3, -v12,-v12,-v12]
!    integer(Ikind)            :: i, npnt 
!    real   (Rkind)            :: x, y, b, bx, by, N(6), dN(2,6)
! !---------------------------------------------------------------------------------------------
!    
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       
!       do i = 1, npnt
!          x = localPnt(1,i); y = localPnt(2,i)
! 
!          b = (v1 - x - y)*x*y; bx = (v1 - v2*x - y)*y; by = (v1 - v2*y - x)*x
!                
!          call elm_P2triangle ( x, y, N, dN )
! 
!          self%shapef(1:6  ,i) = N + c * b
!          self%shapef(  7  ,i) = v27 * b 
!                                 
!          self%derloc(1,1:6,i) = dN(1,1:6) + c * bx
!          self%derloc(2,1:6,i) = dN(2,1:6) + c * by
!          self%derloc(1,  7,i) = v27 * bx
!          self%derloc(2,  7,i) = v27 * by  
!          
!          self%points(:    ,i) = localPnt(:,i)
!       end do      
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'tria7'; self%vtkCode = -999
!       self%nodCoord(1,:) = [ v0 , v1 , v0 , v05 , v05 ,v0  , v1/v3 ]
!       self%nodCoord(2,:) = [ v0 , v0 , v1 , v0  , v05 ,v05 , v1/v3 ]
!    end if
! 
!    END SUBROUTINE elm_tria7     
!       
! 
! !=============================================================================================
!    SUBROUTINE elm_quad4 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmQuad4_t),           intent(in out) :: self
!    real   (  Rkind   ), optional, intent(in    ) :: localPnt(:,:)   
!    logical            , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the (Q1)
! !  bilinear quad (4-noded)
! !
! !     Local node numbering and coordinates:
! !
! !                     (-1,1)          (1,1)
! !                         4------------3
! !                         |      y     |
! !                         |      .     |
! !                         |      . .x  |
! !                         |            |
! !                         |            |
! !                         1------------2
! !                      (-1,-1)       (1,-1)
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 2, nnode = 4   
!    integer(Ikind)            :: i, npnt   
!    real   (Rkind), parameter :: u = 1.0_Rkind
! !---------------------------------------------------------------------------------------------
!    
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       do i = 1, npnt
!          call elm_Q1quad ( localPnt(1,i), localPnt(2,i), &
!                            self%shapef(:,i), self%derloc(:,:,i) ) 
!          self%points(:,i) = localPnt(:,i)
!       end do
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'quad4'; self%vtkCode = 9
!       self%nodCoord(1,:) = [ -u , u , u ,-u ]
!       self%nodCoord(2,:) = [ -u ,-u , u , u  ]
!    end if
! 
!    END SUBROUTINE elm_quad4
! 
! !=============================================================================================
!    SUBROUTINE elm_Q1quad ( x, y, shape, derloc )
! !=============================================================================================   
!    real(Rkind),           intent(in    ) :: x, y
!    real(Rkind),           intent(in out) :: shape(4)
!    real(Rkind), optional, intent(in out) :: derloc(2,4)
! !---------------------------------------------------------------------------------- RH 04/23 - 
!    
! !- local variables: --------------------------------------------------------------------------
!    real(Rkind), parameter :: v1=1.0_Rkind, v025=0.25_Rkind
!    real(Rkind)            :: xm, xp, ym, yp
! !---------------------------------------------------------------------------------------------
! 
!    xm = v1 - x ; xp = v1 + x ; ym = v1 - y ; yp = v1 + y
! 
!    shape = v025 * [ xm*ym, xp*ym, xp*yp, xm*yp ]
!       
!    if ( present(derloc) ) then
!       derloc(1,:) = v025 * [ -ym , ym , yp ,-yp ]
!       derloc(2,:) = v025 * [ -xm ,-xp , xp , xm ]
!    end if
!    
!    END SUBROUTINE elm_Q1quad   
! 
! 
! !=============================================================================================
!    SUBROUTINE elm_quad8 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmQuad8_t),           intent(in out) :: self
!    real   (  Rkind   ), optional, intent(in    ) :: localPnt(:,:)   
!    logical            , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the (Q2)
! !  quadratic quad (8-noded)
! !
! !     Local node numbering and coordinates:
! !
! !                     (-1,1)          (1,1)
! !                         4------7-----3
! !                         |      y     |
! !                         |      .     |
! !                         8      . .x  6 
! !                         |            |
! !                         |            |
! !                         1------5-----2
! !                      (-1,-1)       (1,-1)
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 2, nnode = 8
!    real   (Rkind), parameter :: v0=0.0_Rkind, v1=1.0_Rkind, v2=2.0_Rkind
!    integer(Ikind)            :: i, npnt   
!    real   (Rkind)            :: N(4), c1(4), c2(4), dN(2,4)
!    real   (Rkind)            :: x, y
! !---------------------------------------------------------------------------------------------
!           
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       do i = 1, npnt
!          x = localPnt(1,i) ; y = localPnt(2,i)
!          c1 = [ v1 + x + y , v1 - x + y , v1 - x - y , v1 + x -y ]
!          c2 = [ v1 + x     , v1 + y     , v1 - x     , v1 - y    ]
!          call elm_Q1quad ( x, y, N, dN ) 
!          
!          self%shapef(1:4  ,i) =-N * c1
!          self%shapef(5:8  ,i) = N * c2 * v2 
!          
!          self%derloc(1,1:4,i) =  -dN(1,:) * c1 - N * [ v1,-v1,-v1, v1 ]
!          self%derloc(2,1:4,i) =  -dN(2,:) * c1 - N * [ v1, v1,-v1,-v1 ]
!          self%derloc(1,5:8,i) = ( dN(1,:) * c2 + N * [ v1, v0,-v1, v0 ] ) * v2
!          self%derloc(2,5:8,i) = ( dN(2,:) * c2 + N * [ v0, v1, v0,-v1 ] ) * v2
!          
!          self%points(:    ,i) = localPnt(:,i)
!       end do
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'quad8'; self%vtkCode = 23
!       self%nodCoord(1,:) = [ -v1 , v1 , v1 ,-v1 , v0 , v1 , v0 ,-v1 ]
!       self%nodCoord(2,:) = [ -v1 ,-v1 , v1 , v1 ,-v1 , v0 , v1 , v0  ]
!    end if
! 
!    END SUBROUTINE elm_quad8
! 
! 
! !=============================================================================================
!    SUBROUTINE elm_quad9 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmQuad9_t),           intent(in out) :: self
!    real   (  Rkind   ), optional, intent(in    ) :: localPnt(:,:)   
!    logical            , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the (Q2-s)
! !  quadratic quad (8-noded)
! !
! !     Local node numbering and coordinates:
! !
! !                     (-1,1)          (1,1)
! !                         4------7-----3
! !                         |      y     |
! !                         |      .     |
! !                         8      9 .x  6
! !                         |            |
! !                         |            |
! !                         1------5-----2
! !                      (-1,-1)       (1,-1)
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 2, nnode = 9
!    real   (Rkind), parameter :: v0=0.0_Rkind, v1=1.0_Rkind
!    integer(Ikind)            :: i, npnt   
!    real   (Rkind)            :: Lx(3), Ly(3), dLx(3), dLy(3)
! !---------------------------------------------------------------------------------------------
!                     
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       do i = 1, npnt
!          call elm_P2edge ( localPnt(1,i), Lx, dLx )
!          call elm_P2edge ( localPnt(2,i), Ly, dLy )
!          self%shapef(1:4  ,i) = [ Lx(1)* Ly(1), Lx(2)* Ly(1), Lx(2)* Ly(2), Lx(1)* Ly(2) ] 
!          self%shapef(5:8  ,i) = [ Lx(3)* Ly(1), Lx(2)* Ly(3), Lx(3)* Ly(2), Lx(1)* Ly(3) ]
!          self%shapef(  9  ,i) =   Lx(3)* Ly(3)   
! 
!          self%derloc(1,1:4,i) = [ dLx(1)* Ly(1), dLx(2)* Ly(1), dLx(2)* Ly(2), dLx(1)* Ly(2) ]
!          self%derloc(2,1:4,i) = [  Lx(1)*dLy(1),  Lx(2)*dLy(1),  Lx(2)*dLy(2),  Lx(1)*dLy(2) ]
!           
!          self%derloc(1,5:8,i) = [ dLx(3)* Ly(1), dLx(2)* Ly(3), dLx(3)* Ly(2), dLx(1)* Ly(3) ]
!          self%derloc(2,5:8,i) = [  Lx(3)*dLy(1),  Lx(2)*dLy(3),  Lx(3)*dLy(2),  Lx(1)*dLy(3) ]
!           
!          self%derloc(1,9  ,i) = dLx(3) * Ly(3)
!          self%derloc(2,9  ,i) =  Lx(3) * dLy(3)  
!          
!          self%points(:  ,i) = localPnt(:,i)         
!       end do
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'quad9'; self%vtkCode =-999
!       self%nodCoord(1,:) = [ -v1 , v1 , v1 ,-v1 , v0 , v1 , v0 ,-v1 , v0 ]
!       self%nodCoord(2,:) = [ -v1 ,-v1 , v1 , v1 ,-v1 , v0 , v1 , v0 , v0 ]
!    end if
! 
!    END SUBROUTINE elm_quad9
!    
!                 
! !=============================================================================================
!    SUBROUTINE elm_tetra4 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmTetra4_t),           intent(in out) :: self
!    real   (   Rkind   ), optional, intent(in    ) :: localPnt(:,:)   
!    logical             , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the (P1)
! !  linear tetrahedral element (4-noded)
! !
! !     Local node numbering and coordinates:
! !
! !
! !                      4 (0,0,1)
! !                      |
! !                      |
! !                      |
! !                      |
! !                      |____________ 3 (0,1,0)
! !                     / 1 (0,0,0)
! !                    /
! !                   /
! !                  /
! !                 /
! !                2 (1,0,0)
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 3, nnode = 4
!    real   (Rkind), parameter :: v0=0.0_Rkind, v1=1.0_Rkind   
!    integer(Ikind)            :: i, npnt   
!    real   (Rkind)            :: x, y, z
! !---------------------------------------------------------------------------------------------
!    
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       do i = 1, npnt   
!          x = localPnt(1,i); y = localPnt(2,i); z = localPnt(3,i)
!          
!          self%shapef(:  ,i) = [ v1 - x - y - z,   x,   y,   z ]
!          
!          self%derloc(1,:,i) = [    -v1         , v1,  v0,  v0 ]
!          self%derloc(2,:,i) = [        -v1     , v0,  v1,  v0 ]
!          self%derloc(3,:,i) = [            -v1 , v0,  v0,  v1 ]
!          
!          self%points(:  ,i) = localPnt(:,i)
!       end do
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'tetra4'; self%vtkCode = 10
!       self%nodCoord(1,:) = [ v0 , v1 , v0 , v0 ]
!       self%nodCoord(2,:) = [ v0 , v0 , v1 , v0 ]
!       self%nodCoord(3,:) = [ v0 , v0 , v0 , v1 ]
!    end if
! 
!    END SUBROUTINE elm_tetra4
!    
! 
! !=============================================================================================
!    SUBROUTINE elm_tetra10 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmTetra10_t),           intent(in out) :: self
!    real   (   Rkind    ), optional, intent(in    ) :: localPnt(:,:)   
!    logical              , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the (P2)
! !  quadratic tetrahedral element (10-noded)
! !
! !     Local node numbering and coordinates:
! !
! !
! !                      4
! !                      |
! !                      |
! !                      8     10
! !                      |
! !                  9   |____7______
! !                     /1           3
! !                    /
! !                   5
! !                  /     6
! !                 /
! !                2
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 3, nnode = 10
!    real   (Rkind), parameter :: v0=0.0_Rkind, v05=0.5_Rkind, v1=1.0_Rkind, &
!                                 v2=2.0_Rkind, v4=4.0_Rkind
!    integer(Ikind)            :: i, npnt   
!    real   (Rkind)            :: L1,x,y,z
! !---------------------------------------------------------------------------------------------
! 
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       do i = 1, npnt
!          x = localPnt(1,i); y = localPnt(2,i); z = localPnt(3,i); L1 = v1 - x - y - z
!       
!          self%shapef(1: 4  ,i) = [ L1*(v2*L1-v1), x*(v2*x-v1), y*(v2*y-v1), z*(v2*z-v1) ]
!          
!          self%shapef(5:10  ,i) = [ x*L1 , x*y , y*L1 , z*L1, z*x , y*z ] * v4
!          
!          self%derloc(1,1:4 ,i) = [ v1-v4*L1     , v4*x-v1    , v0         , v0      ]
!          self%derloc(2,1:4 ,i) = [ v1-v4*L1     , v0         , v4*y-v1    , v0      ]
!          self%derloc(3,1:4 ,i) = [ v1-v4*L1     , v0         , v0         , v4*z-v1 ]
!          
!          self%derloc(1,5:10,i) = [ L1-x , y  , -y    , -z    , z   , v0 ] * v4
!          self%derloc(2,5:10,i) = [-x    , x  ,  L1-y , -z    , v0  , z  ] * v4
!          self%derloc(3,5:10,i) = [-x    , v0 , -y    ,  L1-z , x   , y  ] * v4
!          
!          self%points(:     ,i) = localPnt(:,i)   
!       end do
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'tetra10'; self%vtkCode = 24
!       self%nodCoord(1,:) = [ v0, v1, v0, v0, v05, v05, v0 , v0 , v05, v0  ]
!       self%nodCoord(2,:) = [ v0, v0, v1, v0, v0 , v05, v05, v0 , v0 , v05 ]
!       self%nodCoord(3,:) = [ v0, v0, v0, v1, v0 , v0 , v0 , v05, v05, v05 ]
!    end if
! 
!    END SUBROUTINE elm_tetra10
!    
! 
! !=============================================================================================
!    SUBROUTINE elm_hexa8 ( self, localPnt, setInfo )
! !=============================================================================================   
!    class  (elmHexa8_t),           intent(in out) :: self
!    real   (   Rkind  ), optional, intent(in    ) :: localPnt(:,:)   
!    logical            , optional, intent(in    ) :: setInfo
! !--------------------------------------------------------------------------------------------- 
! !  Shape functions and their local derivatives at local coordinates "localPnt" for the (Q1)
! !  tri-linear element (8-noded)
! !
! !     Local node numbering and coordinates:
! !                      ___________
! !                    /|2         /|6
! !                   / |         / |
! !                  /  |        /  |
! !                 /   |       /   |
! !               3/____|______/7   |
! !                |    |______|____|
! !                |    /1     |    /5
! !                |   /       |   /
! !                |  /        |  /
! !                | /         | /
! !                |/__________|/
! !                4           8
! !
! !  If setInfo is present, set:
! !   - the physical dimension (%nDime), 
! !   - the number of node (%nNode),
! !   - the name of this element (%name),
! !   - its vtk code if appropriate (%vtkCode),
! !   - the local coordinates of its nodes (%nodCoord)
! !---------------------------------------------------------------------------------- RH 04/23 - 
! 
! !- local variables: --------------------------------------------------------------------------
!    integer(Ikind), parameter :: ndime = 3, nnode = 8
!    real   (Rkind), parameter :: v0125=0.125_Rkind, v1=1.0_Rkind
!    integer(Ikind)            :: i, npnt   
!    real   (Rkind)            :: x, y, z, xm, xp, ym, yp, zm, zp
! !---------------------------------------------------------------------------------------------
! 
!    if ( present(localPnt) ) then
!       npnt = size(localPnt,2)
!       call allocateShp ( self%shapef, self%derloc, self%points, ndime, nnode, npnt )
!       do i = 1, npnt   
!          x = localPnt(1,i); y = localPnt(2,i); z = localPnt(3,i)
!          
!          xm = v1-x; xp = v1+x; ym = v1-y; yp = v1+y; zm = v1-z; zp = v1+z
!          
!          self%shapef(:  ,i) = v0125 * [ xm*ym*zm, xm*ym*zp, xp*ym*zp, xp*ym*zm, &
!                                         xm*yp*zm, xm*yp*zp, xp*yp*zp, xp*yp*zm  ] 
!                              
!          self%derloc(1,:,i) = v0125 * [ -ym*zm, -ym*zp,  ym*zp,  ym*zm, &
!                                         -yp*zm, -yp*zp,  yp*zp,  yp*zm  ]
!          self%derloc(2,:,i) = v0125 * [ -xm*zm, -xm*zp, -xp*zp, -xp*zm, &
!                                          xm*zm,  xm*zp,  xp*zp,  xp*zm  ]
!          self%derloc(3,:,i) = v0125 * [ -xm*ym,  xm*ym,  xp*ym, -xp*ym, &
!                                         -xm*yp,  xm*yp,  xp*yp, -xp*yp  ]      
!                                    
!          self%points(:  ,i) = localPnt(:,i)
!       end do                            
!    end if
! 
!    if ( present(setInfo) ) then
!       if ( .not. allocated(self%nodCoord) ) allocate(self%nodCoord(ndime,nnode))      
!       self%nDime = ndime ; self%nNode = nnode; self%name = 'hexa8'; self%vtkCode = 12
!       self%nodCoord(1,:) = [ -v1,-v1, v1, v1,-v1,-v1, v1, v1 ]
!       self%nodCoord(2,:) = [ -v1,-v1,-v1,-v1, v1, v1, v1, v1 ]
!       self%nodCoord(3,:) = [ -v1, v1, v1,-v1,-v1, v1, v1,-v1 ]
!    end if
! 
!    END SUBROUTINE elm_hexa8
   

!=============================================================================================
   SUBROUTINE elm_inverseMap ( self, elcod, physPnt, localPnt, stat, niter )
!=============================================================================================   
   class  (elm_t),           intent(in out) :: self
   real   (Rkind),           intent(in    ) :: physPnt(:), elcod(:,:) 
   real   (Rkind),           intent(   out) :: localPnt(size(physPnt))   
   type   (err_t),           intent(in out) :: stat
   integer(Ikind), optional, intent(   out) :: niter
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_inverseMap'
   integer  (Ikind), parameter :: maxit = 20
   real     (Rkind), parameter :: tol = 1.0e-6   
   integer  (Ikind)            :: i, j, k, iter, nit
   real     (Rkind)            :: resid(self%nDime), dresid(self%nDime,self%nDime), &
                                  dPnt(self%nDime), eik, detJac, xloc(self%nDime,1)
   type     (shp_t)            :: shp
   character(MDSTR)            :: msg
!---------------------------------------------------------------------------------------------

   if ( size(physPnt) /= self%nDime ) then
      stat = err_t ( stat=UERROR, where=HERE, msg='Bad dimension of "physPnt" ('// &
                               util_intToChar(self%nDime)//' components expected)' )
      return
   end if
   
   if ( size(elcod,1) /= self%nDime .or. size(elcod,2) /= self%nNode ) then
      stat = err_t ( stat=UERROR, where=HERE, msg='Bad dimension of "elcod" (a ' // &
                     util_intToChar(self%nDime)//'x'//util_intToChar(self%nNode) // &
                     ' array expected)'                                             )
      return
   end if
   
   localPnt = RZERO ; nit = IZERO

   do iter = 1, maxit
!
!-    Shape functions and their local derivatives at localPnt:
!
      call self%computeShape ( localPnt = xloc, res = shp )
!
!-    The residual f = sum(shape*elcod) - pt:
    
      do i = 1, self%nDime
         resid(i) =-physPnt(i)
         do j = 1, self%nNode
            resid(i) = resid(i) + elcod(i,j) * shp%shapef(j,1)
         end do
      end do
!
!-    Its jacobian:
!   
      dresid = RZERO
      do k = 1, self%nNode
         do i = 1, self%nDime
            eik = elcod(i,k)
            do j = 1, self%nDime
               dresid(i,j) = dresid(i,j) + eik * shp%derloc(j,k,1)
            end do
         end do
      end do 
!
!-    New estimate of localPnt:
!            
      call elm_solvLin ( A = dresid, b = resid, x = dPnt, stat = stat, detA = detJac )
      error_TraceNreturn(stat>IZERO, HERE, stat) 
      
      if ( detJac <= RZERO ) then
         write(msg,'(g0)') detJac
         msg = 'Negative jacobian: jac = ' // trim(msg)
         stat = err_t ( stat = UERROR, where = HERE, msg = trim(msg))
         return
      end if
      
      xloc(:,1) = xloc(:,1) - dPnt
!
!-    Stopping criterion:
!
      if ( maxval(abs(dPnt)) <= tol * maxval(abs(xloc(:,1))) ) then
         nit = iter
         exit
      end if
   
   end do

   localPnt = xloc(:,1)
   
   if ( present(niter) ) niter = nit
   
   if ( nit == 0 ) then
      stat = err_t ( stat = WARNING, where = HERE, &
                      msg ='Stopping criterion not satisfied after maxit = ' // &
                      util_intToChar(maxit) // ' iterations' )
      return
   end if

   END SUBROUTINE elm_inverseMap   
   
         
!=============================================================================================
   SUBROUTINE elm_solvLin ( A, b, x, stat, detA )
!=============================================================================================
   real(Rkind),           intent(in    ) :: A(:,:)
   real(Rkind),           intent(in    ) :: b(:)
   real(Rkind),           intent(in out) :: x(:)
   type(err_t),           intent(in out) :: stat  
   real(Rkind), optional, intent(   out) :: detA
!---------------------------------------------------------------------------------------------
!  Computes the solution of the small n x n linear system A*x = b with 1 <= n <=3
!----------------------------------------------------------------------------------------- R.H.
 
!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_solvLin'
   real     (Rkind)            :: invdetA
   integer  (Ikind)            :: n, err
!---------------------------------------------------------------------------------------------                   

   n = size(A,1)
   if ( size(A,2) /= n ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'The matrix must be square')
      return
   end if              
   
   if ( size(b) /= n .or. size(x) /= n ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Incompatible sizes')
      return
   end if
   
   err = 0   
   select case ( n )
      case ( 1 )
         detA = a(1,1)
         if ( detA /= RZERO ) then
            x(1) = b(1) / detA
         else
            err = 1
         end if 
          
      case ( 2 )
         detA = a(1,1)*a(2,2) - a(1,2)*a(2,1)
         if ( detA /= 0 ) then
            invdetA = RONE / detA
            x(1) = ( a(2,2)*b(1) - a(1,2)*b(2) ) * invdetA
            x(2) = (-a(2,1)*b(1) + a(1,1)*b(2) ) * invdetA
         else
            err = 1
         end if

      case ( 3 )
         detA = a(1,1)*(a(2,2)*a(3,3) - a(3,2)*a(2,3)) +  &
                a(1,2)*(a(3,1)*a(2,3) - a(2,1)*a(3,3)) +  &
                a(1,3)*(a(2,1)*a(3,2) - a(3,1)*a(2,2))
                
         if ( detA /= 0 ) then
            invdetA = RONE / detA
            x(1) = ( ( a(2,2)*a(3,3) - a(2,3)*a(3,2) ) * b(1) + &
                     ( a(1,3)*a(3,2) - a(1,2)*a(3,3) ) * b(2) + &
                     ( a(1,2)*a(2,3) - a(1,3)*a(2,2) ) * b(3) ) * invdetA
                     
            x(2) = ( ( a(3,1)*a(2,3) - a(2,1)*a(3,3) ) * b(1) + &
                     ( a(1,1)*a(3,3) - a(1,3)*a(3,1) ) * b(2) + &
                     ( a(2,1)*a(1,3) - a(2,3)*a(1,1) ) * b(3) ) * invdetA
                     
            x(3) = ( ( a(2,1)*a(3,2) - a(3,1)*a(2,2) ) * b(1) + &
                     ( a(1,2)*a(3,1) - a(3,2)*a(1,1) ) * b(2) + &
                     ( a(1,1)*a(2,2) - a(1,2)*a(2,1) ) * b(3) ) * invdetA
                     
         else
            err = 1
         end if
        
      case default
         stat = err_t ( stat = UERROR, where = HERE, msg =              &
                'The matrix must be 1x1, 2x2 or 3x3 only (here n = ' // &
                 util_intToChar(n) // ')'                               )
   end select
    
   if ( err /= 0 ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Singular matrix')
      return
   end if 

   END SUBROUTINE elm_solvLin     


!=============================================================================================
   SUBROUTINE allocateShp ( N, dN, pts, ndime, nnode, npnt )
!=============================================================================================
   real   (Rkind), allocatable, intent(in out) :: N(:,:), dN(:,:,:), pts(:,:)
   integer(Ikind),              intent(in    ) :: ndime, nnode, npnt
!---------------------------------------------------------------------------------------------
!
!----------------------------------------------------------------------------------------- R.H.

   if ( allocated(N) ) then
      if ( size(N,1) /= nnode .or. size(N,2) /= npnt ) then
         deallocate(N) ; allocate(N(nnode,npnt))
      end if
   else
      allocate(N(nnode,npnt))
   end if
   if ( allocated(dN) ) then
      if ( size(dN,1) /= ndime .or. size(dN,2) /= nnode .or. size(dN,3) /= npnt ) then
         deallocate(dN) ; allocate(dN(ndime,nnode,npnt))
      end if
   else
      allocate(dN(ndime,nnode,npnt))
   end if
   if ( allocated(pts) ) then
      if ( size(pts,1) /= ndime .or. size(pts,2) /= npnt ) then
         deallocate(pts) ; allocate(pts(ndime,npnt))
      end if
   else
      allocate(pts(ndime,npnt))
   end if
   
   END SUBROUTINE allocateShp
   
END MODULE elm_m   