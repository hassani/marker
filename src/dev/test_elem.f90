! nagfor -kind=byte -L$(dirlibpk2) -lpk2 -I $(dirmodpk2) element.f90 test_elem.f90
#include "error.fpp"

use element_m
use pk2mod_m

implicit none

integer(Ikind), parameter :: nd=2, nn=6
character(len=*), parameter :: namelem = "tria06"

real(Rkind) :: elcod(nd,nn), pt(nd), xi(nd), dxi(nd), shape(nn), deriv(nd,nn), df(nd,nd), f(nd), tol=1e-6, t0, eip
type(err_t) :: stat
real(Rkind) :: A(3,3), b(3), x(3)

integer(Ikind) :: maxit=2, k,i,j,p,k0,cas,kt
real(Rkind) :: t1,t2

elcod(:,1) = [0,0]; elcod(:,2) = [1,0] ; elcod(:,3) = [-0.2,1.2]
elcod(:,4) = [0.5,0.0]; elcod(:,5)=[0.6,0.6]; elcod(:,6)=[0.0,0.5]

pt = [1.0_Rkind, 2.0_Rkind ] / 3.0_Rkind



xi = 0
k0 = 0

do k = 1, maxit
!
!- Shape functions and their local derivatives at xi:
!
   call element_driver (namelem=namelem, ndime=nd, nnode=nn, pnt=xi, shp=shape, drv=deriv)
!
!- The residual f = sum(shape*elcod) - pt:
!
   do i = 1, nd
      f(i) =-pt(i)
      do j = 1, nn
         f(i) = f(i) + elcod(i,j)*shape(j)
      end do
   end do
!
!- Its jacobian:
!   
   df = 0.0
   do p = 1, nn
      do i = 1, nd
         eip = elcod(i,p)
         do j = 1, nd
            df(i,j) = df(i,j) + eip*deriv(j,p)
         end do
      end do
   end do
!
!- New estimate of xi:
!               
   call inverse ( df, f, dxi, stat )
   xi = xi - dxi
!
!- Stopping criteria:
!
   if ( maxval(abs(dxi)) <= tol * maxval(abs(xi)) ) then
      k0 = k
      exit
   end if
   
end do

if ( k0 == 0 ) then
   print*,'Non converged'
end if

print*,'k0, xi, f=',k0, xi, f

contains

!=============================================================================================
   subroutine inverse ( A, b, x, stat )
!=============================================================================================
   implicit none
   real(Rkind), intent(in    ) :: A(:,:)
   real(Rkind), intent(in    ) :: b(:)
   real(Rkind), intent(in out) :: x(:)
   type(err_t), intent(in out) :: stat   
!---------------------------------------------------------------------------------------------
!  Computes the solution of the small n x n linear system A*x = b with 1 <= n <=3
!----------------------------------------------------------------------------------------- R.H.
 
!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'inverse'
   real     (Rkind)            :: detA, invdetA
   integer  (Ikind)            :: i, n, err
!---------------------------------------------------------------------------------------------                   

   n = size(A,1)
   if ( size(A,2) /= n ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'The matrix must be square')
      return
   end if              
   
   if ( size(b) /= n .or. size(x) /= n ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Incompatible sizes')
      return
   end if
   
   err = 0   
   select case(n)
      case(1)
         if ( a(1,1) /= RZERO ) then
            x(1) = b(1) / a(1,1)
         else
            err = 1
         end if 
          
      case(2)
         detA = a(1,1)*a(2,2) - a(1,2)*a(2,1)
         if ( detA /= 0 ) then
            invdetA = RONE / detA
            x(1) = ( a(2,2)*b(1) - a(1,2)*b(2) ) * invdetA
            x(2) = (-a(2,1)*b(1) + a(1,1)*b(2) ) * invdetA
         else
            err = 1
         end if

      case(3)
         detA = a(1,1)*(a(2,2)*a(3,3) - a(3,2)*a(2,3)) +  &
                a(1,2)*(a(3,1)*a(2,3) - a(2,1)*a(3,3)) +  &
                a(1,3)*(a(2,1)*a(3,2) - a(3,1)*a(2,2))
                
         if ( detA /= 0 ) then
            invdetA = RONE / detA
            x(1) = ( ( a(2,2)*a(3,3) - a(2,3)*a(3,2) ) * b(1) + &
                     ( a(1,3)*a(3,2) - a(1,2)*a(3,3) ) * b(2) + &
                     ( a(1,2)*a(2,3) - a(1,3)*a(2,2) ) * b(3) ) * invdetA
                     
            x(2) = ( ( a(3,1)*a(2,3) - a(2,1)*a(3,3) ) * b(1) + &
                     ( a(1,1)*a(3,3) - a(1,3)*a(3,1) ) * b(2) + &
                     ( a(2,1)*a(1,3) - a(2,3)*a(1,1) ) * b(3) ) * invdetA
                     
            x(3) = ( ( a(2,1)*a(3,2) - a(3,1)*a(2,2) ) * b(1) + &
                     ( a(1,2)*a(3,1) - a(3,2)*a(1,1) ) * b(2) + &
                     ( a(1,1)*a(2,2) - a(1,2)*a(2,1) ) * b(3) ) * invdetA
                     
         else
            err = 1
         end if
        
      case default
         stat = err_t ( stat = UERROR, where = 'element_inverse', msg = &
                'The matrix must be 1x1, 2x2 or 3x3 only (here n = ' // &
                 util_intToChar(n) // ')')
   end select
    
   if ( err /= 0 ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Singular matrix')
      return
   end if 

   end subroutine inverse   
end

