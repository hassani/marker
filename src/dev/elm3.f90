#include "error.fpp"

MODULE elm_m
   
   use pk2mod_m, only: Ikind, Rkind, UERROR, WARNING, IZERO, RZERO, RONE, MDSTR, &
                       err_t, i2a => util_intToChar
   
   implicit none
   
   private
   public :: elm_t, shp_t, elm_init
   
   type :: shp_t
      real(Rkind), allocatable :: shapef(:), derloc(:,:), points(:)
   end type shp_t
!
!- A DT for an element
!   
   type, abstract :: elm_t
      logical                       :: is_init = .false.
      ! informations about the element:
      character(len=:), allocatable :: nameElement, nameSimplex
      integer  (Ikind)              :: nDime = 0, nNode = 0, vtkCode = 0
      real     (Rkind), allocatable :: nodCoord(:,:)
   contains
      ! method for setting these informations:
      procedure(interf_setInfo), deferred, pass(self) :: setInfo   
      ! method for the calculation of shape functions and their local derivatives
      procedure(interf_shape  ), deferred, nopass :: computeShape
      ! method for the inverse mapping:
      procedure, pass(self) :: inverseMap => elm_inverseMap   
   end type elm_t   

   type, extends(elm_t) :: elmEdge2_t
   contains
      procedure, nopass     :: computeShape => elm_edge2Shape      
      procedure, pass(self) :: setInfo      => elm_edge2Info      
   end type elmEdge2_t   

   type, extends(elm_t) :: elmEdge3_t
   contains
      procedure, nopass     :: computeShape => elm_edge3Shape      
      procedure, pass(self) :: setInfo      => elm_edge3Info                
   end type elmEdge3_t   
   
   type, extends(elm_t) :: elmTria3_t
   contains
      procedure, nopass     :: computeShape => elm_tria3Shape      
      procedure, pass(self) :: setInfo      => elm_tria3Info                       
   end type elmTria3_t   

   type, extends(elm_t) :: elmTria4_t
   contains
      procedure, nopass     :: computeShape => elm_tria4Shape   
      procedure, pass(self) :: setInfo      => elm_tria4Info                                
   end type elmTria4_t   

   type, extends(elm_t) :: elmTria6_t
   contains
      procedure, nopass     :: computeShape => elm_tria6Shape   
      procedure, pass(self) :: setInfo      => elm_tria6Info                                
   end type elmTria6_t      

   type, extends(elm_t) :: elmTria7_t
   contains
      procedure, nopass     :: computeShape => elm_tria7Shape   
      procedure, pass(self) :: setInfo      => elm_tria7Info                                
   end type elmTria7_t   
   
   type, extends(elm_t) :: elmQuad4_t
   contains
      procedure, nopass     :: computeShape => elm_quad4Shape   
      procedure, pass(self) :: setInfo      => elm_quad4Info                                
   end type elmQuad4_t 

   type, extends(elm_t) :: elmQuad8_t
   contains
      procedure, nopass     :: computeShape => elm_quad8Shape   
      procedure, pass(self) :: setInfo      => elm_quad8Info                                
   end type elmQuad8_t 
   
   type, extends(elm_t) :: elmQuad9_t
   contains
      procedure, nopass     :: computeShape => elm_quad9Shape   
      procedure, pass(self) :: setInfo      => elm_quad9Info                                
   end type elmQuad9_t 

   type, extends(elm_t) :: elmTetra4_t
   contains
      procedure, nopass     :: computeShape => elm_tetra4Shape   
      procedure, pass(self) :: setInfo      => elm_tetra4Info                                
   end type elmTetra4_t 

   type, extends(elm_t) :: elmTetra10_t
   contains
      procedure, nopass     :: computeShape => elm_tetra10Shape   
      procedure, pass(self) :: setInfo      => elm_tetra10Info                                
   end type elmTetra10_t 

   type, extends(elm_t) :: elmHexa8_t
   contains
      procedure, nopass     :: computeShape => elm_hexa8Shape   
      procedure, pass(self) :: setInfo      => elm_hexa8Info                                
   end type elmHexa8_t 
                        
   abstract interface
      subroutine interf_shape ( localPnt, interpo, stat ) 
         import :: Rkind, shp_t, err_t
         real (Rkind),              intent(in    ) :: localPnt(:,:)
         type (shp_t), allocatable, intent(in out) :: interpo(:)
         type (err_t),              intent(in out) :: stat
      end subroutine interf_shape    
      
      subroutine interf_setInfo ( self ) 
         import :: elm_t
         class(elm_t), intent(out) :: self 
      end subroutine interf_setInfo          
   end interface

   ! constructor/initializer:
   interface elm_t
      module procedure elm_finit
   end interface
          
CONTAINS

            
!=============================================================================================
   FUNCTION elm_finit ( namelem, vtkCode, stat ) result ( res )
!=============================================================================================   
   character(len=*), optional,   intent(in    ) :: namelem
   integer  (Ikind), optional,   intent(in    ) :: vtkCode
   type     (err_t),             intent(in out) :: stat
   class    (elm_t), allocatable                :: res
!--------------------------------------------------------------------------------------------- 
!  Initializes an elm_t variables according to the element name or its vtk code.
!---------------------------------------------------------------------------------- RH 04/23 - 
   
   call elm_init ( res, stat, namelem, vtkCode )
      
   END FUNCTION elm_finit


!=============================================================================================
   SUBROUTINE elm_init ( res, stat, namelem, vtkCode )
!=============================================================================================   
   class    (elm_t), allocatable, intent(in out) :: res
   type     (err_t),              intent(in out) :: stat
   character(len=*), optional,    intent(in    ) :: namelem
   integer  (Ikind), optional,    intent(in    ) :: vtkCode
!--------------------------------------------------------------------------------------------- 
!  Initializes an elm_t variables according to the element name or its vtk code.
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter   :: HERE = 'elm_init'
   character(len=:), allocatable :: name
!---------------------------------------------------------------------------------------------  
   
   if ( present(vtkCode) ) then
      name = elm_fromVtkCodeToElementName ( vtkCode, stat )
      error_TraceNreturn(stat>IZERO, HERE, stat) 
   else if ( present(namelem) ) then
      name = trim(adjustl(namelem))
   else
      stat = err_t ( msg = 'Missing "namelem" or "vtkCode"', stat = UERROR, where = HERE )  
      return
   end if
   
   if ( allocated(res) ) then
      if ( res%is_init .and. res%nameElement == name ) then
         return
      else
         deallocate(res)
      end if
   end if
   
   select case ( name )
      case ("edge2")           
         allocate(elmEdge2_t :: res)  
       case ("edge3")           
          allocate(elmEdge3_t :: res)  
       case ("tria3")           
          allocate(elmTria3_t :: res)  
       case ("tria4")           
          allocate(elmTria4_t :: res)           
       case ("tria6")           
          allocate(elmTria6_t :: res)  
      case ("tria7")           
         allocate(elmTria7_t :: res)                   
       case ("quad4")           
          allocate(elmQuad4_t :: res)                   
      case ("quad8")           
         allocate(elmQuad8_t :: res)                   
       case ("quad9")           
          allocate(elmQuad9_t :: res)                   
       case ("tetra4")           
          allocate(elmTetra4_t :: res)                   
       case ("tetra10")           
          allocate(elmTetra10_t :: res)   
       case ("hexa8")           
          allocate(elmHexa8_t :: res) 
                     
      case default
         stat = err_t ( stat=UERROR, where='msh_init', msg='Unknown element "'//name//'"')
         return
   end select
   
   call res%setInfo (  )
   
   res%is_init = .true.
      
   END SUBROUTINE elm_init   


!=============================================================================================
   FUNCTION elm_fromVtkCodeToElementName ( vtkCode, stat ) result ( name )
!=============================================================================================
   integer  (Ikind), intent(in    ) :: vtkCode
   type     (err_t), intent(in out) :: stat      
   character(len=:), allocatable    :: name
!--------------------------------------------------------------------------------------------- 
!  Mapping between certain VTK codes and the corresponding element names in this library
!---------------------------------------------------------------------------------- RH 04/23 -   

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_fromVtkCodeToElementName'
!---------------------------------------------------------------------------------------------   

   select case (vtkCode)
      case ( 3 ) ! VTK_LINE
         name = 'edge2' 
      case ( 21 ) ! VTK_QUADRATIC_EDGE
         name = 'edge3'
      case ( 5 ) ! VTK_TRIANGLE
         name = 'tria3'
      case ( 22 ) ! VTK_QUADRATIC_TRIANGLE
         name = 'tria6'
      case ( 9 ) ! VTK_QUAD 
         name = 'quad4'
      case ( 23 ) ! VTK_QUADRATIC_QUAD
         name = 'quad8'
      case ( 10 ) ! VTK_TETRA
         name = 'tetra4'
      case ( 24 ) ! VTK_QUADRATIC_TETRA
         name = 'tetra10'
      case ( 12 ) ! VTK_HEXAHEDRON
         name = 'hexa8'
      case ( 25 ) ! VTK_QUADRATIC_HEXAHEDRON
         name = 'hexa20'
      case default
         stat = err_t ( stat=UERROR, where=HERE, msg='element of vtk code ' //  &
                        i2a(vtkCode)//' not present in this library' )
   end select

   END FUNCTION elm_fromVtkCodeToElementName


!=============================================================================================
   SUBROUTINE elm_edge2Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at a set of local points (localPnt) for the (P1)
!  linear egde element (2-noded)
!
!     Local node numbering and coordinates:
!
!            1                   2
!            *-------------------*
!           -1                  +1
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_edge2Shape'
   integer  (Ikind), parameter :: ndime = 1, nnode = 2
   real     (Rkind)            :: x
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i)  
      call elm_P1_1d ( x, interpo(i)%shapef, interpo(i)%derloc )
      interpo(i)%points = x   
   end do
      
   END SUBROUTINE elm_edge2Shape
   
!=============================================================================================
   SUBROUTINE elm_edge2Info ( self )
!=============================================================================================   
   class(elmEdge2_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  Sets:
!   - the physical dimension (%nDime), 
!   - the number of node (%nNode),
!   - the names of the element (%nameElement) and its simplex (%nameSimplex),
!   - its vtk code if appropriate (%vtkCode),
!   - the local coordinates of its nodes (%nodCoord)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------

   self%nDime = 1 ; self%nNode = 2   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'edge2' ; self%nameSimplex = 'edge2' ; self%vtkCode = 3
   self%nodCoord(1,:) = [ -RONE , RONE ]
      
   END SUBROUTINE elm_edge2Info


!=============================================================================================
   SUBROUTINE elm_edge3Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the (P2)
!  quadratic egde element (3-noded)
!
!     Local node numbering and coordinates:
!
!            1         3         2
!            *-------- * --------*  
!           -1         0        +1  
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_edge3Shape'
   integer  (Ikind), parameter :: ndime = 1, nnode = 3
   real     (Rkind)            :: x
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i)  
      call elm_P2_1d ( x, interpo(i)%shapef, interpo(i)%derloc )
      interpo(i)%points = x   
   end do
      
   END SUBROUTINE elm_edge3Shape

!=============================================================================================
   SUBROUTINE elm_edge3Info ( self )
!=============================================================================================   
   class(elmEdge3_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------

   self%nDime = 1 ; self%nNode = 3   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'edge3'; ; self%nameSimplex = 'edge2' ; self%vtkCode = 21
   self%nodCoord(1,:) = [ -RONE , RONE , RZERO ]
      
   END SUBROUTINE elm_edge3Info
   

!=============================================================================================
   SUBROUTINE elm_tria3Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the (P1)
!  linear triangle (3-noded)
!
!     Local node numbering and coordinates:
!
!                3 (0,1)
!                . .
!                .   .
!                .     .
!                .       .              y
!                .         .            .
!                1 . . . . . 2          .. x
!              (0,0)       (1,0)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_tria3Shape'
   integer  (Ikind), parameter :: ndime = 2, nnode = 3
   real     (Rkind)            :: x, y
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i) ; y = localPnt(2,i)
      call elm_P1_2d ( x, y, interpo(i)%shapef, interpo(i)%derloc )
      interpo(i)%points = [ x, y ]   
   end do
      
   END SUBROUTINE elm_tria3Shape

!=============================================================================================
   SUBROUTINE elm_tria3Info ( self )
!=============================================================================================   
   class(elmTria3_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------

   self%nDime = 2 ; self%nNode = 3   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'tria3'; ; self%nameSimplex = 'tria3' ; self%vtkCode = 5
   self%nodCoord(1,:) = [ RZERO , RONE  , RZERO ]
   self%nodCoord(2,:) = [ RZERO , RZERO , RONE  ]   
      
   END SUBROUTINE elm_tria3Info

!=============================================================================================
   SUBROUTINE elm_tria4Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the P1
!  triangle with additional bubble function (4-noded)
!
!     Local node numbering and coordinates:
!
!                3 (0,1)
!                . .
!                .   .
!                .     .
!                .   4 (1/3,1/3)        y
!                .         .            .
!                1 . . . . . 2          .. x
!              (0,0)       (1,0)
!
!
!      The shape functions are:
!
!             L_i -  9 * bubble    i = 1,2,3
!                   27 * bubble    i = 4
!
!       where the L_i are the P1 shape functions abd bubble = L_1 * L_2 * L_3
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_tria4Shape'
   integer  (Ikind), parameter :: ndime = 2, nnode = 4
   real     (Rkind), parameter :: three = 3.0_Rkind, nine = 9.0_Rkind
   real     (Rkind)            :: x, y, b9, bx9, by9, L(3)
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i) ; y = localPnt(2,i)   
!
!-    P1 shape functions:
!
      call elm_P1_2d ( x, y, L ) 
!
!-    bubble (*9) and its derivatives (*9):
!
      b9  = nine * L(1) * L(2) * L(3)
      bx9 = nine * L(3) * (L(1) - L(2))
      by9 = nine * L(2) * (L(1) - L(3))
!
!-    shape functions and their derivatives:
! 
      interpo(i)%shapef(1:3) = L - b9
      interpo(i)%shapef( 4 ) = three * b9
        
      interpo(i)%derloc(1,:) = [ -RONE - bx9 , RONE - bx9 ,       -bx9 , three*bx9 ]
      interpo(i)%derloc(2,:) = [ -RONE - by9 ,      - by9 , RONE - by9 , three*by9 ]

      interpo(i)%points = [ x, y ]   
   end do
      
   END SUBROUTINE elm_tria4Shape

!=============================================================================================
   SUBROUTINE elm_tria4Info ( self )
!=============================================================================================   
   class(elmTria4_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: three = 3.0_Rkind
!---------------------------------------------------------------------------------------------

   self%nDime = 2 ; self%nNode = 4   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'tria4'; ; self%nameSimplex = 'tria3' ; self%vtkCode = -999
   self%nodCoord(1,:) = [ RZERO , RONE  , RZERO, RONE/three ]
   self%nodCoord(2,:) = [ RZERO , RZERO , RONE , RONE/three  ]   

   END SUBROUTINE elm_tria4Info


!=============================================================================================
   SUBROUTINE elm_tria6Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the (P2)
!  quadratic triangle (6-noded)
!
!     Local node numbering and coordinates :
!
!                3 (0,1)
!                . .
!                .   .
!                6     5
!                .       .               y
!                .         .             .
!                1 . . 4. . . 2          .. x
!              (0,0)        (1,0)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_tria6Shape'
   integer  (Ikind), parameter :: ndime = 2, nnode = 6
   real     (Rkind)            :: x, y
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i) ; y = localPnt(2,i)   
      call elm_P2_2d ( x, y, interpo(i)%shapef, interpo(i)%derloc )
      interpo(i)%points = [ x, y ]   
   end do
      
   END SUBROUTINE elm_tria6Shape

!=============================================================================================
   SUBROUTINE elm_tria6Info ( self )
!=============================================================================================   
   class(elmTria6_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: half = 0.5_Rkind
!---------------------------------------------------------------------------------------------

   self%nDime = 2 ; self%nNode = 6   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'tria6'; ; self%nameSimplex = 'tria3' ; self%vtkCode = 22
   self%nodCoord(1,:) = [ RZERO, RONE , RZERO, half , half, RZERO ]
   self%nodCoord(2,:) = [ RZERO, RZERO, RONE , RZERO, half, half  ]
   
   END SUBROUTINE elm_tria6Info
   

!=============================================================================================
   SUBROUTINE elm_tria7Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the 
!  P2-bubble triangle (7-noded)
!
!     Local node numbering and coordinates :
!
!                3 (0,1)
!                . .
!                .   .
!                6     5
!                .   7   .               y
!                .         .             .
!                1 . . 4. . . 2          .. x
!              (0,0)        (1,0)
!
!      The shape functions are:
!
!             N_i +  3 * bubble    i = 1,2,3
!             N_i - 12 * bubble    i = 4,5,6
!                   27 * bubble    i = 7
!
!      where N_i are the shape functions of the P2 element and bubble is 
!                 bubble = L_1 * L_2 * L_3 = (1 - x - y) * x * y
!      with L_i being the shape functions of the P1 element 
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_tria7Shape'
   integer  (Ikind), parameter :: ndime = 2, nnode = 7
   real     (Rkind), parameter :: two = 2.0_Rkind, three = 3.0_Rkind, twelve = 12.0_Rkind, &
                                  v27 = 27.0_Rkind
   real     (Rkind), parameter :: c(6) = [three, three, three, -twelve,-twelve,-twelve]
   real     (Rkind)            :: x, y, b, db_dx, db_dy, N(6), dN(2,6)
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i) ; y = localPnt(2,i) 
        
      b = (RONE - x - y)*x*y; db_dx = (RONE - two*x - y)*y; db_dy = (RONE - two*y - x)*x
                
      call elm_P2_2d ( x, y, N, dN )
 
      interpo(i)%shapef(1:6  ) = b * c + N
      interpo(i)%shapef(  7  ) = b * v27 
                                 
      interpo(i)%derloc(1,1:6) = db_dx * c + dN(1,:)  
      interpo(i)%derloc(2,1:6) = db_dy * c + dN(2,:)  
      interpo(i)%derloc(1,  7) = db_dx * v27 
      interpo(i)%derloc(2,  7) = db_dy * v27  

      interpo(i)%points = [ x, y ]   
   end do
      
   END SUBROUTINE elm_tria7Shape

!=============================================================================================
   SUBROUTINE elm_tria7Info ( self )
!=============================================================================================   
   class(elmTria7_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: half = 0.5_Rkind, three = 3.0_Rkind
!---------------------------------------------------------------------------------------------

   self%nDime = 2 ; self%nNode = 7   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'tria7'; ; self%nameSimplex = 'tria3' ; self%vtkCode = -999
   self%nodCoord(1,:) = [ RZERO, RONE , RZERO, half , half, RZERO, RONE/three ]
   self%nodCoord(2,:) = [ RZERO, RZERO, RONE , RZERO, half, half , RONE/three ]
   
   END SUBROUTINE elm_tria7Info


!=============================================================================================
   SUBROUTINE elm_quad4Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the (Q1)
!  bilinear quad (4-noded)
!
!     Local node numbering and coordinates:
!
!                     (-1,1)          (1,1)
!                         4------------3
!                         |      y     |
!                         |      .     |
!                         |      . .x  |
!                         |            |
!                         |            |
!                         1------------2
!                      (-1,-1)       (1,-1)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_quad4Shape'
   integer  (Ikind), parameter :: ndime = 2, nnode = 4
   real     (Rkind)            :: x, y
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i) ; y = localPnt(2,i)
      call elm_Q1_2d ( x, y, interpo(i)%shapef, interpo(i)%derloc )
      interpo(i)%points = [ x, y ]   
   end do
      
   END SUBROUTINE elm_quad4Shape

!=============================================================================================
   SUBROUTINE elm_quad4Info ( self )
!=============================================================================================   
   class(elmQuad4_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------

   self%nDime = 2 ; self%nNode = 4   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'quad4'; ; self%nameSimplex = 'quad4' ; self%vtkCode = 9
   self%nodCoord(1,:) = [ -RONE, RONE, RONE,-RONE ]
   self%nodCoord(2,:) = [ -RONE,-RONE, RONE, RONE ]   

   END SUBROUTINE elm_quad4Info 


!=============================================================================================
   SUBROUTINE elm_quad8Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the (Q2)
!  quadratic quad (8-noded)
!
!     Local node numbering and coordinates:
!
!                     (-1,1)          (1,1)
!                         4------7-----3
!                         |      y     |
!                         |      .     |
!                         8      . .x  6 
!                         |            |
!                         |            |
!                         1------5-----2
!                      (-1,-1)       (1,-1)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_quad8Shape'
   integer  (Ikind), parameter :: ndime = 2, nnode = 8
   real     (Rkind), parameter :: two = 2.0_Rkind
   real     (Rkind)            :: x, y, N(4), dN(2,4), c1(4), c2(4)
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i) ; y = localPnt(2,i)

      c1 = [ RONE + x + y , RONE - x + y , RONE - x - y , RONE + x - y ]
      c2 = [ RONE + x     , RONE + y     , RONE - x     , RONE - y     ]
      
      call elm_Q1_2d ( x, y, N, dN ) 
          
      interpo(i)%shapef(1:4  ) =-N * c1
      interpo(i)%shapef(5:8  ) = N * c2 * two 
          
      interpo(i)%derloc(1,1:4) =  -dN(1,:) * c1 - N * [ RONE ,-RONE ,-RONE , RONE  ]
      interpo(i)%derloc(2,1:4) =  -dN(2,:) * c1 - N * [ RONE , RONE ,-RONE ,-RONE  ]
      interpo(i)%derloc(1,5:8) = ( dN(1,:) * c2 + N * [ RONE , RZERO,-RONE , RZERO ] ) * two
      interpo(i)%derloc(2,5:8) = ( dN(2,:) * c2 + N * [ RZERO, RONE , RZERO,-RONE  ] ) * two

      interpo(i)%points = [ x, y ]   
   end do
      
   END SUBROUTINE elm_quad8Shape

!=============================================================================================
   SUBROUTINE elm_quad8Info ( self )
!=============================================================================================   
   class(elmQuad8_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------

   self%nDime = 2 ; self%nNode = 8   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'quad8'; ; self%nameSimplex = 'quad4' ; self%vtkCode = 23
   self%nodCoord(1,:) = [ -RONE, RONE, RONE,-RONE, RZERO, RONE , RZERO,-RONE  ]
   self%nodCoord(2,:) = [ -RONE,-RONE, RONE, RONE,-RONE , RZERO, RONE , RZERO ]   

   END SUBROUTINE elm_quad8Info 


!=============================================================================================
   SUBROUTINE elm_quad9Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the (Q2)
!  quadratic quad (9-noded)
!
!     Local node numbering and coordinates:
!
!                     (-1,1)          (1,1)
!                         4------7-----3
!                         |      y     |
!                         |      .     |
!                         8      9 .x  6
!                         |            |
!                         |            |
!                         1------5-----2
!                      (-1,-1)       (1,-1)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_quad9Shape'
   integer  (Ikind), parameter :: ndime = 2, nnode = 9
   real     (Rkind), parameter :: two = 2.0_Rkind
   real     (Rkind)            :: x, y, Lx(3), dLx(3), Ly(3), dLy(3)
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i) ; y = localPnt(2,i)

      call elm_P2_1d ( x, Lx, dLx )
      call elm_P2_1d ( y, Ly, dLy )
      
      interpo(i)%shapef(1:4  ) = [ Lx(1)*Ly(1), Lx(2)*Ly(1), Lx(2)*Ly(2), Lx(1)*Ly(2) ] 
      interpo(i)%shapef(5:8  ) = [ Lx(3)*Ly(1), Lx(2)*Ly(3), Lx(3)*Ly(2), Lx(1)*Ly(3) ]
      interpo(i)%shapef(  9  ) =   Lx(3)*Ly(3)   

      interpo(i)%derloc(1,1:4) = [ dLx(1)*Ly(1), dLx(2)*Ly(1), dLx(2)*Ly(2), dLx(1)*Ly(2) ]
      interpo(i)%derloc(2,1:4) = [ dLy(1)*Lx(1), dLy(1)*Lx(2), dLy(2)*Lx(2), dLy(2)*Lx(1) ]
          
      interpo(i)%derloc(1,5:8) = [ dLx(3)*Ly(1), dLx(2)*Ly(3), dLx(3)*Ly(2), dLx(1)*Ly(3) ]
      interpo(i)%derloc(2,5:8) = [ dLy(1)*Lx(3), dLy(3)*Lx(2), dLy(2)*Lx(3), dLy(3)*Lx(1) ]
          
      interpo(i)%derloc(1,9  ) = dLx(3)*Ly(3)
      interpo(i)%derloc(2,9  ) = dLy(3)*Lx(3)  

      interpo(i)%points = [ x, y ]   
   end do
      
   END SUBROUTINE elm_quad9Shape

!=============================================================================================
   SUBROUTINE elm_quad9Info ( self )
!=============================================================================================   
   class(elmQuad9_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------

   self%nDime = 2 ; self%nNode = 9   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'quad9'; ; self%nameSimplex = 'quad4' ; self%vtkCode = -999
   self%nodCoord(1,:) = [ -RONE, RONE, RONE,-RONE, RZERO, RONE , RZERO,-RONE , RZERO ]
   self%nodCoord(2,:) = [ -RONE,-RONE, RONE, RONE,-RONE , RZERO, RONE , RZERO, RZERO ]   

   END SUBROUTINE elm_quad9Info 


!=============================================================================================
   SUBROUTINE elm_tetra4Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the (P1)
!  linear tetrahedral element (4-noded)
!
!     Local node numbering and coordinates:
!
!
!                      4 (0,0,1)
!                      |
!                      |
!                      |
!                      |
!                      |____________ 3 (0,1,0)
!                     / 1 (0,0,0)
!                    /
!                   /
!                  /
!                 /
!                2 (1,0,0)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_tetra4Shape'
   integer  (Ikind), parameter :: ndime = 3, nnode = 4
   real     (Rkind)            :: x, y, z
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i) ; y = localPnt(2,i) ; z = localPnt(3,i)
      call elm_P1_3d ( x, y, z, interpo(i)%shapef, interpo(i)%derloc )
      interpo(i)%points = [ x, y, z ]   
   end do
      
   END SUBROUTINE elm_tetra4Shape

!=============================================================================================
   SUBROUTINE elm_tetra4Info ( self )
!=============================================================================================   
   class(elmTetra4_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------

   self%nDime = 3 ; self%nNode = 4   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'tetra4'; ; self%nameSimplex = 'tetra4' ; self%vtkCode = 10
   self%nodCoord(1,:) = [ RZERO, RONE , RZERO, RZERO ]
   self%nodCoord(2,:) = [ RZERO, RZERO, RONE , RZERO ]
   self%nodCoord(3,:) = [ RZERO, RZERO, RZERO, RONE ]

   END SUBROUTINE elm_tetra4Info 


!=============================================================================================
   SUBROUTINE elm_tetra10Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the (P2)
!  quadratic tetrahedral element (10-noded)
!
!     Local node numbering and coordinates:
!
!
!                      4
!                      |
!                      |
!                      8     10
!                      |
!                  9   |____7______
!                     /1           3
!                    /
!                   5
!                  /     6
!                 /
!                2
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_tetra10Shape'
   integer  (Ikind), parameter :: ndime = 3, nnode = 10
   real     (Rkind)            :: x, y, z
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i) ; y = localPnt(2,i) ; z = localPnt(3,i)
      call elm_P2_3d ( x, y, z, interpo(i)%shapef, interpo(i)%derloc )
      interpo(i)%points = [ x, y, z ]   
   end do
      
   END SUBROUTINE elm_tetra10Shape

!=============================================================================================
   SUBROUTINE elm_tetra10Info ( self )
!=============================================================================================   
   class(elmTetra10_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: half = 0.5_Rkind
!---------------------------------------------------------------------------------------------

   self%nDime = 3 ; self%nNode = 10   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'tetra10'; ; self%nameSimplex = 'tetra4' ; self%vtkCode = 24
   self%nodCoord(1,:) = [RZERO, RONE , RZERO, RZERO, half , half , RZERO, RZERO, half , RZERO]
   self%nodCoord(2,:) = [RZERO, RZERO, RONE , RZERO, RZERO, half , half , RZERO, RZERO, half ]
   self%nodCoord(3,:) = [RZERO, RZERO, RZERO, RONE , RZERO, RZERO, RZERO, half , half , half ]
   
   END SUBROUTINE elm_tetra10Info 


!=============================================================================================
   SUBROUTINE elm_hexa8Shape ( localPnt, interpo, stat )
!=============================================================================================   
   real (  Rkind   ),              intent(in    ) :: localPnt(:,:) 
   type (  shp_t   ), allocatable, intent(in out) :: interpo(:)  
   type (  err_t   ),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Shape functions and their local derivatives at local coordinates "localPnt" for the (Q1)
!  tri-linear element (8-noded)
!
!     Local node numbering and coordinates:
!                      ___________
!                    /|2         /|6
!                   / |         / |
!                  /  |        /  |
!                 /   |       /   |
!               3/____|______/7   |
!                |    |______|____|
!                |    /1     |    /5
!                |   /       |   /
!                |  /        |  /
!                | /         | /
!                |/__________|/
!                4           8
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_hexa8Shape'
   integer  (Ikind), parameter :: ndime = 3, nnode = 8
   real     (Rkind)            :: x, y, z
   integer  (Ikind)            :: i, npnt
!---------------------------------------------------------------------------------------------
   
   if ( size(localPnt,1) /= ndime ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Expected point(s) of ' //   &
                     i2a(ndime)//'D space instead of ' // i2a(size(localPnt,1)) //'D')
      return
   end if
   
   npnt = size(localPnt,2)
   call allocateShp ( ndime, nnode, npnt, interpo )

   do i = 1, npnt
      x = localPnt(1,i) ; y = localPnt(2,i) ; z = localPnt(3,i)
      call elm_Q1_3d ( x, y, z, interpo(i)%shapef, interpo(i)%derloc )
      interpo(i)%points = [ x, y, z ]   
   end do
      
   END SUBROUTINE elm_hexa8Shape

!=============================================================================================
   SUBROUTINE elm_hexa8Info ( self )
!=============================================================================================   
   class(elmHexa8_t), intent(out) :: self
!--------------------------------------------------------------------------------------------- 
!  (see elm_edge2Info)
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------

   self%nDime = 3 ; self%nNode = 8   
   allocate(self%nodCoord(self%nDime,self%nNode))      
   self%nameElement = 'hexa8'; ; self%nameSimplex = 'hexa8' ; self%vtkCode = 12
   self%nodCoord(1,:) = [ -RONE,-RONE, RONE, RONE,-RONE,-RONE, RONE, RONE ]
   self%nodCoord(2,:) = [ -RONE,-RONE,-RONE,-RONE, RONE, RONE, RONE, RONE ]
   self%nodCoord(3,:) = [ -RONE, RONE, RONE,-RONE,-RONE, RONE, RONE,-RONE ] 
     
   END SUBROUTINE elm_hexa8Info 
   

!=============================================================================================
   SUBROUTINE elm_inverseMap ( self, elcod, physPnt, localPnt, stat, toler, niter )
!=============================================================================================   
   class  (elm_t),           intent(in    ) :: self
   real   (Rkind),           intent(in    ) :: physPnt(:), elcod(:,:) 
   real   (Rkind),           intent(in out) :: localPnt(:)   
   type   (err_t),           intent(in out) :: stat
   real   (Rkind), optional, intent(in    ) :: toler
   integer(Ikind), optional, intent(   out) :: niter
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter   :: HERE = 'elm_inverseMap'
   integer  (Ikind), parameter   :: maxit = 20
   real     (Rkind), parameter   :: toldefault = 1.0e-6   
   integer  (Ikind)              :: i, j, k, iter, nit
   real     (Rkind)              :: resid(self%nDime), dresid(self%nDime,self%nDime), &
                                    dPnt(self%nDime), eik, detJac, xloc(self%nDime,1), tol
   character(MDSTR)              :: msg
!- local variable (with save status): --------------------------------------------------------
   type (shp_t), SAVE, allocatable :: interpo(:) !(save to avoid reallocation when not needed)
!---------------------------------------------------------------------------------------------

   if ( size(physPnt) /= self%nDime ) then
      stat = err_t ( stat=UERROR, where=HERE, msg='Bad dimension of "physPnt" ('// &
                                          i2a(self%nDime)//' components expected)' )
      return
   end if
   
   if ( size(elcod,1) /= self%nDime .or. size(elcod,2) /= self%nNode ) then
      stat = err_t ( stat=UERROR, where=HERE, msg='Bad dimension of "elcod" (a ' // &
                     i2a(self%nDime) // 'x'//i2a(self%nNode) // ' array expected)'  )
      return
   end if
   
   if ( present(toler) ) then
      tol = toler
   else
      tol = toldefault
   end if
   
   nit = maxit
!
!- Start with the given guess:
! 
   xloc(:,1) = localPnt

   do iter = 1, maxit
!
!-    Shape functions and their local derivatives at localPnt:
!
      call self%computeShape ( xloc, interpo, stat )
      error_TraceNreturn(stat>IZERO, HERE, stat) 
!
!-    The residual f = sum(shape*elcod) - pt:
    
      do i = 1, self%nDime
         resid(i) =-physPnt(i)
         do j = 1, self%nNode
            resid(i) = resid(i) + elcod(i,j) * interpo(1)%shapef(j)
         end do
      end do
!
!-    Its jacobian:
!   
      dresid = RZERO
      do k = 1, self%nNode
         do i = 1, self%nDime
            eik = elcod(i,k)
            do j = 1, self%nDime
               dresid(i,j) = dresid(i,j) + eik * interpo(1)%derloc(j,k)
            end do
         end do
      end do 
!
!-    New estimate of localPnt:
!            
      call elm_solvLin ( A = dresid, b = resid, x = dPnt, stat = stat, detA = detJac )
      error_TraceNreturn(stat>IZERO, HERE, stat) 
      
      if ( detJac <= RZERO ) then
         write(msg,'(g0)') detJac
         msg = 'Negative jacobian: jac = ' // trim(msg)
         stat = err_t ( stat = UERROR, where = HERE, msg = trim(msg))
         return
      end if
      
      xloc(:,1) = xloc(:,1) - dPnt
!
!-    Stopping criterion:
!
      if ( maxval(abs(dPnt)) <= tol * maxval(abs(xloc(:,1))) ) then
         call self%computeShape ( xloc, interpo, stat )
         error_TraceNreturn(stat>IZERO, HERE, stat) 
         nit = iter
         exit
      end if
   
   end do

   localPnt = xloc(:,1)
   
   if ( present(niter) ) niter = nit
   
   if ( nit == 0 ) then
      stat = err_t ( stat = WARNING, where = HERE, &
          msg ='Stopping criterion not satisfied after maxit = '//i2a(maxit)//' iterations' )
      return
   end if

   END SUBROUTINE elm_inverseMap   
   
         
!=============================================================================================
   SUBROUTINE elm_solvLin ( A, b, x, stat, detA )
!=============================================================================================
   real(Rkind),           intent(in    ) :: A(:,:)
   real(Rkind),           intent(in    ) :: b(:)
   real(Rkind),           intent(in out) :: x(:)
   type(err_t),           intent(in out) :: stat  
   real(Rkind), optional, intent(   out) :: detA
!---------------------------------------------------------------------------------------------
!  Computes the solution of the small n x n linear system A*x = b with 1 <= n <=3
!----------------------------------------------------------------------------------------- R.H.
 
!- local variables: --------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'elm_solvLin'
   real     (Rkind)            :: invdetA
   integer  (Ikind)            :: n, err
!---------------------------------------------------------------------------------------------                   

   n = size(A,1)
   if ( size(A,2) /= n ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'The matrix must be square')
      return
   end if              
   
   if ( size(b) /= n .or. size(x) /= n ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Incompatible sizes')
      return
   end if
   
   err = 0   
   select case ( n )
      case ( 1 )
         detA = a(1,1)
         if ( detA /= RZERO ) then
            x(1) = b(1) / detA
         else
            err = 1
         end if 
          
      case ( 2 )
         detA = a(1,1)*a(2,2) - a(1,2)*a(2,1)
         if ( detA /= 0 ) then
            invdetA = RONE / detA
            x(1) = ( a(2,2)*b(1) - a(1,2)*b(2) ) * invdetA
            x(2) = (-a(2,1)*b(1) + a(1,1)*b(2) ) * invdetA
         else
            err = 1
         end if

      case ( 3 )
         detA = a(1,1)*(a(2,2)*a(3,3) - a(3,2)*a(2,3)) +  &
                a(1,2)*(a(3,1)*a(2,3) - a(2,1)*a(3,3)) +  &
                a(1,3)*(a(2,1)*a(3,2) - a(3,1)*a(2,2))
                
         if ( detA /= 0 ) then
            invdetA = RONE / detA
            x(1) = ( ( a(2,2)*a(3,3) - a(2,3)*a(3,2) ) * b(1) + &
                     ( a(1,3)*a(3,2) - a(1,2)*a(3,3) ) * b(2) + &
                     ( a(1,2)*a(2,3) - a(1,3)*a(2,2) ) * b(3) ) * invdetA
                     
            x(2) = ( ( a(3,1)*a(2,3) - a(2,1)*a(3,3) ) * b(1) + &
                     ( a(1,1)*a(3,3) - a(1,3)*a(3,1) ) * b(2) + &
                     ( a(2,1)*a(1,3) - a(2,3)*a(1,1) ) * b(3) ) * invdetA
                     
            x(3) = ( ( a(2,1)*a(3,2) - a(3,1)*a(2,2) ) * b(1) + &
                     ( a(1,2)*a(3,1) - a(3,2)*a(1,1) ) * b(2) + &
                     ( a(1,1)*a(2,2) - a(1,2)*a(2,1) ) * b(3) ) * invdetA
                     
         else
            err = 1
         end if
        
      case default
         stat = err_t ( stat = UERROR, where = HERE, msg =                        &
                'The matrix must be 1x1, 2x2 or 3x3 only (here n = '//i2a(n)//')' )
   end select
    
   if ( err /= 0 ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Singular matrix')
      return
   end if 

   END SUBROUTINE elm_solvLin     

   
!=============================================================================================
   SUBROUTINE allocateShp ( ndime, nnode, npnt, interpo )
!=============================================================================================
   integer(Ikind),              intent(in    ) :: ndime, nnode, npnt
   type   (shp_t), allocatable, intent(in out) :: interpo(:)
!---------------------------------------------------------------------------------------------
!  Allocates or reallocates "interpo" and its members
!
!  Reallocations are only performed when "ndime", "nnode", "npnt" do not match the actual 
!  dimensions of "interpo" and its members
!----------------------------------------------------------------------------------------- R.H.

!-- local variables: ------------------------------------------------------------------------
    integer(Ikind) :: i
!---------------------------------------------------------------------------------------------
   
   if ( allocated(interpo) ) then
      if ( size(interpo) /= npnt ) then
         deallocate(interpo) ; allocate(interpo(npnt))
      end if
   else
      allocate(interpo(npnt)) 
   end if
   
   do i = 1, npnt
      
      if ( allocated(interpo(i)%shapef) ) then
         if ( size(interpo(i)%shapef) /= nnode) then
            deallocate(interpo(i)%shapef) ; allocate(interpo(i)%shapef(nnode))
         end if
      else
         allocate(interpo(i)%shapef(nnode))
      end if
      
      if ( allocated(interpo(i)%derloc) ) then
         if ( size(interpo(i)%derloc,1)/=ndime .or. size(interpo(i)%derloc,2)/=nnode ) then
            deallocate(interpo(i)%derloc) ; allocate(interpo(i)%derloc(ndime,nnode))
         end if
      else
         allocate(interpo(i)%derloc(ndime,nnode))
      end if
      
      if ( allocated(interpo(i)%points) ) then
         if ( size(interpo(i)%points) /= ndime ) then
            deallocate(interpo(i)%points) ; allocate(interpo(i)%points(ndime))
         end if
      else
         allocate(interpo(i)%points(ndime))
      end if
   
   end do
   
   END SUBROUTINE allocateShp

!=============================================================================================
   SUBROUTINE elm_P1_1d ( x, shapef, derloc )
!=============================================================================================   
   real(Rkind),           intent(in    ) :: x
   real(Rkind),           intent(   out) :: shapef(2)
   real(Rkind), optional, intent(   out) :: derloc(2)
!---------------------------------------------------------------------------------- RH 04/23 - 
   
!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: half = 0.5_Rkind, one = 1.0_Rkind
!---------------------------------------------------------------------------------------------

   shapef = half * [ one - x, one + x ]
   if ( present(derloc) ) derloc = half * [ -one, one ] 
   
   END SUBROUTINE elm_P1_1d   

!=============================================================================================
   SUBROUTINE elm_P1_2d ( x, y, shapef, derloc )
!=============================================================================================   
   real(Rkind),           intent(in    ) :: x, y
   real(Rkind),           intent(   out) :: shapef(3)
   real(Rkind), optional, intent(   out) :: derloc(2,3)
!---------------------------------------------------------------------------------- RH 04/23 - 
   
!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: zero = 0.0_Rkind, one = 1.0_Rkind
!---------------------------------------------------------------------------------------------

   shapef = [ one - x - y, x, y ]
   if ( present(derloc) ) then
      derloc(1,:) = [ -one, one , zero ]
      derloc(2,:) = [ -one, zero, one  ]
   end if
   
   END SUBROUTINE elm_P1_2d   
   
!=============================================================================================
    SUBROUTINE elm_P1_3d ( x, y, z, shapef, derloc )
!=============================================================================================   
   real(Rkind),           intent(in    ) :: x, y, z
   real(Rkind),           intent(   out) :: shapef(4)
   real(Rkind), optional, intent(   out) :: derloc(3,4)
!--------------------------------------------------------------------------------------------- 
!
!---------------------------------------------------------------------------------- RH 04/23 - 

!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: zero = 0.0_Rkind, one = 1.0_Rkind   
!---------------------------------------------------------------------------------------------
          
   shapef = [ one - x - y - z, x,  y,  z ]
   if ( present(derloc) ) then
      derloc(1,:) = [-one, one , zero, zero ]
      derloc(2,:) = [-one, zero, one , zero ]
      derloc(3,:) = [-one, zero, zero, one  ]
   end if

   END SUBROUTINE elm_P1_3d   

!=============================================================================================
   SUBROUTINE elm_Q1_2d ( x, y, shapef, derloc )
!=============================================================================================   
   real(Rkind),           intent(in    ) :: x, y
   real(Rkind),           intent(   out) :: shapef(4)
   real(Rkind), optional, intent(   out) :: derloc(2,4)
!---------------------------------------------------------------------------------- RH 04/23 - 
   
!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: one = 1.0_Rkind, quart = 0.25_Rkind
   real(Rkind)            :: xm, xp, ym, yp
!---------------------------------------------------------------------------------------------

   xm = one - x ; xp = one + x ; ym = one - y ; yp = one + y

   shapef = quart * [ xm*ym, xp*ym, xp*yp, xm*yp ]
      
   if ( present(derloc) ) then
      derloc(1,:) = quart * [ -ym , ym , yp ,-yp ]
      derloc(2,:) = quart * [ -xm ,-xp , xp , xm ]
   end if
   
   END SUBROUTINE elm_Q1_2d   

!=============================================================================================
   SUBROUTINE elm_Q1_3d ( x, y, z, shapef, derloc )
!=============================================================================================   
   real(Rkind),           intent(in    ) :: x, y, z
   real(Rkind),           intent(   out) :: shapef(8)
   real(Rkind), optional, intent(   out) :: derloc(3,8)
!---------------------------------------------------------------------------------- RH 04/23 - 
   
!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: one = 1.0_Rkind, eighth = 0.125_Rkind
   real(Rkind)            :: xm, xp, ym, yp, zm, zp
!---------------------------------------------------------------------------------------------
        
   xm = one-x; xp = one+x; ym = one-y; yp = one+y; zm = one-z; zp = one+z
         
   shapef = eighth * [ xm*ym*zm, xm*ym*zp, xp*ym*zp, xp*ym*zm, &
                       xm*yp*zm, xm*yp*zp, xp*yp*zp, xp*yp*zm  ] 
                             
   if ( present(derloc) ) then
      derloc(1,:) = eighth * [ -ym*zm, -ym*zp,  ym*zp,  ym*zm, &
                               -yp*zm, -yp*zp,  yp*zp,  yp*zm  ]
      derloc(2,:) = eighth * [ -xm*zm, -xm*zp, -xp*zp, -xp*zm, &
                                xm*zm,  xm*zp,  xp*zp,  xp*zm  ]
      derloc(3,:) = eighth * [ -xm*ym,  xm*ym,  xp*ym, -xp*ym, &
                               -xm*yp,  xm*yp,  xp*yp, -xp*yp  ]      
   end if
   
   END SUBROUTINE elm_Q1_3d
   
!=============================================================================================
   SUBROUTINE elm_P2_1d ( x, shapef, derloc )
!=============================================================================================   
   real(Rkind),           intent(in    ) :: x
   real(Rkind),           intent(   out) :: shapef(3)
   real(Rkind), optional, intent(   out) :: derloc(3)
!---------------------------------------------------------------------------------- RH 04/23 - 
   
!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: half = 0.5_Rkind, one = 1.0_Rkind, two = 2.0_Rkind
!---------------------------------------------------------------------------------------------

   shapef = [ half*(x*x - x), half*(x*x + x), one - x*x ]
   if ( present(derloc) ) derloc = [ x - half, x + half, -two*x ]
   
   END SUBROUTINE elm_P2_1d

!=============================================================================================
   SUBROUTINE elm_P2_2d ( x, y, shapef, derloc )
!=============================================================================================   
   real(Rkind),           intent(in    ) :: x, y
   real(Rkind),           intent(   out) :: shapef(6)
   real(Rkind), optional, intent(   out) :: derloc(2,6)
!---------------------------------------------------------------------------------- RH 04/23 - 
   
!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: zero=0.0_Rkind, one=1.0_Rkind, two=2.0_Rkind, four=4.0_Rkind
   real(Rkind)            :: L1, fourL1, fourx, foury
!---------------------------------------------------------------------------------------------

   L1 = one - x - y ; fourx = four*x ; foury = four*y
   
   shapef(1:3) = [ L1*(two*L1 - one), x*(two*x - one), y*(two*y - one) ]
   shapef(4:6) = [ fourx*L1         , fourx*y        , foury*L1        ]
   
   if ( present(derloc) ) then
      fourL1 = four*L1
      derloc(1,1:3) = [ one - fourL1   , fourx - one , zero           ]
      derloc(2,1:3) = [ one - fourL1   , zero        , foury - one    ]
      derloc(1,4:6) = [ fourL1 - fourx , foury       ,-foury          ]
      derloc(2,4:6) = [-fourx          , fourx       , fourL1 - foury ]      
   end if
   
   END SUBROUTINE elm_P2_2d

!=============================================================================================
   SUBROUTINE elm_P2_3d ( x, y, z, shapef, derloc )
!=============================================================================================   
   real(Rkind),           intent(in    ) :: x, y, z
   real(Rkind),           intent(   out) :: shapef(10)
   real(Rkind), optional, intent(   out) :: derloc(3,10)
!---------------------------------------------------------------------------------- RH 04/23 - 
   
!- local variables: --------------------------------------------------------------------------
   real(Rkind), parameter :: zero=0.0_Rkind, one=1.0_Rkind, two=2.0_Rkind, four=4.0_Rkind
   real(Rkind)            :: L1, fourL1, fourx, foury, fourz
!---------------------------------------------------------------------------------------------

   L1 = one - x - y - z ; fourx = four*x ; foury = four*y ; fourz = four*z
      
   shapef(1: 4) = [ L1*(two*L1-one), x*(two*x-one), y*(two*y-one), z*(two*z-one) ]     
   shapef(5:10) = [ fourx*L1, fourx*y, foury*L1, fourz*L1, fourz*x, foury*z ] 
   
   if ( present(derloc) ) then
      fourL1 = four*L1
      derloc(1,1:4 ) = [ one-fourL1, fourx-one, zero     , zero      ]
      derloc(2,1:4 ) = [ one-fourL1, zero     , foury-one, zero      ]
      derloc(3,1:4 ) = [ one-fourL1, zero     , zero     , fourz-one ]
         
      derloc(1,5:10) = [  fourL1-fourx, foury, -foury       , -fourz       , fourz, zero  ]
      derloc(2,5:10) = [ -fourx       , fourx,  fourL1-foury, -fourz       , zero , fourz ]
      derloc(3,5:10) = [ -fourx       , zero , -foury       ,  fourL1-fourz, fourx, foury ]   
   end if

   END SUBROUTINE elm_P2_3d
   
END MODULE elm_m   