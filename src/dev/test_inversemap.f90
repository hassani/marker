! nagfor -fpp -kind=byte -L$dirlibpk2 -lpk2 -I $dirmodpk2 elm3.f90 test_inversemap.f90
#include "error.fpp"

use elm_m
use pk2mod_m

implicit none

!--------------------------------------------------------------------------------------------- 
type(err_t)               :: stat
integer                   :: i, n = 3
logical                   :: display = .true.
class(elm_t), allocatable :: myElm
!--------------------------------------------------------------------------------------------- 

call err_SetHaltingMode ( halting = OFF, unit = STDOUT, DisplayWarning = ON ) 

myElm=elm_t(namelem="edge2",stat=stat) ; call stat%display(abort=ON)
do i=1,n
   call test_edge2(myElm)
end do

myElm=elm_t(namelem="edge3",stat=stat) ; call stat%display(abort=ON)
do i=1,n
   call test_edge3(myElm)
end do

myElm=elm_t(namelem="tria3",stat=stat) ; call stat%display(abort=ON)
do i = 1, n
   call test_tria3(myElm)
end do

myElm=elm_t(namelem="tria4",stat=stat) ; call stat%display(abort=ON)
do i = 1, n
   call test_tria4(myElm)
end do

myElm=elm_t(namelem="tria6",stat=stat) ; call stat%display(abort=ON)
do i = 1, n
   call test_tria6(myElm)
end do

myElm=elm_t(namelem="tria7",stat=stat) ; call stat%display(abort=ON)
do i = 1, n
   call test_tria7(myElm)
end do

myElm=elm_t(namelem="quad4",stat=stat) ; call stat%display(abort=ON)
do i = 1, n
   call test_quad4(myElm)
end do

myElm=elm_t(namelem="quad8",stat=stat) ; call stat%display(abort=ON)
do i = 1, n
   call test_quad8(myElm)
end do

myElm=elm_t(namelem="quad9",stat=stat) ; call stat%display(abort=ON)
do i = 1, n
   call test_quad9(myElm)
end do

myElm=elm_t(namelem="tetra4",stat=stat) ; call stat%display(abort=ON)
do i = 1, n
   call test_tetra4(myElm)
end do

myElm=elm_t(namelem="tetra10",stat=stat) ; call stat%display(abort=ON)
do i = 1, n
   call test_tetra10(myElm)
end do

myElm=elm_t(namelem="hexa8",stat=stat) ; call stat%display(abort=ON)
do i = 1, n
   call test_hexa8(myElm)
end do


contains

!=============================================================================================
   subroutine test_edge2 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=1, nn=2   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(1,:) = [-0.2, 2.0]

   call random_number(xi0) ; xi0 = 2.*xi0- 1.

   call maps ( myElm, elcod, xi0 )
   
   end subroutine test_edge2

!=============================================================================================
   subroutine test_edge3 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter :: nd=1, nn=3   
   real   (Rkind)            :: elcod(nd,nn), xi0(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(1,:) = [-0.2, 2.0, 0.6]

   call random_number(xi0) ; xi0 = 2.*xi0- 1.

   call maps ( myElm, elcod, xi0 )
   
   end subroutine test_edge3   

!=============================================================================================
   subroutine test_tria3 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=2, nn=3   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(1,:) = [0.0, 2.0, 1.5]
   elcod(2,:) = [0.0, 0.5, 1.0]

   call random_number(xi0) ; xi0(2) = xi0(2)*(1.0 - xi0(1))
   call maps ( myElm, elcod, xi0 )

   end subroutine test_tria3
   
!=============================================================================================
   subroutine test_tria4 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=2, nn=4   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(:,1) = [0.0,0.0] ; elcod(:,2) = [2.0,0.5] ; elcod(:,3) = [1.5,1.0]
   elcod(:,4) = [sum(elcod(1,1:3))/3.0 , sum(elcod(2,1:3))/3.0] 

   call random_number(xi0) ; xi0(2) = xi0(2)*(1.0 - xi0(1))
   call maps ( myElm, elcod, xi0 )

   end subroutine test_tria4   

!=============================================================================================
   subroutine test_tria6 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=2, nn=6   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd), r(nd), eps=0.1, u(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(:,1) = [0.0,0.0] ; elcod(:,2) = [2.0,0.5] ; elcod(:,3) = [1.5,1.0] 
   
   u = elcod(:,2)-elcod(:,1) ; u = [u(2),-u(1)] 
   call random_number(r) ; r = r * eps * u
   elcod(:,4) = (elcod(:,1) + elcod(:,2))/2.0 + r
   
   u = elcod(:,3)-elcod(:,2) ; u = [u(2),-u(1)] 
   call random_number(r) ; r = r * eps * u
   elcod(:,5) = (elcod(:,2) + elcod(:,3))/2.0 + r

   u = elcod(:,1)-elcod(:,3) ; u = [u(2),-u(1)] 
   call random_number(r) ; r = r * eps * u
   elcod(:,6) = (elcod(:,3) + elcod(:,1))/2.0 + r
   
   call random_number(xi0) ; xi0(2) = xi0(2)*(1.0 - xi0(1))
   call maps ( myElm, elcod, xi0 )

   end subroutine test_tria6  


!=============================================================================================
   subroutine test_tria7 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=2, nn=7   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd), r(nd), eps=0.1, u(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(:,1) = [0.0,0.0] ; elcod(:,2) = [2.0,0.5] ; elcod(:,3) = [1.5,1.0] 
   
   u = elcod(:,2)-elcod(:,1) ; u = [u(2),-u(1)] !/ sqrt(u(1)**2+u(2)**2)
   call random_number(r) ; r = r * eps * u
   elcod(:,4) = (elcod(:,1) + elcod(:,2))/2.0 + r
   
   u = elcod(:,3)-elcod(:,2) ; u = [u(2),-u(1)] !/ sqrt(u(1)**2+u(2)**2)
   call random_number(r) ; r = r * eps * u
   elcod(:,5) = (elcod(:,2) + elcod(:,3))/2.0 + r

   u = elcod(:,1)-elcod(:,3) ; u = [u(2),-u(1)] !/ sqrt(u(1)**2+u(2)**2)
   call random_number(r) ; r = r * eps * u
   elcod(:,6) = (elcod(:,3) + elcod(:,1))/2.0 + r
   
   elcod(:,7) = [sum(elcod(1,1:6))/6.0 , sum(elcod(2,1:6))/6.0]
   
   call random_number(xi0) ; xi0(2) = xi0(2)*(1.0 - xi0(1))
   call maps ( myElm, elcod, xi0 )

   end subroutine test_tria7        
   
!=============================================================================================
   subroutine test_quad4 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=2, nn=4   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(1,:) = [0.0, 2.0, 1.5, -0.3]
   elcod(2,:) = [0.0, 0.5, 1.0,  0.6]

   call random_number(xi0) ; xi0 = 2.*xi0- 1.
   call maps ( myElm, elcod, xi0 )
   
   end subroutine test_quad4

!=============================================================================================
   subroutine test_quad8 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=2, nn=8   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(1,1:4) = [0.0, 2.0, 1.5, -0.3]
   elcod(2,1:4) = [0.0, 0.5, 1.0,  0.6]
   
   elcod(:,5) = 0.5*(elcod(:,1)+elcod(:,2))
   elcod(:,6) = 0.5*(elcod(:,2)+elcod(:,3))
   elcod(:,7) = 0.5*(elcod(:,3)+elcod(:,4))
   elcod(:,8) = 0.5*(elcod(:,4)+elcod(:,1))

   call random_number(xi0) ; xi0 = 2.*xi0- 1.
   call maps ( myElm, elcod, xi0 )

   end subroutine test_quad8

!=============================================================================================
   subroutine test_quad9 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=2, nn=9   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(1,1:4) = [0.0, 2.0, 1.5, -0.3]
   elcod(2,1:4) = [0.0, 0.5, 1.0,  0.6]
   
   elcod(:,5) = 0.5*(elcod(:,1)+elcod(:,2))
   elcod(:,6) = 0.5*(elcod(:,2)+elcod(:,3))
   elcod(:,7) = 0.5*(elcod(:,3)+elcod(:,4))
   elcod(:,8) = 0.5*(elcod(:,4)+elcod(:,1))

   elcod(:,9) = [sum(elcod(1,1:8))/8.0 , sum(elcod(2,1:8))/8.0]
   
   call random_number(xi0) ; xi0 = 2.*xi0- 1.
   call maps ( myElm, elcod, xi0 )

   end subroutine test_quad9

!=============================================================================================
   subroutine test_tetra4 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=3, nn=4   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(1,:) = [0.0, 2.0, 1.5, 0.1]
   elcod(2,:) = [0.0, 0.5, 1.0,-0.2]
   elcod(3,:) = [0.0,-0.1, 0.2, 1.3]

   call random_number(xi0) ; xi0(3) = xi0(3)*(1.0 - xi0(1) - xi0(2))
   call maps ( myElm, elcod, xi0 )

   end subroutine test_tetra4

!=============================================================================================
   subroutine test_tetra10 (myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=3, nn=10   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd), r(nd,6), eps=0.01
!--------------------------------------------------------------------------------------------- 

   elcod(1,1:4) = [0.0, 2.0, 1.5, 0.1]
   elcod(2,1:4) = [0.0, 0.5, 1.0,-0.2]
   elcod(3,1:4) = [0.0,-0.1, 0.2, 1.3]

   call random_number(r)   
   
   elcod(:, 5) = 0.5*(elcod(:,1)+elcod(:,2)) + eps*r(:,1)
   elcod(:, 6) = 0.5*(elcod(:,2)+elcod(:,3)) + eps*r(:,2)
   elcod(:, 7) = 0.5*(elcod(:,3)+elcod(:,1)) + eps*r(:,3)
   elcod(:, 8) = 0.5*(elcod(:,4)+elcod(:,1)) + eps*r(:,4)
   elcod(:, 9) = 0.5*(elcod(:,4)+elcod(:,2)) + eps*r(:,5)
   elcod(:,10) = 0.5*(elcod(:,4)+elcod(:,3)) + eps*r(:,6)
   
   call random_number(xi0) ; xi0(3) = xi0(3)*(1.0 - xi0(1) - xi0(2))
   call maps ( myElm, elcod, xi0 )

   end subroutine test_tetra10

!=============================================================================================
   subroutine test_hexa8(myElm)
!=============================================================================================
   class  (elm_t), intent(in) :: myElm
   integer(Ikind), parameter  :: nd=3, nn=8   
   real   (Rkind)             :: elcod(nd,nn), xi0(nd)
!--------------------------------------------------------------------------------------------- 

   elcod(1,:) = [0.0, 0.0, 1.5,  1.0, 0.0, -0.2, 1.3, 0.8]
   elcod(2,:) = [0.0, 0.2, 0.0, -0.2, 1.0,  0.8, 1.2, 0.8]
   elcod(3,:) = [0.0, 1.0, 1.3,  0.3, 0.0,  1.5, 1.3, 0.2]
   
   call random_number(xi0) ; xi0 = 2.*xi0- 1.

   call maps ( myElm, elcod, xi0 )

   end subroutine test_hexa8
                  
!=============================================================================================
   subroutine maps ( myElm, elcod, xi0 )
!=============================================================================================
   class    (elm_t), intent(in) :: myElm
   real     (Rkind), intent(in) :: elcod(:,:), xi0(:)
!--------------------------------------------------------------------------------------------- 
   integer(Ikind)                    :: i, niter, err
   real   (Rkind)                    :: pt(size(xi0)), xi(size(xi0)), xxi0(size(xi0),1)
   real   (Rkind)                    :: eps=1e-5
   logical                           :: outside
   type   (shp_t), SAVE, allocatable :: interpo(:), interpo0(:)
   class  (elm_t), SAVE, allocatable :: mySimplex
!--------------------------------------------------------------------------------------------- 

   err = 0
   if ( myElm%nDime /= size(elcod,1) .or. myElm%nDime /= size(xi0) ) err = 1
   if ( myElm%nNode  > size(elcod,2) ) err = 1
   if ( err /= 0 ) then
      print*,'Incompatibilty between element type and data'
      print '(a,a,a,i0,a,i0)','myElm%nameElement = ',myElm%nameElement, &
                            ', myElm%nDime = ',myElm%nDime, &
                            ', myElm%nNode = ',myElm%nNode
      print '(a,i0,1x,i0,a)','size(elcod) = [',size(elcod,1),size(elcod,2),']'
      print '(a,i0)'        ,'size(xi0) = ',size(xi0)
      stop
   end if
   
   xxi0(:,1) = xi0
   
   call myElm%computeShape ( xxi0, interpo, stat )
   call stat%display(abort=ON)

   pt = 0.0
   do i=1,myElm%nNode
      pt(:) = pt(:) + interpo(1)%shapef(i)*elcod(:,i)
   end do
   if ( display ) then
      print*,'******* ELEMENT '//myElm%nameElement//' *******'
      print*,'1) From local coordinates to physical coordinates (direct mapping)'
      print '(a,*(1x,g0))','    local coordinates   :',xi0
      print '(a,*(1x,g0))','    physical coordinates:',pt
   end if

   xi = 0.0

   call myElm%inverseMap ( elcod, pt, xi, stat, niter = niter )

   if ( stat /= 0 ) then
      print*,'FAILED'
      print*,'elcod:'
      do i=1,size(elcod,2)
         print*,elcod(:,i)
      end do
      call stat%display(abort=ON)
   end if

   if ( display ) then
      print*,'2) From physical coordinates to local coordinates (inverse mapping)'
      print '(a,*(1x,g0))','    physical coordinates:',pt
      print '(a,*(1x,g0))','    local coordinates   :',xi
      print '(a,1x,g0,1x,a )','    (computed in',niter,'iterations)'
   
      print '(4x,a,1x,g0,1x,a,1x,g0)','max|xi-xi0| =',maxval(abs(xi-xi0)), &
                                      'max|xi0| =',maxval(abs(xi0))
   end if

   ! See if the point is inside or outside the element:
      
   call elm_init ( res = mySimplex, stat = stat, namelem = myElm%nameSimplex )
   xxi0(:,1) = xi
   call mySimplex%computeShape ( xxi0, interpo0, stat )
   call stat%display(abort=ON)
   if ( display ) then
      print '(a)','    Barycentric coordinates:'      
      do i = 1, mySimplex%nNode
         print '(4x,g0)',interpo0(1)%shapef(i)
      end do
      outside = minval(interpo0(1)%shapef(:)) < -eps
      if ( outside ) then
         print '(a)','    The point is outside the element'
      else
         print '(a)','    The point is inside the element'
      end if            
   end if
   
   end subroutine maps


!=============================================================================================
   subroutine maps0 ( elem, elcod, xi0 )
!=============================================================================================
   character(len=*), intent(in) :: elem
   real     (Rkind), intent(in) :: elcod(:,:), xi0(:)
!--------------------------------------------------------------------------------------------- 
   integer(Ikind)              :: i, niter, err
   real   (Rkind)              :: pt(size(xi0)), xi(size(xi0)), xxi0(size(xi0),1), eps=1e-5
   logical                     :: outside
   class  (elm_t), allocatable :: myElm
   type   (shp_t), allocatable :: interpo(:)
!--------------------------------------------------------------------------------------------- 

   myElm = elm_t ( namelem = elem, stat = stat )
   call stat%display(abort=ON)   

   err = 0
   if ( myElm%nDime /= size(elcod,1) .or. myElm%nDime /= size(xi0) ) err = 1
   if ( myElm%nNode  > size(elcod,2) ) err = 1
   if ( err /= 0 ) then
      print*,'Incompatibilty between element type and data'
      print '(a,a,a,i0,a,i0)','myElm%nameElement = ',myElm%nameElement, &
                            ', myElm%nDime = ',myElm%nDime, &
                            ', myElm%nNode = ',myElm%nNode
      print '(a,i0,1x,i0,a)','size(elcod) = [',size(elcod,1),size(elcod,2),']'
      print '(a,i0)'        ,'size(xi0) = ',size(xi0)
      stop
   end if
   
   xxi0(:,1) = xi0
   
   call myElm%computeShape ( xxi0, interpo, stat )
   call stat%display(abort=ON)

   pt = 0.0
   do i=1,myElm%nNode
      pt(:) = pt(:) + interpo(1)%shapef(i)*elcod(:,i)
   end do
   if ( display ) then
      print*,'******* ELEMENT '//elem//' *******'
      print*,'1) From local coordinates to physical coordinates (direct mapping)'
      print '(a,*(1x,g0))','    local coordinates   :',xi0
      print '(a,*(1x,g0))','    physical coordinates:',pt
   end if

   xi = 0.0

   call myElm%inverseMap ( elcod, pt, xi, stat, niter = niter )

   if ( stat /= 0 ) then
      print*,'FAILED'
      print*,'elcod:'
      do i=1,size(elcod,2)
         print*,elcod(:,i)
      end do
      call stat%display(abort=ON)
   end if

   if ( display ) then
      print*,'2) From physical coordinates to local coordinates (inverse mapping)'
      print '(a,*(1x,g0))','    physical coordinates:',pt
      print '(a,*(1x,g0))','    local coordinates   :',xi
      print '(a,1x,g0,1x,a )','    (computed in',niter,'iterations)'
   
      print '(4x,a,1x,g0,1x,a,1x,g0)','max|xi-xi0| =',maxval(abs(xi-xi0)), &
                                      'max|xi0| =',maxval(abs(xi0))
      
      ! See if the point is inside or outside the element:
      
      myElm = elm_t ( namelem = myElm%nameSimplex, stat = stat )
      xxi0(:,1) = xi
      call myElm%computeShape ( xxi0, interpo, stat )
      call stat%display(abort=ON)
      print '(a)','    Barycentric coordinates:'      
      do i = 1, myElm%nNode
         print '(4x,g0)',interpo(1)%shapef(i)
      end do
      outside = minval(interpo(1)%shapef(:)) < -eps
      if ( outside ) then
         print '(a)','    The point is outside the element'
      else
         print '(a)','    The point is inside the element'
      end if         
      
   end if
   
   
   end subroutine maps0   
end

