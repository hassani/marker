! nagfor -fpp -kind=byte -L$dirlibpk2 -lpk2 -I $dirmodpk2 elm3.f90 test_elm3.f90

#include "error.fpp"

use elm_m
use pk2mod_m

implicit none

integer(Ikind), parameter   :: degmax = 3
real   (Rkind), parameter   :: ptest(3) = [ 0.1_Rkind, 0.7_Rkind, 0.2_Rkind ]
real   (Rkind)              :: Coefx(0:degmax), Coefy(0:degmax), Coefz(0:degmax)
class  (elm_t), allocatable :: myElm
type   (shp_t), allocatable :: interpo(:)
type   (err_t)              :: stat

call err_SetHaltingMode ( halting = OFF, unit = STDOUT, DisplayWarning = ON ) 

call random_number(Coefx) ; call random_number(Coefy) ; call random_number(Coefz)


call test_interpol("edge2")   ; call stat%display(abort=ON)
call test_interpol("edge3")   ; call stat%display(abort=ON)
call test_interpol("tria3")   ; call stat%display(abort=ON)
call test_interpol("tria4")   ; call stat%display(abort=ON)
call test_interpol("tria6")   ; call stat%display(abort=ON)
call test_interpol("tria7")   ; call stat%display(abort=ON)
call test_interpol("quad4")   ; call stat%display(abort=ON)
call test_interpol("quad8")   ; call stat%display(abort=ON)
call test_interpol("quad9")   ; call stat%display(abort=ON)
call test_interpol("tetra4")  ; call stat%display(abort=ON)
call test_interpol("tetra10") ; call stat%display(abort=ON)
call test_interpol("hexa8")   ; call stat%display(abort=ON)




contains


!=============================================================================================    
   subroutine test_interpol ( namelem )
!============================================================================================= 
   character(len=*), intent(in) :: namelem
!--------------------------------------------------------------------------------------------- 
   character(len=*), parameter   :: HERE = 'test_edge2'
   real     (Rkind)              :: fex
   real     (Rkind), allocatable :: pts(:,:), fnode(:), dfnode(:,:), dfex(:)
   integer  (Ikind), allocatable :: deg(:,:)
   integer  (Ikind)              :: i, cas, nnode, ndime
!--------------------------------------------------------------------------------------------- 

   print '(/,a,/)',"****************** Element: "//namelem//" ******************" 
   
   myElm = elm_t ( namelem = namelem, stat = stat ) 
   error_TraceNreturn(stat>IZERO, HERE, stat)
       
   ndime = myElm%nDime ; nnode = myElm%nNode
   
   allocate(pts(ndime,1), dfex(ndime), fnode(nnode), dfnode(ndime,nnode))
   
   pts(:,1) = ptest(1:ndime)
   
   call myElm%computeShape ( localPnt = pts, interpo = interpo, stat = stat)
   error_TraceNreturn(stat>IZERO, HERE, stat)

   call ptrscreen1 ( myElm%nodCoord, pts(:,1), interpo(1)%shapef(:), interpo(1)%derloc(:,:), &
                     ndime, nnode )
   
   call degres ( ndime, degmax, deg )
                     
   do cas = 1, size(deg,2)
      do i = 1, nnode
         call polyn ( myElm%nodCoord(:,i), fnode(i), dfnode(:,i), deg(:,cas), ndime )
      end do
      call polyn ( pts(:,1), fex, dfex, deg(:,cas), ndime )
      call ptrscreen2 ( cas, deg(:,cas), interpo(1)%shapef(:), fnode, interpo(1)%derloc(:,:), &
                        fex, dfex, ndime )
   end do      
   
   end subroutine test_interpol

!=============================================================================================    
   subroutine degres ( ndime, degmax, deg )
!=============================================================================================       
   integer(Ikind),              intent(in    ) :: ndime, degmax
   integer(Ikind), allocatable, intent(   out) :: deg(:,:)
!--------------------------------------------------------------------------------------------- 
   integer :: i, j, k, l, ncas, n
!--------------------------------------------------------------------------------------------- 
   
   n = degmax + 1
   
   if ( ndime == 1 ) then
      allocate(deg(ndime,n))
      l = 0
      do i = 0, degmax
         l = l + 1
         deg(1,l) = i
      end do
   else if ( ndime == 2 ) then
      ncas = ( n * (n+1) ) /2 ! nbr triangulaires
      allocate(deg(ndime,ncas))
      l = 0
      do i = 0, degmax
         do j = 0, degmax-i
            l = l + 1
            deg(:,l) = [i,j]
         end do
      end do
   else if ( ndime == 3 ) then
      ncas = ( n * (n+1) * (n+2) ) / 6  ! nbr tetraedriques
      allocate(deg(ndime,ncas))
      l = 0
      do i = 0, degmax
         do j = 0, degmax-i
            do k = 0, degmax-i-j
               l = l + 1
               deg(:,l)=[i,j,k]
            enddo
         enddo
      enddo
   end if
   
   end subroutine degres
   
!=============================================================================================    
   subroutine ptrscreen2 ( cas, deg, shape, fnode, deriv, fex, dfex, ndime )
!=============================================================================================    
   integer(Ikind), intent(in) :: cas, deg(:), ndime
   real   (Rkind), intent(in) :: shape(:), fnode(:), deriv(:,:), dfex(:), fex
!--------------------------------------------------------------------------------------------- 
   real(Rkind) :: f
!--------------------------------------------------------------------------------------------- 

   if ( ndime == 1 ) then
      print '(a,i0,a,i0)', "Case " ,cas,") polynomial of degree: ",sum(deg)
   else if ( ndime == 2 ) then
      print '(a,*(i0,a))', &
         "Case " ,cas,") polynomial of degree: ",sum(deg), &  
         " (deg(x)=",deg(1),", deg(y)=",deg(2),")"
   else if ( ndime == 3 ) then
      print '(a,*(i0,a))', &
         "Case " ,cas,") polynomial of degree: ",sum(deg), &  
         " (deg(x)=",deg(1),", deg(y)=",deg(2),", deg(z)=",deg(3),")"
   end if
   f = dot_product(shape,fnode)
   write(*,2)f,fex,abs(fex-f)
   f=dot_product(deriv(1,:),fnode)
   write(*,3)f,dfex(1),abs(dfex(1)-f)
   if (ndime >=2) then
      f = dot_product(deriv(2,:),fnode)
      write(*,4)f,dfex(2),abs(dfex(2)-f)
      if (ndime == 3 ) then
         f = dot_product(deriv(3,:),fnode)
         write(*,5)f,dfex(3),abs(dfex(3)-f)
      end if
   end if

   2 format("sum(N_i * f_i  ) =",f8.5,", fexact     =",f8.5,", error =",f8.5)
   3 format("sum(dxN_i * f_i) =",f8.5,", dfexact/dx =",f8.5,", error =",f8.5)
   4 format("sum(dyN_i * f_i) =",f8.5,", dfexact/dy =",f8.5,", error =",f8.5)
   5 format("sum(dzN_i * f_i) =",f8.5,", dfexact/dz =",f8.5,", error =",f8.5)

   end subroutine ptrscreen2

!=============================================================================================    
   subroutine ptrscreen1 ( xi, ptest, shape, deriv, ndime, nnode )
!=============================================================================================    
   integer(Ikind), intent(in) :: ndime, nnode
   real   (Rkind), intent(in) :: shape(:), ptest(:)
   real   (Rkind), intent(in) :: deriv(:,:), xi(:,:)   
!---------------------------------------------------------------------------------------------    
   integer(Ikind) :: i, j

   write(*,1)
   do i = 1,nnode
      write(*,2)i,(xi(j,i),j=1,ndime)
   end do
   
   if (ndime == 1) then
      write(*,31)(ptest(i),i=1,ndime)
   else if (ndime == 2) then
      write(*,32)(ptest(i),i=1,ndime)
   else if (ndime == 3) then
      write(*,33)(ptest(i),i=1,ndime)
   end if
   
   do i = 1, nnode
      write(*,2)i,shape(i),(deriv(j,i),j=1,ndime)
   end do
   write(*,4)sum(shape(1:nnode))
   write(*,5)dot_product(shape,xi(1,:)),ptest(1)
   if (ndime >= 2) write(*,6)dot_product(shape,xi(2,:)),ptest(2)
   if (ndime == 3) write(*,7)dot_product(shape,xi(3,:)),ptest(3)

   print '(a)','Interpolation of a polynomial at this point:'

   1  format("Local coordinates of nodes:")
   2  format(i2,1x,4(1x,f8.5))
   31 format("Shape functions and derivatives at point:",f8.5,/," i",6x,"N_i",3x,"gradN_i")
   32 format("Shape functions and derivatives at point:",2f8.5,/," i",6x,"N_i",8x,"gradN_i")
   33 format("Shape functions and derivatives at point:",3f8.5,/," i",6x,"N_i",10x,"gradN_i")
   4  format("sum(N_i) =",f8.5)
   5  format("sum(N_i * x_i) =",f8.5,", x =",f8.5)
   6  format("sum(N_i * y_i) =",f8.5,", y =",f8.5)
   7  format("sum(N_i * z_i) =",f8.5,", z =",f8.5)
   
   end subroutine ptrscreen1
   

!=============================================================================================    
   subroutine horner ( c, x, p, dp )
!=============================================================================================     
   real(Rkind), intent (in    ) :: c(0:), x
   real(Rkind), intent (   out) :: p, dp
!--------------------------------------------------------------------------------------------- 
   integer(Ikind) :: i
!---------------------------------------------------------------------------------------------
  
   p = c(ubound(c,1))
   dp = 0.0_Rkind
   do i = ubound(c,1)-1, 0, -1
      dp = dp * x + p
      p  = p  * x + c (i)
   end do

   end subroutine horner
   
!=============================================================================================    
   subroutine horner2 ( coeffs, x, y, p, dp )
!=============================================================================================     
   real(Rkind), intent (in    ) :: coeffs(0:,0:), x, y
   real(Rkind), intent (   out) :: p, dp(:)
!--------------------------------------------------------------------------------------------- 
   integer(Ikind) :: j, n, m
   real   (Rkind) :: a(0:ubound(coeffs,2)), daj, yj
!---------------------------------------------------------------------------------------------

   n = ubound(coeffs,1)
   m = ubound(coeffs,2)

   dp = 0.0_Rkind
   yj = 1.0_Rkind
   do j = 0, m
      call horner ( coeffs(0:n,j), x, a(j), daj ) 
      dp(1) = dp(1) + daj*yj
      yj = yj*y
   end do

   call horner ( a(0:m), y, p, dp(2) )
    
   end subroutine horner2  
   


!=============================================================================================    
   subroutine  polyn ( x, p, dp, deg, ndime )
!=============================================================================================    
   integer(Ikind), intent(in    ) :: deg(:), ndime
   real   (Rkind), intent(in    ) :: x(:)
   real   (Rkind), intent(   out) :: p, dp(ndime)
!--------------------------------------------------------------------------------------------- 
   real   (Rkind) :: px, dpx, py, dpy, pz, dpz 
!--------------------------------------------------------------------------------------------- 

   p = 0.0_Rkind ; dp = 0.0_Rkind
   
   call horner ( Coefx(0:deg(1)), x(1), px, dpx )
   
   if ( ndime == 1 ) then
      p = px ; dp(1) = dpx
      return
   else if ( ndime > 1 ) then
      call horner ( Coefy(0:deg(2)), x(2), py, dpy )
      if ( ndime == 2 ) then
         p = px * py ; dp = [ dpx * py, px * dpy ]
         return
      else if ( ndime == 3 ) then
         call horner ( Coefz(0:deg(3)), x(3), pz, dpz )
         p = px * py * pz  ; dp = [ dpx*py*pz, px*dpy*pz, px*py*dpz ]
      end if
   end if
   
   end subroutine  polyn
      


end 
