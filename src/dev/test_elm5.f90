! nagfor -fpp -kind=byte -L$dirlibpk2 -lpk2 -llapack -lblas -I $dirmodpk2 elm5.f90 test_elm5.f90 

#include "error.fpp"

use elm_m
use pk2mod_m

implicit none

integer(Ikind), parameter   :: degmax = 3
real   (Rkind), parameter   :: ptest(3) = [ 0.1_Rkind, 0.7_Rkind, 0.2_Rkind ]
real   (Rkind), allocatable :: Coef(:,:,:)
class  (elm_t), allocatable :: myElm
type   (err_t)              :: stat

call err_SetHaltingMode ( halting = OFF, unit = STDOUT, DisplayWarning = ON ) 


allocate ( Coef(0:degmax,0:degmax,0:degmax) )

call random_number(Coef) 



call test_interpol("edge2")   ; call stat%display(abort=ON)
call test_interpol("edge3")   ; call stat%display(abort=ON)
call test_interpol("tria3")   ; call stat%display(abort=ON)
call test_interpol("tria4")   ; call stat%display(abort=ON)
call test_interpol("tria6")   ; call stat%display(abort=ON)
call test_interpol("tria7")   ; call stat%display(abort=ON)
call test_interpol("quad4")   ; call stat%display(abort=ON)
call test_interpol("quad8")   ; call stat%display(abort=ON)
call test_interpol("quad9")   ; call stat%display(abort=ON)
call test_interpol("tetra4")  ; call stat%display(abort=ON)
call test_interpol("tetra10") ; call stat%display(abort=ON)
call test_interpol("hexa8")   ; call stat%display(abort=ON)


contains


!=============================================================================================    
   subroutine test_interpol ( namelem )
!============================================================================================= 
   character(len=*), intent(in) :: namelem
!--------------------------------------------------------------------------------------------- 
   character(len=*), parameter   :: HERE = 'test_interpol'
   integer  (Ikind)              :: nnode, ndime
   real     (Rkind), allocatable :: pts(:,:)
!--------------------------------------------------------------------------------------------- 

   print '(/,a,/)',"****************** Element: "//namelem//" ******************" 
   
   myElm = elm_t ( namelem = namelem, stat = stat )
   error_TraceNreturn(stat>IZERO, HERE, stat)
       
   ndime = myElm%nDime ; nnode = myElm%nNode

   allocate(pts(ndime,1)) ; pts(:,1) = ptest(1:ndime)
   
   call myElm%computeShape ( localPnt = pts, stat = stat )
   error_TraceNreturn(stat>IZERO, HERE, stat)

   call ptrscreen1 ( myElm%nodCoord, ptest, myElm%shapef(:,1), myElm%derloc(:,:,1), &
                     ndime, nnode )
   
   if ( ndime == 1 ) then
      call test_interpol1d ( nnode, ptest(1) )
   else if ( ndime == 2 ) then
      call test_interpol2d ( nnode, ptest(1:2) )
   else if ( ndime == 3 ) then
      call test_interpol3d ( nnode, ptest(1:3) )
   end if
   
   end subroutine test_interpol
   
   
!=============================================================================================   
   subroutine test_interpol1d ( nnode, pts )
!=============================================================================================  
   integer(Ikind), intent(in) :: nnode
   real   (Rkind), intent(in) :: pts
!--------------------------------------------------------------------------------------------- 
   integer(Ikind) :: i, deg
   real   (Rkind) :: c(0:degmax), fnode(nnode), dfnode, fex, dfex(1)
!--------------------------------------------------------------------------------------------- 

   c = 0.0_Rkind
   
   do deg = 0, degmax
      c(0:deg) = coef(0:deg,0,0)
      do i = 1, nnode
         call util_horner ( c(0:), myElm%nodCoord(1,i), fnode(i), dfnode )
      end do      
      call util_horner ( c(0:), pts, fex, dfex(1) )        
      call ptrscreen2 ( deg, myElm%shapef(:,1), fnode, myElm%derloc(:,:,1), fex, dfex )      
   end do         
   
   end subroutine test_interpol1d


!=============================================================================================   
   subroutine test_interpol2d ( nnode, pts )
!=============================================================================================  
   integer(Ikind), intent(in) :: nnode
   real   (Rkind), intent(in) :: pts(:)
!--------------------------------------------------------------------------------------------- 
   integer(Ikind) :: i, j, deg
   real   (Rkind) :: c(0:degmax,0:degmax), fnode(nnode), dfnode(2), fex, dfex(2)
!--------------------------------------------------------------------------------------------- 

   c = 0.0_Rkind
   
   do deg = 0, degmax
      do j = 0, degmax
         do i = 0, degmax
            if ( i + j <= deg ) c(i,j) = coef(i,j,0)
         end do
      end do
      do i = 1, nnode
         call util_horner ( c(0:,0:), myElm%nodCoord(:,i), fnode(i), dfnode )
      end do      
      call util_horner ( c(0:,0:), pts, fex, dfex )        
      call ptrscreen2 ( deg, myElm%shapef(:,1), fnode, myElm%derloc(:,:,1), fex, dfex )      
   end do         
   
   end subroutine test_interpol2d 


!=============================================================================================   
   subroutine test_interpol3d ( nnode, pts )
!=============================================================================================  
   integer(Ikind), intent(in) :: nnode
   real   (Rkind), intent(in) :: pts(:)
!--------------------------------------------------------------------------------------------- 
   integer(Ikind) :: i, j, k, deg
   real   (Rkind) :: c(0:degmax,0:degmax,0:degmax), fnode(nnode), dfnode(3), fex, dfex(3)
!--------------------------------------------------------------------------------------------- 

   c = 0.0_Rkind
   
   do deg = 0, degmax
      do k = 0, degmax
         do j = 0, degmax
            do i = 0, degmax
               if ( i + j + k <= deg ) c(i,j,k) = coef(i,j,k)
            end do
         end do
      end do
      do i = 1, nnode
         call util_horner ( c(0:,0:,0:), myElm%nodCoord(:,i), fnode(i), dfnode )
      end do      
      call util_horner ( c(0:,0:,0:), pts, fex, dfex )        
      call ptrscreen2 ( deg, myElm%shapef(:,1), fnode, myElm%derloc(:,:,1), fex, dfex )      
   end do         
   
   end subroutine test_interpol3d 
   

!=============================================================================================    
   subroutine ptrscreen2 ( deg, shape, fnode, deriv, fex, dfex )
!=============================================================================================    
   integer(Ikind), intent(in) :: deg
   real   (Rkind), intent(in) :: shape(:), fnode(:), deriv(:,:), dfex(:), fex
!--------------------------------------------------------------------------------------------- 
   real(Rkind) :: f
!--------------------------------------------------------------------------------------------- 

   print '(a,i0,a,/,a)', "- Polynomial of degree "//util_intTochar(deg)//':'
   print '(2x,a)',repeat('-',22)

   f = dot_product(shape,fnode)
   write(*,2)f,fex,abs(fex-f)
   f=dot_product(deriv(1,:),fnode)
   write(*,3)f,dfex(1),abs(dfex(1)-f)
   if (size(dfex) >=2) then
      f = dot_product(deriv(2,:),fnode)
      write(*,4)f,dfex(2),abs(dfex(2)-f)
      if (size(dfex) == 3 ) then
         f = dot_product(deriv(3,:),fnode)
         write(*,5)f,dfex(3),abs(dfex(3)-f)
      end if
   end if

   2 format("sum(N_i * f_i  ) =",f8.5,", fexact     =",f8.5,", error =",f8.5)
   3 format("sum(dxN_i * f_i) =",f8.5,", dfexact/dx =",f8.5,", error =",f8.5)
   4 format("sum(dyN_i * f_i) =",f8.5,", dfexact/dy =",f8.5,", error =",f8.5)
   5 format("sum(dzN_i * f_i) =",f8.5,", dfexact/dz =",f8.5,", error =",f8.5)

   end subroutine ptrscreen2

!=============================================================================================    
   subroutine ptrscreen1 ( xi, ptest, shape, deriv, ndime, nnode )
!=============================================================================================    
   integer(Ikind), intent(in) :: ndime, nnode
   real   (Rkind), intent(in) :: shape(:), ptest(:)
   real   (Rkind), intent(in) :: deriv(:,:), xi(:,:)   
!---------------------------------------------------------------------------------------------    
   integer(Ikind) :: i, j

   write(*,1)
   do i = 1,nnode
      write(*,2)i,(xi(j,i),j=1,ndime)
   end do
   
   if (ndime == 1) then
      write(*,31)(ptest(i),i=1,ndime)
   else if (ndime == 2) then
      write(*,32)(ptest(i),i=1,ndime)
   else if (ndime == 3) then
      write(*,33)(ptest(i),i=1,ndime)
   end if
   
   do i = 1, nnode
      write(*,2)i,shape(i),(deriv(j,i),j=1,ndime)
   end do
   write(*,4)sum(shape(1:nnode))
   write(*,5)dot_product(shape,xi(1,:)),ptest(1)
   if (ndime >= 2) write(*,6)dot_product(shape,xi(2,:)),ptest(2)
   if (ndime == 3) write(*,7)dot_product(shape,xi(3,:)),ptest(3)

   print '(a)','Interpolation of a polynomial at this point:'

   1  format("Local coordinates of nodes:")
   2  format(i2,1x,4(1x,f8.5))
   31 format("Shape functions and derivatives at point:",f8.5,/," i",6x,"N_i",3x,"gradN_i")
   32 format("Shape functions and derivatives at point:",2f8.5,/," i",6x,"N_i",8x,"gradN_i")
   33 format("Shape functions and derivatives at point:",3f8.5,/," i",6x,"N_i",10x,"gradN_i")
   4  format("sum(N_i) =",f8.5)
   5  format("sum(N_i * x_i) =",f8.5,", x =",f8.5)
   6  format("sum(N_i * y_i) =",f8.5,", y =",f8.5)
   7  format("sum(N_i * z_i) =",f8.5,", z =",f8.5)
   
   end subroutine ptrscreen1
   

end 
