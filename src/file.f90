!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       file_m
!
! Description: 
!        Define a DT to manage and read unformatted / formatted files
! 
! Notes: 
!       
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        06/22
!
! Changes:
!        07/22
!        
!---------------------------------------------------------------------------------------------

#include "error.fpp"

MODULE file_m
   use constantsmarker_m, only: i32, i64, rSp, rDp, Ikind, &
                                UERROR, RERROR, EOF, IZERO, LGSTR, NLT
   use pk2mod_m,          only: err_t, i2a => util_IntToChar 

   implicit none
                    
   private
   public :: file_t
!
!- an abstract class for any file:
!                        
   type, abstract :: fbase_t
      character(len=:), allocatable :: name
      integer  (Ikind)              :: unit = 0
      integer  (Ikind)              :: line = 0
   contains
      private
      procedure, pass(self) :: close  => file_fbaseclose  
      
!-    deferred methods:

      procedure(interf_open ), deferred, pass(self) :: open      
      
      ! for reading 4-bytes real rank-0, rank-1, rank-2 arrays:
      procedure(interf_rSpk0), deferred, pass(self) :: rSpk0Reader      
      procedure(interf_rSpk1), deferred, pass(self) :: rSpk1Reader  
      procedure(interf_rSpk2), deferred, pass(self) :: rSpk2Reader  

      ! for reading 8-bytes real rank-0, rank-1, rank-2 arrays:
      procedure(interf_rDpk0), deferred, pass(self) :: rDpk0Reader      
      procedure(interf_rDpk1), deferred, pass(self) :: rDpk1Reader    
      procedure(interf_rDpk2), deferred, pass(self) :: rDpk2Reader  
              
      ! for reading 4-bytes integer rank-0, rank-1, rank-2 arrays:
      procedure(interf_i32k0), deferred, pass(self) :: i32k0Reader    
      procedure(interf_i32k1), deferred, pass(self) :: i32k1Reader
      procedure(interf_i32k2), deferred, pass(self) :: i32k2Reader

      ! for reading 8-bytes integer rank-0, rank-1, rank-2 arrays:
      procedure(interf_i64k0), deferred, pass(self) :: i64k0Reader    
      procedure(interf_i64k1), deferred, pass(self) :: i64k1Reader
      procedure(interf_i64k2), deferred, pass(self) :: i64k2Reader  
      
      ! for reading a string of unknown lenght:
      procedure(interf_str  ), deferred, pass(self) :: strReader

      ! for reading a string of a given lenght:      
      procedure(interf_char ), deferred, pass(self) :: charReader
                
      generic, public :: reader => rSpk0Reader, rSpk1Reader, rSpk2Reader, &
                                   rDpk0Reader, rDpk1Reader, rDpk2Reader, &
                                   i32k0Reader, i32k1Reader, i32k2Reader, &
                                   i64k0Reader, i64k1Reader, i64k2Reader, &
                                   strReader  , charReader
   end type fbase_t
   
!
!- a class for unformatted files:
!   
   type, extends(fbase_t) :: fBin_t
   contains
      private
      procedure, pass(self) :: open => file_fbaseopenBin 
      
      procedure, pass(self) :: rSpk0Reader => file_fbaserSpk0ReaderBin, &
                               rSpk1Reader => file_fbaserSpk1ReaderBin, &
                               rSpk2Reader => file_fbaserSpk2ReaderBin, &
                               rDpk0Reader => file_fbaserDpk0ReaderBin, &
                               rDpk1Reader => file_fbaserDpk1ReaderBin, &
                               rDpk2Reader => file_fbaserDpk2ReaderBin, &
                               i32k0Reader => file_fbaseI32k0ReaderBin, &
                               i32k1Reader => file_fbaseI32k1ReaderBin, &
                               i32k2Reader => file_fbaseI32k2ReaderBin, &
                               i64k0Reader => file_fbaseI64k0ReaderBin, &
                               i64k1Reader => file_fbaseI64k1ReaderBin, &
                               i64k2Reader => file_fbaseI64k2ReaderBin, &
                               strReader   => file_fbasestrReaderBin  , &  
                               charReader  => file_fbasecharReaderBin      
   end type fBin_t   

!
!- a class for formatted files:
!  
   type, extends(fbase_t) :: fAscii_t
   contains
      private
      procedure, pass(self) :: open => file_fbaseopenAscii    
      
      procedure, pass(self) :: rSpk0Reader => file_fbaserSpk0ReaderAscii, &
                               rSpk1Reader => file_fbaserSpk1ReaderAscii, &
                               rSpk2Reader => file_fbaserSpk2ReaderAscii, &
                               rDpk0Reader => file_fbaserDpk0ReaderAscii, &
                               rDpk1Reader => file_fbaserDpk1ReaderAscii, &
                               rDpk2Reader => file_fbaserDpk2ReaderAscii, & 
                               i32k0Reader => file_fbaseI32k0ReaderAscii, &
                               i32k1Reader => file_fbaseI32k1ReaderAscii, &
                               i32k2Reader => file_fbaseI32k2ReaderAscii, &
                               i64k0Reader => file_fbaseI64k0ReaderAscii, &
                               i64k1Reader => file_fbaseI64k1ReaderAscii, &
                               i64k2Reader => file_fbaseI64k2ReaderAscii, &
                               strReader   => file_fbasestrReaderAscii  , & 
                               charReader  => file_fbasecharReaderAscii 
   end type fAscii_t   


   type :: file_t
      class(fbase_t), allocatable :: f
   contains
      private
      ! private methods:
      procedure, pass(self) :: file_readi32k0, file_readi32k1, file_readi32k2, &
                               file_readi64k0, file_readi64k1, file_readi64k2, &                
                               file_readrSpk0, file_readrSpk1, file_readrSpk2, &
                               file_readrDpk0, file_readrDpk1, file_readrDpk2, &
                               file_readStr  , file_readChar      
      ! public methods:
      generic, public :: read   => file_readi32k0, file_readi32k1, file_readi32k2, &
                                   file_readi64k0, file_readi64k1, file_readi64k2, &
                                   file_readrSpk0, file_readrSpk1, file_readrSpk2, &
                                   file_readrDpk0, file_readrDpk1, file_readrDpk2, &
                                   file_readStr  , file_readChar

      procedure, public, pass(self) :: open    => file_open   , &  
                                       close   => file_close  , &
                                       getLine => file_getLine, &    
                                       getUnit => file_getUnit, &    
                                       getName => file_getName, &    
                                       rewind  => file_rewind    
   end type file_t
        
   abstract interface
       subroutine interf_open ( self, name, stat, access )
         import :: fbase_t, err_t
         class    (fbase_t),           intent(in out) :: self 
         character(len=*  ),           intent(in    ) :: name
         type     (err_t  ),           intent(in out) :: stat   
         character(len=*  ), optional, intent(in    ) :: access
      end subroutine interf_open   

      subroutine interf_rSpk0 ( self, x, stat, what )
         import :: fbase_t, rsp, err_t
         class    (fbase_t),           intent(in out) :: self 
         real     (rsp    ),           intent(in out) :: x
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_rSpk0 
      subroutine interf_rDpk0 ( self, x, stat, what )
         import :: fbase_t, rdp, err_t
         class    (fbase_t),           intent(in out) :: self 
         real     (rdp    ),           intent(in out) :: x
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what         
      end subroutine interf_rDpk0       

      subroutine interf_rSpk1 ( self, x, stat, what )
         import :: fbase_t, rsp, err_t
         class    (fbase_t),           intent(in out) :: self 
         real     (rsp    ),           intent(in out) :: x(:)
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_rSpk1 
      subroutine interf_rDpk1 ( self, x, stat, what )
         import :: fbase_t, rdp, err_t
         class    (fbase_t),           intent(in out) :: self 
         real     (rdp    ),           intent(in out) :: x(:)
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_rDpk1             

      subroutine interf_rSpk2 ( self, x, stat, what )
         import :: fbase_t, rsp, err_t
         class    (fbase_t),           intent(in out) :: self 
         real     (rsp    ),           intent(in out) :: x(:,:)
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_rSpk2 
      subroutine interf_rDpk2 ( self, x, stat, what )
         import :: fbase_t, rdp, err_t
         class    (fbase_t),           intent(in out) :: self 
         real     (rdp    ),           intent(in out) :: x(:,:)
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_rDpk2
                
      subroutine interf_i32k0 ( self, x, stat, what )
         import :: fbase_t, i32, err_t
         class    (fbase_t),           intent(in out) :: self 
         integer  (i32    ),           intent(in out) :: x
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_i32k0 
      subroutine interf_i64k0 ( self, x, stat, what )
         import :: fbase_t, i64, err_t
         class    (fbase_t),           intent(in out) :: self 
         integer  (i64    ),           intent(in out) :: x
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_i64k0 
      
      subroutine interf_i32k1 ( self, x, stat, what )
         import :: fbase_t, i32, err_t
         class    (fbase_t),           intent(in out) :: self 
         integer  (i32    ),           intent(in out) :: x(:)
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_i32k1   
      subroutine interf_i64k1 ( self, x, stat, what )
         import :: fbase_t, i64, err_t
         class    (fbase_t),           intent(in out) :: self 
         integer  (i64    ),           intent(in out) :: x(:)
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_i64k1   
      
      subroutine interf_i32k2 ( self, x, stat, what )
         import :: fbase_t, i32, err_t
         class    (fbase_t),           intent(in out) :: self 
         integer  (i32    ),           intent(in out) :: x(:,:)
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_i32k2   
      subroutine interf_i64k2 ( self, x, stat, what )
         import :: fbase_t, i64, err_t
         class    (fbase_t),           intent(in out) :: self 
         integer  (i64    ),           intent(in out) :: x(:,:)
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_i64k2        
           
      subroutine interf_str ( self, str, stat, what )
         import :: fbase_t, err_t
         class    (fbase_t),              intent(in out) :: self 
         character(len=:  ), allocatable, intent(in out) :: str
         type     (err_t  ),              intent(in out) :: stat
         character(len=*  ), optional,    intent(in    ) :: what
      end subroutine interf_str       

      subroutine interf_char ( self, nch, ch, stat, what )
         import :: fbase_t, err_t, Ikind
         class    (fbase_t),           intent(in out) :: self 
         integer  (Ikind  ),           intent(in    ) :: nch
         character(len=nch),           intent(in out) :: ch
         type     (err_t  ),           intent(in out) :: stat
         character(len=*  ), optional, intent(in    ) :: what
      end subroutine interf_char     
      
   end interface     

CONTAINS

!=============================================================================================
   FUNCTION file_getLine ( self ) result ( line )
!=============================================================================================
   class  (file_t), intent(in) :: self
   integer(Ikind )             :: line
!--------------------------------------------------------------------------------------------- 
   
   if ( allocated(self%f) ) then
      line = self%f%line
   else
      line =-1
   end if
   
   END FUNCTION file_getLine


!=============================================================================================
   FUNCTION file_getUnit ( self ) result ( unit )
!=============================================================================================
   class  (file_t), intent(in) :: self
   integer(Ikind )             :: unit
!--------------------------------------------------------------------------------------------- 
   
   if ( allocated(self%f) ) then
      unit = self%f%unit
   else
      unit =-1
   end if 
     
   END FUNCTION file_getUnit


!=============================================================================================
   FUNCTION file_getName ( self ) result ( name )
!=============================================================================================
   class    (file_t), intent(in) :: self
   character(len=: ), allocatable :: name
!--------------------------------------------------------------------------------------------- 
   
   if ( allocated(self%f) ) then
      if (allocated(self%f%name)) then
         name = self%f%name
      else
         name = ''
      end if
   else
      name = ''
   end if
   
   END FUNCTION file_getName   

     
!=============================================================================================
   SUBROUTINE file_open ( self, name, form, stat, access  )
!=============================================================================================
   class    (file_t),           intent(in out) :: self
   character(len=* ),           intent(in    ) :: name, form 
   type     (err_t ),           intent(in out) :: stat 
   character(len=* ), optional, intent(in    ) :: access 
!---------------------------------------------------------------------------------------------      
!
!---------------------------------------------------------------------------------------------      

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'file_open'
!--------------------------------------------------------------------------------------------- 

   if ( trim(adjustl(form)) == 'unformatted' ) then
      allocate(fBin_t :: self%f)
   else if ( trim(adjustl(form)) == 'formatted' ) then
      allocate(fAscii_t :: self%f)
   else
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'file_open: Invalid value '//trim(form)//'for format specifier' )
      return
   end if
   
   call self%f%open ( name, stat, access ) ; error_TraceNreturn(stat/=IZERO, HERE, stat)
         
   END SUBROUTINE file_open
   

!=============================================================================================
   SUBROUTINE file_close ( self )
!=============================================================================================
   class(file_t), intent(in out) :: self
!---------------------------------------------------------------------------------------------      

   if ( allocated(self%f) ) then
      call self%f%close ( ) ;  deallocate(self%f)
   end if
   
   END SUBROUTINE file_close
   

!=============================================================================================
   SUBROUTINE file_rewind ( self )
!=============================================================================================
   class(file_t), intent(in out) :: self
!---------------------------------------------------------------------------------------------      

   if ( allocated(self%f) ) then
      rewind(self%getUnit())
      self%f%line = IZERO
   end if
   
   END SUBROUTINE file_rewind
   
   
!=============================================================================================   
   SUBROUTINE file_readi32k0 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   integer  (i32   ),           intent(in out) :: x
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readi32k0', stat)
   
   END SUBROUTINE file_readi32k0

!=============================================================================================   
   SUBROUTINE file_readi32k1 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   integer  (i32   ),           intent(in out) :: x(:)
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readi32k1', stat)
   
   END SUBROUTINE file_readi32k1

!=============================================================================================   
   SUBROUTINE file_readi32k2 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   integer  (i32   ),           intent(in out) :: x(:,:)
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readi32k2', stat)
   
   END SUBROUTINE file_readi32k2

!=============================================================================================   
   SUBROUTINE file_readi64k0 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   integer  (i64   ),           intent(in out) :: x
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      

   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readi64k0', stat)
   
   END SUBROUTINE file_readi64k0

!=============================================================================================   
   SUBROUTINE file_readi64k1 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   integer  (i64   ),           intent(in out) :: x(:)
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      
   
   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readi64k1', stat)
   
   END SUBROUTINE file_readi64k1

!=============================================================================================   
   SUBROUTINE file_readi64k2 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   integer  (i64   ),           intent(in out) :: x(:,:)
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      
   
   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readi64k2', stat)
   
   END SUBROUTINE file_readi64k2
            
!=============================================================================================   
   SUBROUTINE file_readrSpk0 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   real     (rSp   ),           intent(in out) :: x
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      
   
   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readrSpk0', stat)
   
   END SUBROUTINE file_readrSpk0

!=============================================================================================   
   SUBROUTINE file_readrSpk1 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   real     (rSp   ),           intent(in out) :: x(:)
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      
   
   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readrSpk1', stat)
   
   END SUBROUTINE file_readrSpk1
    
!=============================================================================================   
   SUBROUTINE file_readrSpk2 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   real     (rSp   ),           intent(in out) :: x(:,:)
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      
   
   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readrSpk2', stat)
   
   END SUBROUTINE file_readrSpk2  

!=============================================================================================   
   SUBROUTINE file_readrDpk0 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   real     (rDp   ),           intent(in out) :: x
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      
   
   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readrDpk0', stat)
   
   END SUBROUTINE file_readrDpk0

!=============================================================================================   
   SUBROUTINE file_readrDpk1 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   real     (rDp   ),           intent(in out) :: x(:)
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      
   
   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readrDpk1', stat)
   
   END SUBROUTINE file_readrDpk1
    
!=============================================================================================   
   SUBROUTINE file_readrDpk2 ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   real     (rDp   ),           intent(in out) :: x(:,:)
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      
   
   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readrDpk2', stat)
   
   END SUBROUTINE file_readrDpk2   

!=============================================================================================  
   SUBROUTINE file_readStr ( self, x, stat, what )
!=============================================================================================  
   class    (file_t),              intent(in out) :: self
   character(len=: ), allocatable, intent(in out) :: x
   type     (err_t ),              intent(in out) :: stat
   character(len=* ), optional,    intent(in    ) :: what
!---------------------------------------------------------------------------------------------      
   
   call self%f%reader ( x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readStr', stat)
         
   END SUBROUTINE file_readStr  

!=============================================================================================  
   SUBROUTINE file_readChar ( self, n, x, stat, what )
!=============================================================================================  
   class    (file_t),           intent(in out) :: self
   integer  (Ikind ),           intent(in    ) :: n
   character(len=n ),           intent(in out) :: x
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      
   
   call self%f%reader ( n, x, stat, what )
   error_TraceNreturn(stat/=IZERO, 'file_readChar', stat)
         
   END SUBROUTINE file_readChar  
!
!- Pour fichier binaire
!
!=============================================================================================
   SUBROUTINE file_fbaseclose ( self )
!=============================================================================================
   class  (fbase_t), intent(in out) :: self
!---------------------------------------------------------------------------------------------      

   close(self%unit)
   self%unit = 0
   self%line = 0
   self%name = ''

   END SUBROUTINE file_fbaseclose

!=============================================================================================     
   SUBROUTINE file_fbaseopenBin ( self, name, stat, access )
!============================================================================================= 
   class    (fBin_t),           intent(in out) :: self
   character(len=* ),           intent(in    ) :: name 
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: access 
!---------------------------------------------------------------------------------------------    
!
!---------------------------------------------------------------------------------------------    

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'file_fbaseopenBin'
   integer                     :: ios
   character(LGSTR)            :: iomsg
!---------------------------------------------------------------------------------------------    
    
   self%name = name
   if ( present(access) ) then
      open(newunit=self%unit, file=name, form='unformatted', action='read', &
           status = 'old', iostat=ios, iomsg = iomsg, access=access)
   else
      open(newunit=self%unit, file=name, form='unformatted', action='read', &
           status = 'old', iostat=ios, iomsg = iomsg)   
   end if
        
   if ( ios /= 0 ) &
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'Unable to open the file "'//name//'"'//NLT// &
                      'I/O message: '//trim(iomsg) )
                      
   END SUBROUTINE file_fbaseopenBin

!=============================================================================================   
   SUBROUTINE file_fbaserSpk0ReaderBin ( self, x, stat, what )
!=============================================================================================  
   class    (fBin_t),           intent(in out) :: self
   real     (rSp   ),           intent(in out) :: x
   type     (err_t ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      
   
   character(len=*), parameter :: HERE = 'file_fbaserSpk0ReaderBin'

#include "inc/file_fbaseReadBin.inc"
   
   END SUBROUTINE file_fbaserSpk0ReaderBin

!=============================================================================================
   SUBROUTINE file_fbaserSpk1ReaderBin ( self, x, stat, what )
!=============================================================================================   
   class    (fBin_t),           intent(in out) :: self
   real     (rSp   ),           intent(in out) :: x(:)
   type     (err_t ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserSpk1ReaderBin'

#include "inc/file_fbaseReadBin.inc"
   
   END SUBROUTINE file_fbaserSpk1ReaderBin

!=============================================================================================
   SUBROUTINE file_fbaserSpk2ReaderBin ( self, x, stat, what )
!=============================================================================================
   class    (fBin_t),           intent(in out) :: self
   real     (rSp   ),           intent(in out) :: x(:,:)
   type     (err_t ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what      
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserSpk2ReaderBin'
   
#include "inc/file_fbaseReadBin.inc"

   END SUBROUTINE file_fbaserSpk2ReaderBin

!=============================================================================================  
   SUBROUTINE file_fbaserDpk0ReaderBin ( self, x, stat, what )
!=============================================================================================   
   class    (fBin_t),           intent(in out) :: self
   real     (rDp   ),           intent(in out) :: x
   type     (err_t ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what      
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserDpk0ReaderBin'

#include "inc/file_fbaseReadBin.inc"
   
   END SUBROUTINE file_fbaserDpk0ReaderBin

!=============================================================================================
   SUBROUTINE file_fbaserDpk1ReaderBin ( self, x, stat, what )
!=============================================================================================
   class    (fBin_t),           intent(in out) :: self
   real     (rDp   ),           intent(in out) :: x(:)
   type     (err_t ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what      
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserDpk1ReaderBin'

#include "inc/file_fbaseReadBin.inc"

   END SUBROUTINE file_fbaserDpk1ReaderBin

!=============================================================================================
   SUBROUTINE file_fbaserDpk2ReaderBin ( self, x, stat, what )
!=============================================================================================
   class    (fBin_t),           intent(in out) :: self
   real     (rDp   ),           intent(in out) :: x(:,:)
   type     (err_t ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what      
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserDpk2ReaderBin'

#include "inc/file_fbaseReadBin.inc"

   END SUBROUTINE file_fbaserDpk2ReaderBin

!=============================================================================================          
   SUBROUTINE file_fbaseI32k0ReaderBin ( self, x, stat, what )
!=============================================================================================  
   class    (fBin_t),           intent(in out) :: self
   integer  (i32   ),           intent(in out) :: x
   type     (err_t ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what        
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI32k0ReaderBin'

#include "inc/file_fbaseReadBin.inc"
   
   END SUBROUTINE file_fbaseI32k0ReaderBin

!=============================================================================================
   SUBROUTINE file_fbaseI32k1ReaderBin ( self, x, stat, what )
!=============================================================================================  
   class    (fBin_t),           intent(in out) :: self
   integer  (i32   ),           intent(in out) :: x(:)
   type     (err_t ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what      
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI32k1ReaderBin'

#include "inc/file_fbaseReadBin.inc"
   
   END SUBROUTINE file_fbaseI32k1ReaderBin

!=============================================================================================
   SUBROUTINE file_fbaseI32k2ReaderBin ( self, x, stat, what )
!=============================================================================================
   class    (fBin_t),           intent(in out) :: self
   integer  (i32   ),           intent(in out) :: x(:,:)
   type     (err_t ),           intent(in out) :: stat 
   character(len=* ), optional, intent(in    ) :: what       
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI32k2ReaderBin'

#include "inc/file_fbaseReadBin.inc"

   END SUBROUTINE file_fbaseI32k2ReaderBin   

!=============================================================================================          
   SUBROUTINE file_fbaseI64k0ReaderBin ( self, x, stat, what )
!=============================================================================================  
   class    (fBin_t),           intent(in out) :: self
   integer  (i64   ),           intent(in out) :: x
   type     (err_t ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what      
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI64k0ReaderBin'

#include "inc/file_fbaseReadBin.inc"
   
   END SUBROUTINE file_fbaseI64k0ReaderBin

!=============================================================================================
   SUBROUTINE file_fbaseI64k1ReaderBin ( self, x, stat, what )
!=============================================================================================  
   class    (fBin_t),           intent(in out) :: self
   integer  (i64   ),           intent(in out) :: x(:)
   type     (err_t ),           intent(in out) :: stat 
   character(len=* ), optional, intent(in    ) :: what       
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI64k1ReaderBin'

#include "inc/file_fbaseReadBin.inc"

   END SUBROUTINE file_fbaseI64k1ReaderBin

!=============================================================================================
   SUBROUTINE file_fbaseI64k2ReaderBin ( self, x, stat, what )
!=============================================================================================
   class    (fBin_t),           intent(in out) :: self
   integer  (i64   ),           intent(in out) :: x(:,:)
   type     (err_t ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what      
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI64k2ReaderBin'

#include "inc/file_fbaseReadBin.inc"

   END SUBROUTINE file_fbaseI64k2ReaderBin   
   
!=============================================================================================  
   SUBROUTINE file_fbaseStrReaderBin ( self, str, stat, what )
!=============================================================================================  
   use, intrinsic :: iso_fortran_env, only: iostat_end
   class    (fBin_t),              intent(in out) :: self
   character(len=: ), allocatable, intent(in out) :: str
   type     (err_t ),              intent(in out) :: stat  
   character(len=* ), optional,    intent(in    ) :: what      
!---------------------------------------------------------------------------------------------      
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'file_fbaseStrReaderBin'
   integer                     :: i, n, ios
!--------------------------------------------------------------------------------------------- 

   n = 100 
   self%line = self%line + 1
   do 
      str = repeat(' ',n)
      read(self%unit,iostat=ios) (str(i:i), i = 1, n)
      if (ios /= 0) exit
      n = n * 10
      backspace(self%unit)
   end do
   str = trim(str)
   
   ! return with stat = EOF if ios = iostat_end:
   
   if (ios == iostat_end) stat = err_t ( stat = EOF, where = HERE, &
                                          msg = '(end of file "'//self%name//'" reached)' )
   
   END SUBROUTINE file_fbaseStrReaderBin  
   
!=============================================================================================  
   SUBROUTINE file_fbaseCharReaderBin ( self, nch, ch, stat, what )
!=============================================================================================  
   class    (fBin_t ),           intent(in out) :: self
   integer  (Ikind  ),           intent(in    ) :: nch
   character(len=nch),           intent(in out) :: ch
   type     (err_t  ),           intent(in out) :: stat  
   character(len=*  ), optional, intent(in    ) :: what      
!---------------------------------------------------------------------------------------------      
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter   :: HERE = 'file_fbaseCharReaderBin' 
   character(len=:), allocatable :: msg
   integer                       :: ios
!--------------------------------------------------------------------------------------------- 

   ch = ''
   self%line = self%line + 1
   read(self%unit, iostat=ios) ch
        
   if (ios > 0) then
      msg = 'Read error'
      if ( present(what) ) msg = msg // ' (while reading '//what//')'
      stat = err_t (stat=UERROR, where=HERE, msg=msg)
   end if
   
   END SUBROUTINE file_fbaseCharReaderBin     
       
!
!- Pour fichier ascii
!
!=============================================================================================
   SUBROUTINE file_fbaseopenAscii ( self, name, stat, access )
!=============================================================================================   
   class    (fAscii_t),           intent(in out) :: self
   character(len=*   ),           intent(in    ) :: name
   type     (err_t   ),           intent(in out) :: stat
   character(len=*   ), optional, intent(in    ) :: access   
!---------------------------------------------------------------------------------------------    
!
!---------------------------------------------------------------------------------------------    

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'file_fbaseopenAscii'
   integer                     :: ios
   character(LGSTR)            :: iomsg
!---------------------------------------------------------------------------------------------   
   self%name = name
   
   if ( present(access) ) then
      open(newunit=self%unit, file=name, action='read', status='old', &
           iostat=ios, iomsg=iomsg, access=access)
   else
      open(newunit=self%unit, file=name, action='read', status='old', &
           iostat=ios, iomsg=iomsg)
   end if
   
   if ( ios /= 0 ) &
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'Unable to open the file "'//name//'"'//NLT// &
                      'I/O message: '//trim(iomsg) )
                         
   END SUBROUTINE file_fbaseopenAscii
        

!=============================================================================================      
   SUBROUTINE file_fbaserSpk0ReaderAscii ( self, x, stat, what )
!=============================================================================================  
   class  (fAscii_t),           intent(in out) :: self
   real   (rSp     ),           intent(in out) :: x
   type   (err_t   ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserSpk0ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaserSpk0ReaderAscii

!=============================================================================================
   SUBROUTINE file_fbaserSpk1ReaderAscii ( self, x, stat, what )
!=============================================================================================
   class  (fAscii_t),           intent(in out) :: self
   real   (rSp     ),           intent(in out) :: x(:)
   type   (err_t   ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what   
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserSpk1ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaserSpk1ReaderAscii

!=============================================================================================
   SUBROUTINE file_fbaserSpk2ReaderAscii ( self, x, stat, what )
!=============================================================================================  
   class  (fAscii_t),           intent(in out) :: self
   real   (rSp     ),           intent(in out) :: x(:,:)
   type   (err_t   ),           intent(in out) :: stat
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserSpk2ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaserSpk2ReaderAscii

!=============================================================================================   
   SUBROUTINE file_fbaserDpk0ReaderAscii ( self, x, stat, what )
!=============================================================================================  
   class  (fAscii_t),           intent(in out) :: self
   real   (rDp     ),           intent(in out) :: x
   type   (err_t   ),           intent(in out) :: stat 
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserDpk0ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaserDpk0ReaderAscii

!=============================================================================================
   SUBROUTINE file_fbaserDpk1ReaderAscii ( self, x, stat, what )
!=============================================================================================   
   class  (fAscii_t),           intent(in out) :: self
   real   (rDp     ),           intent(in out) :: x(:)
   type   (err_t   ),           intent(in out) :: stat 
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserDpk1ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaserDpk1ReaderAscii

!=============================================================================================
   SUBROUTINE file_fbaserDpk2ReaderAscii ( self, x, stat, what )
!=============================================================================================   
   class  (fAscii_t),           intent(in out) :: self
   real   (rDp     ),           intent(in out) :: x(:,:)
   type   (err_t   ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaserDpk2ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaserDpk2ReaderAscii

!=============================================================================================            
   SUBROUTINE file_fbaseI32k0ReaderAscii ( self, x, stat, what )
!=============================================================================================   
   class  (fAscii_t),           intent(in out) :: self
   integer(i32     ),           intent(in out) :: x
   type   (err_t   ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI32k0ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaseI32k0ReaderAscii  

!=============================================================================================
   SUBROUTINE file_fbaseI32k1ReaderAscii ( self, x, stat, what )
!=============================================================================================   
   class  (fAscii_t),           intent(in out) :: self
   integer(i32     ),           intent(in out) :: x(:)
   type   (err_t   ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI32k1ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaseI32k1ReaderAscii   

!=============================================================================================
   SUBROUTINE file_fbaseI32k2ReaderAscii ( self, x, stat, what )
!=============================================================================================   
   class  (fAscii_t),           intent(in out) :: self
   integer(i32     ),           intent(in out) :: x(:,:)
   type   (err_t   ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI32k2ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaseI32k2ReaderAscii      

!=============================================================================================            
   SUBROUTINE file_fbaseI64k0ReaderAscii ( self, x, stat, what )
!=============================================================================================   
   class  (fAscii_t),           intent(in out) :: self
   integer(i64     ),           intent(in out) :: x
   type   (err_t   ),           intent(in out) :: stat 
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI64k0ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
  
   END SUBROUTINE file_fbaseI64k0ReaderAscii  

!=============================================================================================
   SUBROUTINE file_fbaseI64k1ReaderAscii ( self, x, stat, what )
!=============================================================================================   
   class  (fAscii_t),           intent(in out) :: self
   integer(i64     ),           intent(in out) :: x(:)
   type   (err_t   ),           intent(in out) :: stat  
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI64k1ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaseI64k1ReaderAscii   

!=============================================================================================
   SUBROUTINE file_fbaseI64k2ReaderAscii ( self, x, stat, what )
!=============================================================================================   
   class  (fAscii_t),           intent(in out) :: self
   integer(i64     ),           intent(in out) :: x(:,:)
   type   (err_t   ),           intent(in out) :: stat 
   character(len=* ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      

   character(len=*), parameter :: HERE = 'file_fbaseI64k2ReaderAscii'
   
#include "inc/file_fbaseReadAscii.inc"
   
   END SUBROUTINE file_fbaseI64k2ReaderAscii      
   
!=============================================================================================
   SUBROUTINE file_fbaseStrReaderAscii ( self, str, stat, what )
!=============================================================================================  
   use, intrinsic :: iso_fortran_env, only: iostat_end
   class    (fAscii_t),              intent(in out) :: self
   character(len=:   ), allocatable, intent(in out) :: str
   type     (err_t   ),              intent(in out) :: stat  
   character(len=*   ), optional,    intent(in    ) :: what
!--------------------------------------------------------------------------------------------- 
         
!- local variables: -------------------------------------------------------------------------- 
   character(len=*  ), parameter   :: HERE = 'file_fbaseStrReaderAscii' 
   integer                         :: size, ios
   character(len=256)              :: buf
   character(len=:  ), allocatable :: msg
!--------------------------------------------------------------------------------------------- 

   str = ''
   self%line = self%line + 1
   do
      read(self%unit,'(a)', advance='no', iostat=ios, size=size) buf
    
      if (ios > 0) then 
         msg = 'Read error'
         if ( present(what) ) msg = msg // ' (while reading '//what//')'
         stat = err_t (stat=UERROR, where=HERE, msg=msg)
         return
      end if   

      str = str // buf(:size)
      
      ! if ios < 0 exit and return. Moreover if ios = iostat_end return with stat = EOF
        
      if (ios < 0) then
         !if (ios /= iostat_eor) &
         if (ios == iostat_end) stat = err_t (stat=EOF, where=HERE, &
                                               msg='(end of file "'//self%name//'" reached)')
         return
      end if
   end do
   
   END SUBROUTINE file_fbaseStrReaderAscii   
  
!=============================================================================================  
   SUBROUTINE file_fbaseCharReaderAscii ( self, nch, ch, stat, what )
!=============================================================================================  
   class    (fAscii_t),           intent(in out) :: self
   integer  (Ikind   ),           intent(in    ) :: nch
   character(len=nch ),           intent(in out) :: ch
   type     (err_t   ),           intent(in out) :: stat  
   character(len=*   ), optional, intent(in    ) :: what
!---------------------------------------------------------------------------------------------      
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*  ), parameter   :: HERE = 'file_fbaseCharReaderAscii' 
   character(len=:  ), allocatable :: msg
   integer                         :: ios
!--------------------------------------------------------------------------------------------- 

   ch = ''
   self%line = self%line + 1
   read(self%unit,'(a)',iostat=ios) ch
        
   if (ios > 0) then
      msg = 'Read error'
      if ( present(what) ) msg = msg // ' (while reading '//what//')'
      stat = err_t (stat=UERROR, where=HERE, msg=msg)
   end if
   
   END SUBROUTINE file_fbaseCharReaderAscii
   
                 
END MODULE file_m 