module horner_m
   use pk2mod_m, only: Rkind, Ikind
   private
   public :: horner

   interface horner
      module procedure horner1d, horner2d, horner3d
   end interface   
   
contains
!=============================================================================================    
   function numberOfCoef ( ndime, ndeg ) result ( ncoef )
!=============================================================================================     
   integer(Ikind), intent (in) :: ndime, ndeg
   integer(Ikind)               :: ncoef
!--------------------------------------------------------------------------------------------- 
   integer(Ikind) :: i, facndime
!---------------------------------------------------------------------------------------------

   facndime = 1
   do i = 1, ndime
      facndime = facndime * i
   end do
   
   ncoef = ndeg + 1
   do i = 1, ndime-1
      ncoef = ncoef * (ndeg + i + 1)
   end do
   ncoef = ncoef / facndime
   
   end function numberOfCoef

!=============================================================================================    
   subroutine horner1d ( c, x, p, dp )
!=============================================================================================     
   real(Rkind), intent (in    ) :: c(0:), x
   real(Rkind), intent (   out) :: p, dp
!--------------------------------------------------------------------------------------------- 
   integer(Ikind) :: i, ndeg
!---------------------------------------------------------------------------------------------
  
   ndeg = ubound(c,1)
   
   p = c(ndeg)
   dp = 0.0_Rkind
   do i = ndeg-1, 0, -1
      dp = dp * x + p
      p  = p  * x + c(i)
   end do

   end subroutine horner1d

!=============================================================================================    
   subroutine horner2d ( c, x, p, dp )
!=============================================================================================     
   real(Rkind), intent (in    ) :: c(0:,0:), x(:)
   real(Rkind), intent (   out) :: p, dp(:)
!--------------------------------------------------------------------------------------------- 
   integer(Ikind) :: j, n2
   real   (Rkind) :: b(0:ubound(c,2)), dpj, yj
!---------------------------------------------------------------------------------------------
   
   n2 = ubound(c,2)
   
   dp = 0.0_Rkind ; yj = 1.0_Rkind
   
   do j = 0, n2
      call horner1d ( c(0:,j), x(1), b(j), dpj ) 
      dp(1) = dp(1) + dpj * yj
      yj = yj * x(2)
   end do
   
   call horner1d ( b, x(2), p, dp(2) ) 

   end subroutine horner2d

!=============================================================================================    
   subroutine horner3d ( c, x, p, dp )
!=============================================================================================     
   real(Rkind), intent (in    ) :: c(0:,0:,0:), x(:)
   real(Rkind), intent (   out) :: p, dp(:)
!--------------------------------------------------------------------------------------------- 
   integer(Ikind) :: k, n3
   real   (Rkind) :: b(0:ubound(c,3)), dpk(2), zk
!---------------------------------------------------------------------------------------------
   
   n3 = ubound(c,3)
   
   dp = 0.0_Rkind ; zk = 1.0_Rkind 
   
   do k = 0, n3
      call horner2d ( c(0:,0:,k), x(1:2), b(k), dpk )
      dp(1:2) = dp(1:2) + dpk * zk
      zk = zk * x(3)
   end do

   call horner1d ( b, x(3), p, dp(3) ) 
   
   end subroutine horner3d      
   
end module horner_m

use pk2mod_m, only: Rkind, Ikind
use horner_m

implicit none
integer(Ikind), parameter :: ndeg = 2
integer(Ikind) :: ndime, ncoef

real(Rkind), allocatable :: c(:,:,:), x(:), dp(:),cc(:,:)
real(Rkind) :: p

allocate(c(0:ndeg,0:ndeg,0:ndeg),x(3),dp(3))




x=[-1.4,0.3,-0.4]


! p(x) = 1 + 2x + 3x^2 ; dp = 2 + 6x
c(0,0,0)=1; c(1,0,0)=2; c(2,0,0)=3
call horner(c(:,0,0),x(1),p,dp(1))
print*,'horner(1d): ','p=',p,"p'=",dp(1)

! p(x) = 1 + 2x + 3y + 4xy + 0x2 + 6y2 ; dp=[2+4y+10x , 3+4x+12y]
c(0,0,0)=1; c(1,0,0)=2; c(2,0,0)=0; c(1,1,0)=4; c(0,1,0)=3; c(0,2,0)=6
call horner(c(:,:,0),x(1:2),p,dp(1:2))
print*,'horner(2d): ','p=',p,'gradp=',dp(1:2)


allocate(cc(0:1,0:2))
cc(0,0)=1; cc(1,0)=2; cc(1,1)=4; cc(0,1)=3; cc(0,2)=6

call horner(cc,x(1:2),p,dp(1:2))
print*,'horner(2d): ','p=',p,'gradp=',dp(1:2)

! p(x,y,z) = 1 + 2x + 3y + 4z + 5xy + 6yz + 7zx + 8x2 + 9y2 + 10z2
! dp=[2+5y+7z+16x, 3+5x+6z+18y, 4+6y+7x+20z]
c = 0.0
c(0,0,0)=1; c(1,0,0)=2; c(2,0,0)=8; c(1,1,0)=5; c(1,0,1)=7; c(0,1,1)=6
c(0,1,0)=3; c(0,2,0)=9; c(0,0,1)=4; c(0,0,2)=10
call horner(c,x,p,dp)
print*,'horner(3d): ','p=',p,'gradp=',dp


! p(x,y,z) = 1 + 2x + 4z + 7zx + 8x2  
! dp=[2+7z+16x, 0, 4+7x]
deallocate(c) ; allocate(c(0:2,0:0,0:1))
c = 0.0
c(0,0,0)=1; c(1,0,0)=2; c(2,0,0)=8;  c(1,0,1)=7; 
c(0,0,1)=4; 
call horner(c,x,p,dp)
print*,'horner(3d): ','p=',p,'gradp=',dp

contains


      
end