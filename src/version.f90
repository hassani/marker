!---------------------------------------------------------------------------------------------
! marker, version 2023.4.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       version_m
!
! Description: 
!       All that depends on the version
! 
! Notes: 
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        04/23
!
! Changes:
!        
!---------------------------------------------------------------------------------------------

MODULE version_m
         
   implicit none
   
   private
   public :: version_getVersion
   
   character(len=*), parameter :: VERSIONID = '2023.4.0'      ! release(year.month).version
   character(len=*), parameter :: FDEF = '.ptovtuUserDefault' ! user default file

CONTAINS

!=============================================================================================
   SUBROUTINE version_getVersion 
!=============================================================================================
   use globalParameters_m
!---------------------------------------------------------------------------------------------

   numversion = VERSIONID
   filedef    = FDEF
   
#ifdef __CDATE
   compilDate = __CDATE
#else
   compilDate = ""
#endif   

#ifdef __COPTS   
   compilOPts = __COPTS
#else
   compilOPts = ""
#endif     

   END SUBROUTINE version_getVersion
      
END MODULE version_m   