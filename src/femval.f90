!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       femval_m
!
! Description: 
!       Define a DT to gather the data (real values) of a f.e. calculation
! 
! Notes: 
!       
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        06/22
!
! Changes:
!        07/22
!        
!---------------------------------------------------------------------------------------------

#include "error.fpp"

MODULE femval_m
   
   use file_m,            only: file_t
   use units_m,           only: phQ_t
   use constantsmarker_m, only: Ikind, Rkind, i64, rSp, rDp, &
                                UERROR, WARNING, IZERO, IONE, NLT

   use pk2mod_m,          only: str_t, err_t, util_IntToChar, disp   
   
   implicit none
   
   private
   public :: femval_t, femval_init, femval_displayVark1
      
   type, abstract :: femval_t
      ! name of the set:
      character(len=:), allocatable :: name
      ! number of variables:
      integer  (Ikind)              :: nvar = 0      
      ! name and number of entities (nodes/cells) to which these variables are attached:
      character(len=:), allocatable :: geomEntities
      integer  (Ikind)              :: nent = 0
      ! variable names, physical quantity names and their physical units:
      type     (str_t), allocatable :: varNames (:), physNames (:)
      type     (str_t), allocatable :: physUnits(:), physUnits0(:)
      ! number of components of each variables:
      integer  (Ikind), allocatable :: varNcomp(:) 
      ! optionally conversion factors and conversion operation between units:
      logical         , allocatable :: please_convertMe(:) 
      real     (Rkind), allocatable :: convUnit(:)
      character(len=1), allocatable :: convOper(:)
      ! (useful) size of v (the actual size may be greater than vsize):
      integer  ( i64 )              :: usedSize = 0
      ! set is_new = .true. when the femval_t variable is reused with a different set of data 
      logical                       :: is_new = .true., is_active = .false.
   contains
      procedure, pass(self) :: InitUnitConversion => femval_InitUnitConversion
      procedure, pass(self) :: displayVar         => femval_displayVar
      ! deferred methods:
      procedure(interf_alloc   ), deferred, pass(self) :: alloc
      procedure(interf_read    ), deferred, pass(self) :: read      
      procedure(interf_setAnElm), deferred, pass(self) :: setElm    
      procedure(interf_setAVar ), deferred, pass(self) :: set    
      procedure(interf_getVal  ), deferred, pass(self) :: getVal    
   end type femval_t  
   
   type, extends(femval_t) :: vSp_t
      private
      ! the set of values (simple precision version)
      real(rSp), allocatable :: v(:)
   contains
      private
      procedure, public, pass(self) :: alloc       =>  femval_allocSp   , &
                                       read        =>  femval_readSp    , &
                                       setElm      =>  femval_setAnElmSp, &
                                       set         =>  femval_setAVarSp , &
                                       getVal      =>  femval_getValuesSp 
   end type vSp_t

   type, extends(femval_t) :: vDp_t
      private
      ! the set of values (double precision version)
      real(rDp), allocatable :: v(:)
   contains
      private
      procedure, public, pass(self) :: alloc       =>  femval_allocDp   , &
                                       read        =>  femval_readDp    , &
                                       setElm      =>  femval_setAnElmDp, &
                                       set         =>  femval_setAVarDp , &
                                       getVal      =>  femval_getValuesDp 
   end type vDp_t   


    abstract interface
       subroutine interf_alloc ( self, nvar, nent, allocv, name ) 
          import :: femval_t
          import :: Ikind
          class    (femval_t),           intent(in out) :: self 
          integer  (Ikind)   , optional, intent(in    ) :: nvar, nent
          logical            , optional, intent(in    ) :: allocv
          character(len=*   ), optional, intent(in    ) :: name                
       end subroutine interf_alloc            
       
       subroutine interf_read ( self, file, stat ) 
          import :: femval_t, file_t, err_t
          class    (femval_t),           intent(in out) :: self 
          class    (file_t  ),           intent(in out) :: file
          type     (err_t   ),           intent(in out) :: stat   
       end subroutine interf_read  

       subroutine interf_setAnElm ( self, values, varId, entId )!, stat ) 
          import :: femval_t, Ikind, Rkind, err_t
          class  (femval_t), intent(in out) :: self 
          real   (Rkind   ), intent(in    ) :: values(:)
          integer(Ikind   ), intent(in    ) :: varId, entId
          !type   (err_t   ), intent(in out) :: stat   
       end subroutine interf_setAnElm  

       subroutine interf_setAVar ( self, values, varId, stat ) 
          import :: femval_t, Ikind, Rkind, err_t
          class  (femval_t), intent(in out) :: self 
          real   (Rkind   ), intent(in    ) :: values(:)
          integer(Ikind   ), intent(in    ) :: varId
          type   (err_t   ), intent(in out) :: stat   
       end subroutine interf_setAVar  

       subroutine interf_getVal ( self, values, stat, varId, entId ) 
          import :: femval_t, Ikind, Rkind, err_t
          class  (femval_t),              intent(in    ) :: self 
          real   (Rkind   ), allocatable, intent(in out) :: values(:)
          type   (err_t   ),              intent(in out) :: stat   
          integer(Ikind   ), optional   , intent(in    ) :: varId, entId          
       end subroutine interf_getVal  
              
    end interface

   interface femval_init
      module procedure femval_InitRank0
      module procedure femval_InitRank1
   end interface
          
CONTAINS

!=============================================================================================
   SUBROUTINE femval_InitRank0 ( name, kindOfReal, stat, res )
!=============================================================================================   
   character(len=*   ),              intent(in    ) :: name
   integer  (Ikind   ),              intent(in    ) :: kindOfReal
   type     (err_t   ),              intent(in out) :: stat
   class    (femval_t), allocatable, intent(   out) :: res   
!--------------------------------------------------------------------------------------------- 
!  Initializes a femval_t variable according to the kind of real.
!---------------------------------------------------------------------------------------------    

   if ( kindOfReal == 4 ) then
      allocate(vSp_t :: res)  
   else if ( kindOfReal == 8 ) then
      allocate(vDp_t :: res) 
   else
      stat = err_t ( stat = UERROR, where = 'femval_init', &
                      msg = 'unintended kind of real ('//util_intToChar(kindOfReal)//')' )
      return   
   end if
   
   res%name = name
      
   END SUBROUTINE femval_InitRank0
   

!=============================================================================================
   SUBROUTINE femval_InitRank1 ( names, kindOfReal, stat, res )
!=============================================================================================   
   type   (str_t   ),              intent(in    ) :: names(:)
   integer(Ikind   ),              intent(in    ) :: kindOfReal
   type   (err_t   ),              intent(in out) :: stat
   class  (femval_t), allocatable, intent(   out) :: res(:)   
!--------------------------------------------------------------------------------------------- 
!  Initializes a femval_t array according to the kind of real.
!----------------------------------------------------------------------------------- R.H. 2022

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'femvalRank1_init'
   integer                     :: i
!--------------------------------------------------------------------------------------------- 

   if ( kindOfReal == 4 ) then
      allocate(vSp_t :: res(size(names)))  
   else if ( kindOfReal == 8 ) then
      allocate(vDp_t :: res(size(names)))
   else
      stat = err_t ( stat = UERROR, where = HERE, &
                      msg = 'unintended kind of real ('//util_intToChar(kindOfReal)//')' )
      return   
   end if
   
   do i = 1, size(names)
      res(i)%name = names(i)%str
   end do
      
   END SUBROUTINE femval_InitRank1   
   
   
!=============================================================================================
   SUBROUTINE femval_setAnElmSp ( self, values, varId, entId )!, stat )
!=============================================================================================  
   class    (vSp_t), intent(in out) :: self
   real     (Rkind), intent(in    ) :: values(:)
   integer  (Ikind), intent(in    ) :: varId, entId
   !type     (err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Sets the values of the variable #varId attached to the entity #entId
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   integer(i64) :: offset, i1, i2
!--------------------------------------------------------------------------------------------- 

   if ( varId == 1 ) then
      offset = 0
   else
      offset = sum ( self%varNcomp(1:varId-1) ) * self%nent
   end if
   
   i1 = offset + (entId-1)*self%varNcomp(varId) + 1 ; i2 = i1 + self%varNcomp(varId) - 1
   
   self%v(i1:i2) = values(:) ! potential conversion from Rkind to rSp
   
   END SUBROUTINE femval_setAnElmSp

!=============================================================================================
   SUBROUTINE femval_setAnElmDp ( self, values, varId, entId )!, stat )
!=============================================================================================  
   class    (vDp_t), intent(in out) :: self
   real     (Rkind), intent(in    ) :: values(:)
   integer  (Ikind), intent(in    ) :: varId, entId
   !type     (err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Sets the values of the variable #varId attached to the entity #entId
!---------------------------------------------------------------------------------------------

!- local variables ---------------------------------------------------------------------------
   integer(i64) :: offset, i1, i2
!--------------------------------------------------------------------------------------------- 

   if ( varId == 1 ) then
      offset = 0
   else
      offset = sum ( self%varNcomp(1:varId-1) ) * self%nent
   end if
   
   i1 = offset + (entId-1)*self%varNcomp(varId) + 1 ; i2 = i1 + self%varNcomp(varId) - 1
   
   self%v(i1:i2) = values(:) ! potential conversion from Rkind to rDp
   
   END SUBROUTINE femval_setAnElmDp


!=============================================================================================
   SUBROUTINE femval_setAVarSp ( self, values, varId, stat )
!=============================================================================================  
   class    (vSp_t), intent(in out) :: self
   real     (Rkind), intent(in    ) :: values(:)
   integer  (Ikind), intent(in    ) :: varId   
   type     (err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Sets all the values of the variable #varId
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'femval_setAVarSp'
   integer  ( i64 )            :: i1, i2
!--------------------------------------------------------------------------------------------- 

   if ( varId == 1 ) then
      i1 = 1
   else
      i1 = sum ( self%varNcomp(1:varId-1) ) * self%nent + 1 
   end if

   i2 = i1 + self%varNcomp(varId) * self%nent - 1

   if ( size(values) /= i2-i1+1 ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Incompatible size for "values"')
      return   
   end if
      
   self%v(i1:i2) = values(:) ! potential conversion from Rkind to rSp
   
   END SUBROUTINE femval_setAVarSp
   
!=============================================================================================
   SUBROUTINE femval_setAVarDp ( self, values, varId, stat )
!=============================================================================================  
   class    (vDp_t), intent(in out) :: self
   real     (Rkind), intent(in    ) :: values(:)   
   integer  (Ikind), intent(in    ) :: varId   
   type     (err_t), intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Sets all the values of the variable #varId
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'femval_setAVarSp'
   integer  ( i64 )            :: i1, i2
!--------------------------------------------------------------------------------------------- 

   if ( varId == 1 ) then
      i1 = 1
   else
      i1 = sum ( self%varNcomp(1:varId-1) ) * self%nent + 1 
   end if

   i2 = i1 + self%varNcomp(varId) * self%nent - 1

   if ( size(values) /= i2-i1+1 ) then
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Incompatible size for "values"')
      return   
   end if
   
   self%v(i1:i2) = values(:) ! potential conversion from Rkind to rDp
   
   END SUBROUTINE femval_setAVarDp
   

!=============================================================================================
   SUBROUTINE femval_getValuesSp ( self, values, stat, varId, entId )
!=============================================================================================  
   class    (vSp_t),              intent(in    ) :: self
   real     (Rkind), allocatable, intent(in out) :: values(:)
   type     (err_t),              intent(in out) :: stat
   integer  (Ikind), optional   , intent(in    ) :: varId, entId   
!--------------------------------------------------------------------------------------------- 
!  . When varId and entId are present:
!        gets the value(s) of the varId-th variable attached to entId-th entity
!  . when only varId is present:
!        gets the values of the varId-th variable attached to all entities
!  . when only entId is present:
!        get the values of all variables attached to the entId-th entity
!
!  Notes:
!  . Possible conversion from rSp (self%v) to Rkind (values)
!  . The values are expressed according to the choosen units
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'femval_getValuesSp'
!--------------------------------------------------------------------------------------------- 

#include "inc/femval_getValues.inc"

   END SUBROUTINE femval_getValuesSp

!=============================================================================================
   SUBROUTINE femval_getValuesDp ( self, values, stat, varId, entId )
!=============================================================================================  
   class    (vDp_t),              intent(in    ) :: self
   real     (Rkind), allocatable, intent(in out) :: values(:)
   type     (err_t),              intent(in out) :: stat
   integer  (Ikind), optional   , intent(in    ) :: varId, entId   
!--------------------------------------------------------------------------------------------- 
!  (see femval_getValuesSp)
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'femval_getValuesDp'
!--------------------------------------------------------------------------------------------- 

#include "inc/femval_getValues.inc"
   
   END SUBROUTINE femval_getValuesDp 
             
!=============================================================================================
   SUBROUTINE femval_getVarOfEntSp ( self, varId, entId, values )!, stat )
!=============================================================================================  
   class    (vSp_t),              intent(in    ) :: self
   integer  (Ikind),              intent(in    ) :: varId, entId
   real     (Rkind), allocatable, intent(in out) :: values(:)
   !type     (err_t),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Gets the value of the varId-th variable attached to entId-th entity
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   integer( i64 ) :: i0, k1, k2
   integer(Ikind) :: n
!--------------------------------------------------------------------------------------------- 

   if ( varId == 1 ) then
      i0 = 0
   else
      i0 = sum ( self%varNcomp(1:varId-1) ) * max(1_Ikind,self%nent)
   end if

   n = self%varNcomp(varId)
   k2 = i0 + entId * n
   k1 = k2 - n + 1

   if ( allocated(values) ) then
      if ( size(values) < n ) then
         deallocate(values) ; allocate(values(n))
      end if
   else
      allocate(values(n))
   end if
      
   values(1:n) = self%v(k1:k2) ! potential conversion from rSp to Rkind
   
   END SUBROUTINE femval_getVarOfEntSp

!=============================================================================================
   SUBROUTINE femval_getVarOfEntDp ( self, varId, entId, values )!, stat )
!=============================================================================================  
   class    (vDp_t),              intent(in    ) :: self
   integer  (Ikind),              intent(in    ) :: varId, entId
   real     (Rkind), allocatable, intent(in out) :: values(:)
   !type     (err_t),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Gets the value of the varId-th variable attached to entId-th entity
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   integer( i64 ) :: i0, k1, k2
   integer(Ikind) :: n
!--------------------------------------------------------------------------------------------- 

   if ( varId == 1 ) then
      i0 = 0
   else
      i0 = sum ( self%varNcomp(1:varId-1) ) * max(1_Ikind,self%nent)
   end if

   n = self%varNcomp(varId)
   k2 = i0 + entId * n
   k1 = k2 - n + 1

   if ( allocated(values) ) then
      if ( size(values) < n ) then
         deallocate(values) ; allocate(values(n))
      end if
   else
      allocate(values(n))
   end if
      
   values(1:n) = self%v(k1:k2) ! potential conversion from rSp to Rkind

   END SUBROUTINE femval_getVarOfEntDp
   

!=============================================================================================
   SUBROUTINE femval_getAVarSp ( self, varId, values )!, stat )
!=============================================================================================  
   class    (vSp_t),              intent(in    ) :: self
   integer  (Ikind),              intent(in    ) :: varId   
   real     (Rkind), allocatable, intent(in out) :: values(:)
   !type     (err_t),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Gets the values of the varId-th variable for all entities
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   integer( i64 ) :: i1, i2
   integer(Ikind) :: n
!--------------------------------------------------------------------------------------------- 

   if ( varId == 1 ) then
      i1 = 1
   else
      i1 = sum ( self%varNcomp(1:varId-1) ) * max(1_Ikind,self%nent) + 1 
   end if
   
   n = self%varNcomp(varId) *  max(1_Ikind,self%nent) 
   
   if ( allocated(values) ) then
      if ( size(values) < n ) then
         deallocate(values) ; allocate(values(n))
      end if
   else
      allocate(values(n))
   end if
   
   i2 = i1 + n - 1
   
   values(1:n) = self%v(i1:i2) ! potential conversion from rSp to Rkind
   
   END SUBROUTINE femval_getAVarSp
   
!=============================================================================================
   SUBROUTINE femval_getAVarDp ( self, varId, values )!, stat )
!=============================================================================================  
   class    (vDp_t),              intent(in    ) :: self
   integer  (Ikind),              intent(in    ) :: varId   
   real     (Rkind), allocatable, intent(in out) :: values(:)
   !type     (err_t),              intent(in out) :: stat
!--------------------------------------------------------------------------------------------- 
!  Gets the values of the varId-th variable for all entities
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   integer( i64 ) :: i1, i2
   integer(Ikind) :: n
!--------------------------------------------------------------------------------------------- 

   if ( varId == 1 ) then
      i1 = 1
   else
      i1 = sum ( self%varNcomp(1:varId-1) ) * max(1_Ikind,self%nent) + 1 
   end if
   
   n = self%varNcomp(varId) * max(1_Ikind,self%nent)  
   
   if ( allocated(values) ) then
      if ( size(values) < n ) then
         deallocate(values) ; allocate(values(n))
      end if
   else
      allocate(values(n))
   end if
   
   i2 = i1 + n - 1
   
   values(1:n) = self%v(i1:i2) ! potential conversion from rDp to Rkind
   
   END SUBROUTINE femval_getAVarDp
         

!=============================================================================================
   SUBROUTINE femval_InitUnitConversion ( self, catalog, conversion, stat )
!=============================================================================================
   class(femval_t), intent(in out) :: self
   type (phQ_t   ), intent(in    ) :: catalog(:)
   logical        , intent(in    ) :: conversion
   type (err_t   ), intent(in out) :: stat   
!--------------------------------------------------------------------------------------------- 
!  Initializes the conversions to be performed for all the variables 
!---------------------------------------------------------------------------------------------  

!- local variables ---------------------------------------------------------------------------
   character(len=*), parameter :: HERE = 'femval_InitUnitConversion'
   integer                     :: i, var, physqFound, unitFound, choosen
!----------------------------------------------------------------------------------- R.H. 2022
 
   if ( .not. conversion ) then
      self%please_convertMe(1:self%nvar) = .false.
      return
   end if
   
   do var = 1, self%nvar
   
      if ( self%physNames(var) == '-' .or. self%physNames(var) == '' ) then
         self%please_convertMe(var) = .false.
         cycle
      end if
!
!-    See if the physical quantity of the var-th variable is referenced in the catalog:
!
      physqFound = 0
      do i = 1, size(catalog)
         if ( .not. catalog(i)%is_init ) cycle
         if ( catalog(i)%quantityName == self%physNames(var) ) then
            physqFound = i ; exit
         end if
      end do

      if (physqFound == 0) then
         stat = err_t ( msg = 'Physical quantity "'// self%physNames(var)%str // '" ' //  &
                              'not found in the catalog.' // NLT //                       &
                              'No conversion will be done for this quantity.',            &
                        stat = WARNING, where = HERE                                      ) 
         self%please_convertMe(var) = .false.
         cycle
      end if
!
!-    See if the unit given for this quantity is present in the list of units used for such 
!     physical quantity in the catalog:
! 
      unitFound = 0
      do i = 1, size(catalog(physqFound)%unitNames)
         if ( catalog(physqFound)%unitNames(i) == self%physUnits(var) ) then
            unitFound = i ; exit
         end if
      end do
 
      if (unitFound == 0) then
         stat = err_t ( msg = 'Unexpected unit "'// self%physUnits(var)%str // '" for ' // &
                              'physical quantity "'// self%physNames(var)%str//'".'//NLT// &
                              'No conversion will be done for this quantity.',             &
                        stat = WARNING, where = HERE ) 
         self%please_convertMe(var) = .false.
         cycle
      end if                              
!
!-    The unit # choosen by the user:
!      
      choosen = catalog(physqFound)%choosenUnit
!
!-    Store the new unit (save the original one in %physUnits0):
!      
      self%physUnits0(var)%str = self%physUnits(var)%str
      self%physUnits (var)%str = catalog(physqFound)%unitNames(choosen)%str
      
      if ( choosen == unitFound ) then
         self%please_convertMe(var) = .false.
      else
         self%please_convertMe(var) = .true.      
!
!-       Compute the conversion factor (or term) to switch from the initial unit to the one 
!        chosen by the user:
!
         if ( catalog(physqFound)%conversionOperator == '*' ) then
!
!-          the values of this variable will be multiplied by self%convUnit(var)    
!   
            self%convUnit(var) = catalog(physqFound)%conversionTable(unitFound) / &
                                 catalog(physqFound)%conversionTable(choosen) 
            self%convOper(var) = '*'
  
         else
!
!-          self%convUnit(var) will be added to the values of this variable    
!       
            self%convUnit(var) = catalog(physqFound)%conversionTable(unitFound) - & 
                                 catalog(physqFound)%conversionTable(choosen)
            self%convOper(var) = '+'
         end if  
      end if
   
   end do
   
   END SUBROUTINE femval_InitUnitConversion
   
   
!=============================================================================================
   SUBROUTINE femval_allocSp ( self, nvar, nent, allocv, name )
!=============================================================================================  
   class    (vSp_t),           intent(in out) :: self
   integer  (Ikind), optional, intent(in    ) :: nvar, nent
   logical         , optional, intent(in    ) :: allocv
   character(len=*), optional, intent(in    ) :: name
!--------------------------------------------------------------------------------------------- 
!  Allocates the members of self
!--------------------------------------------------------------------------------------------- 

#include "inc/femval_alloc.inc"
   END SUBROUTINE femval_allocSp
   
!=============================================================================================
   SUBROUTINE femval_allocDp ( self, nvar, nent, allocv, name )
!=============================================================================================  
   class    (vDp_t),           intent(in out) :: self
   integer  (Ikind), optional, intent(in    ) :: nvar, nent
   logical         , optional, intent(in    ) :: allocv
   character(len=*), optional, intent(in    ) :: name
!--------------------------------------------------------------------------------------------- 
#include "inc/femval_alloc.inc"
   END SUBROUTINE femval_allocDp


!=============================================================================================
   SUBROUTINE femval_readSp  ( self, file, stat )
!=============================================================================================
   class    (vSp_t ),           intent(in out) :: self
   class    (file_t),           intent(in out) :: file
   type     (err_t ),           intent(in out) :: stat  
!--------------------------------------------------------------------------------------------- 
!  Reads the member self%v
!--------------------------------------------------------------------------------------------- 
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'femval_readSp'
!---------------------------------------------------------------------------------------------    
    
   call self%alloc ( allocv = .true. )
       
   call file%read ( self%v(1:self%usedSize), stat, what = self%name )
   error_TraceNreturn(stat/=IZERO, HERE, stat)
   
   END SUBROUTINE femval_readSp

!=============================================================================================
   SUBROUTINE femval_readDp  ( self, file, stat )
!=============================================================================================
   class    (vDp_t ),           intent(in out) :: self
   class    (file_t),           intent(in out) :: file
   type     (err_t ),           intent(in out) :: stat  
!--------------------------------------------------------------------------------------------- 
!  Reads the member self%v
!--------------------------------------------------------------------------------------------- 
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'femval_readDp'
!---------------------------------------------------------------------------------------------    
    
   call self%alloc ( allocv = .true. )
    
   call file%read ( self%v(1:self%usedSize), stat, what = self%name )
   error_TraceNreturn(stat/=IZERO, HERE, stat)
   
   END SUBROUTINE femval_readDp


!=============================================================================================
   SUBROUTINE femval_displayVar ( self  )
!=============================================================================================
   class(femval_t), intent(in) :: self
!--------------------------------------------------------------------------------------------- 
!  Prints to the sdtout the names of the variables contained in self
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   integer         , parameter   :: ncell = 6
   character(len=:), allocatable :: mat(:,:), line
   type     (str_t)              :: cellNames(ncell)
   integer                       :: i, lenmax = 0, l(ncell) = 0
   integer  ( i64 )              :: i2
!--------------------------------------------------------------------------------------------- 
   
   cellNames(1) = 'Name'       ; cellNames(2) = 'Quantity' ; cellNames(3) = 'Unit'
   cellNames(4) = '(Original)' ; cellNames(5) = '# Comp.'  ; cellNames(6) = 'Index'
   
   do i = 1, ncell
      l(i) = len(cellNames(i)%str)
   end do
   
   i2 = 0
   do i = 1, self%nvar    
      l(1) = max(l(1),len_trim(self%varNames  (i)%str))
      l(2) = max(l(2),len_trim(self%physNames (i)%str))
      l(3) = max(l(3),len_trim(self%physUnits (i)%str))
      if ( len_trim(self%physUnits0(i)%str) > 0 ) &
         l(4) = max(l(4),len_trim(self%physUnits0(i)%str)+2)
      l(5) = max(l(5),len_trim(util_intToChar(self%varNcomp(i))))
      i2 = i2 + self%varNcomp(i) * self%nent      
      l(6) = max(l(6),len_trim(util_intToChar(i2)))
   end do    
   lenmax = maxval(l)
   
   allocate(character(len=lenmax) :: mat(self%nvar+2,ncell))
   
   line = ''
   do i = 1, ncell
      mat(1,i) = cellNames(i)%str
      mat(2,i) = repeat('=',l(i))
      if (i < ncell) line = line // trim(mat(2,i)) // '==='
   end do
   line = line // trim(mat(2,ncell))
   
   i2 = 0
   do i = 1, self%nvar    
      mat(i+2,1) = self%varNames  (i)%str
      mat(i+2,2) = self%physNames (i)%str
      mat(i+2,3) = self%physUnits (i)%str
      if ( len_trim(self%physUnits0(i)%str) > 0 ) then
         mat(i+2,4) = '('//self%physUnits0(i)%str//')'
      else
         mat(i+2,4) = ''
      end if
      mat(i+2,5) = util_intToChar(self%varNcomp(i))
      i2 = i2 + self%varNcomp(i) * max(1_Ikind,self%nent)           
      mat(i+2,6) = util_intToChar(i2)
   end do
   
   write(*,'(/,a)') &
        'Set of variables "' // trim(adjustl(self%name)) // '":'
   write(*,'(a)'  ) line       
   call disp (mat, sep = ' | ')
   write(*,'(a,/)') line      

   END SUBROUTINE femval_displayVar   


!=============================================================================================
   SUBROUTINE femval_displayVark1 ( a, all, title )
!=============================================================================================
   class    (femval_t),           intent(in) :: a(:)
   logical            , optional, intent(in) :: all
   character(len=*   ), optional, intent(in) :: title
!--------------------------------------------------------------------------------------------- 
!  Prints to the sdtout the names of the variables contained in the array "a" of femval type
!--------------------------------------------------------------------------------------------- 

!- local variables ---------------------------------------------------------------------------
   integer         , parameter   :: ncell = 7
   character(len=:), allocatable :: mat(:,:), line
   type     (str_t)              :: cellNames(ncell)
   integer                       :: i, j, n, lenmax = 0, l(ncell) = 0
   integer  ( i64 )              :: i2
   logical                       :: dispall
!--------------------------------------------------------------------------------------------- 

   if ( present(all) ) then
      dispall = all
   else
      dispall = .false.
   end if
   
   cellNames(1) = 'Name'        ; cellNames(2) = 'Quantity' ; cellNames(3) = 'Unit'
   cellNames(4) = '(Original)'  ; cellNames(5) = '# Comp.'  ; cellNames(6) = 'Object, Index'
   cellNames(7) = 'Attached to'
   
   do i = 1, ncell
      l(i) = len(cellNames(i)%str)
   end do
   
   n = 0
   do j = 1, size(a)
      if ( .not. a(j)%is_active ) cycle
      if ( .not. dispall .and. .not. a(j)%is_new ) cycle
      i2 = 0
      do i = 1, a(j)%nvar   
         l(1) = max(l(1),len_trim(a(j)%varNames  (i)%str))
         l(2) = max(l(2),len_trim(a(j)%physNames (i)%str))
         l(3) = max(l(3),len_trim(a(j)%physUnits (i)%str))
         if ( len_trim(a(j)%physUnits0(i)%str) > 0 ) &
            l(4) = max(l(4),len_trim(a(j)%physUnits0(i)%str)+2)
         l(5) = max(l(5),len_trim(util_intToChar(a(j)%varNcomp(i))))
         i2 = i2 + a(j)%varNcomp(i) * a(j)%nent      
         l(6) = max(l(6),len_trim(util_intToChar(j))  + 2 + &
                         len_trim(util_intToChar(i2))       )
         l(7) = max(l(7),len_trim(a(j)%geomEntities))
      end do  
      n = n + a(j)%nvar  
   end do

   if ( n == 0 ) return
   
   lenmax = maxval(l)
   
   allocate(character(len=lenmax) :: mat(n+2,ncell))
   
   line = ''
   do i = 1, ncell
      mat(1,i) = cellNames(i)%str
      mat(2,i) = repeat('=',l(i))
      if (i < ncell) line = line // trim(mat(2,i)) // '==='
   end do
   line = line // trim(mat(2,ncell))
   
   n = 0
   do j = 1, size(a)  
      if ( .not. a(j)%is_active ) cycle
      if ( .not. dispall .and. .not. a(j)%is_new ) cycle
      i2 = 0
      do i = 1, a(j)%nvar   
         n = n + 1 
         mat(n+2,1) = a(j)%varNames  (i)%str
         mat(n+2,2) = a(j)%physNames (i)%str
         mat(n+2,3) = a(j)%physUnits (i)%str
         if ( len_trim(a(j)%physUnits0(i)%str) > 0 ) then
            mat(n+2,4) = '('//a(j)%physUnits0(i)%str//')'
         else
            mat(n+2,4) = ''
         end if
         mat(n+2,5) = util_intToChar(a(j)%varNcomp(i))
         i2 = i2 + a(j)%varNcomp(i) * max(1_Ikind,a(j)%nent)           
         mat(n+2,6) = util_intToChar(j) // ', ' // &
                      util_intToChar(i2)
         mat(n+2,7) = a(j)%geomEntities
      end do
   end do
   
   if ( present(title) ) then
      write(*,'(/,a)') title
   else
      write(*,'(/,a)') 'Set of variables:'
   end if
   write(*,'(a)'  ) line       
   call disp (mat, sep = ' | ')
   write(*,'(a,/)') line      

   END SUBROUTINE femval_displayVark1      
                                
END MODULE femval_m