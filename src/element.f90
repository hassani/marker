#include "error.fpp"

MODULE element_m

    use pk2mod_m, only: Ikind, Rkind, err_t, util_StringLow, util_intToChar, UERROR
    
!=============================================================================================
!
!     code CAMEF (Code d'Apprentissage de la Methode des Elements Finis)
!                 
!     MASTER 2 Ingenierie Mathematique   
!
!     INITIATION A LA METHODE DES ELEMENTS FINIS
!
!     voir le polycopie du cours "MISE EN OEUVRE DE LA METHODE DES ELEMENTS FINIS"
!                                    
!                                   Riad HASSANI Chambery, 09/2000
!                                   version revisitee (f90)  Nice, 04/2016
!
!---------------------------------------------------------------------------------------------
!
!     Fichier element.f90
!
!     petite librairie de routines pour calcul des fonctions d'interpolation E.F.
!     
!     Adaptation faite a partir des sources suivantes :
!
!      - Dhatt & Touzot (une presentation de la methode des elements finis)
!      - Hinton & Owen (Finite Element in Plasticity)
!      - Cheung (A Pract. Introd. to F.E.-Analysis)
!      - code Geodyn (J.P. Vilotte)
!
!
!     Chaque routine (element_XXXXXX) est relative a un element de reference.
!     Tous les arguments de ces routines sont optionnels.
!
!     L'appel a une de ces routines peut prendre l'une des formes :
!
!     - call element_XXXXXX (info=nnd)
!
!          pour interoger la routine sur le nbr de noeuds de l'element
!          et sa dimension d'espace (1, 2 ou 3)
!
!     - call element_XXXXXX (pnt=cdloc,shp=shape,drv=deriv)
!
!          utilisation normale
!
!     - call element_XXXXXX (pnt=cdloc,shp=shape,drv=deriv,cnd=cdnod)
!
!          pour connaitre en plus les coordonnees des noeuds
!
!     Une routine pilote (element_driver) permet l'appel a la routine
!     desiree. Elle necessite le nom de l'element (XXXXXX) de reference
!     et le couple (nnode,ndime) servant a dimensionner les tableaux.
!
!     La routine element_jacob calcule le jacobien et les derivees cartesiennes
!---------------------------------------------------------------------------------------------

    contains


!=============================================================================================
    SUBROUTINE element_driver (namelem, ndime, nnode, pnt, shp, drv, cnd, nnd)
!=============================================================================================
    integer   (Ikind), intent(in    )           :: ndime, nnode
    character (len=*), intent(in    )           :: namelem
    integer   (Ikind), intent(   out), optional :: nnd(2) 
    real      (Rkind), intent(in    ), optional :: pnt(ndime)
    real      (Rkind), intent(   out), optional :: shp(nnode)
    real      (Rkind), intent(   out), optional :: drv(ndime,nnode)
    real      (Rkind), intent(   out), optional :: cnd(ndime,nnode)
!---------------------------------------------------------------------------------------------    
!
!   Routine qui pilote le calcul des fonctions d'interpolation (shp) et de 
!   leurs derivees (drv) sur l'element de reference en un point donne par
!   ses coordonnees locales (pnt)
!
!   En entree : namelem ....................... nom de l'element
!
!             : ndime ......................... dimension de l'espace geometrique
!                                                    
!             : nnode ......................... nbr de noeuds
!
!             : pnt(1:ndime) .................. coordonnee(s) du point
!
!   En sortie : shp(1:nnode) .................. valeurs des fonctions d'interpolation
!                                                    
!             : drv(1:ndime,1:nnode) .......... valeurs de leurs derivees
!                                                    
!             : cnd(1:ndime,1:nnode) .......... coordonnees locales des noeuds
!                                                    
!             : nnd ........................... nbr de noeuds (nnode) et dimensions (ndime)
!                                                    
!   Les arguments pnt, shp, drv, cnd et nnd sont optionnels
!
!   Les appels possibles a cette routine sont, par exemple :
!
!   - call element_driver (namelem,ndime,nnode,pnt=coord,shp=shape,drv=deriv)
!
!     (utilisation normale)
!
!   - call element_driver (namelem,ndime,nnode,pnt=coord,shp=shape,drv=deriv, cnd=cdnod)
!
!     (si besoin des coordonnees des noeuds de l'element de reference)
!
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    integer   (Ikind)                           :: tst(2)
    character (len=20)                          :: namecap !, string_low
!---------------------------------------------------------------------------------------------
     
    namecap = util_StringLow ( adjustl(namelem) )
!
!-- on teste d'abord la conformite du couple (ndime,nnode) avec
!   l'element demande :

    select case (namecap)
      case ("segm02")
         call element_segm02 (info=tst)
      case ("segm03")
         call element_segm03 (info=tst)
      case ("tria03")
         call element_tria03 (info=tst)
      case ("tria04")
         call element_tria04 (info=tst)
      case ("tria06")
         call element_tria06 (info=tst)
      case ("tria07")
         call element_tria07 (info=tst)
      case ("quad04")
         call element_quad04 (info=tst)
      case ("quad08")
         call element_quad08 (info=tst)
      case ("quad09")
         call element_quad09 (info=tst)
      case ("tetr04")
         call element_tetr04 (info=tst)
      case ("tetr10")
         call element_tetr10 (info=tst)
      case ("hexa08")
         call element_hexa08 (info=tst)
      case default
         write(*,1)namecap,nnode,ndime
         stop
    end select
    
    if (present(nnd)) nnd = tst
    
    if (.not. present(pnt) .and. .not. present(cnd)) return ! rien d'autre a faire
    
    if (tst(1) /= nnode .or. tst(2) /= ndime) then
       write(*,2)namecap,tst(1),tst(2),nnode,ndime
       stop
    end if

!
!-- calcul effectif des fonctions d'interpolation :
!

    select case (namecap)
      case ("segm02")
         call element_segm02 (pnt,shp,drv,cnd,nnd)
      case ("segm03")
         call element_segm03 (pnt,shp,drv,cnd,nnd)
      case ("tria03")
         call element_tria03 (pnt,shp,drv,cnd,nnd)
      case ("tria04")
         call element_tria04 (pnt,shp,drv,cnd,nnd)
      case ("tria06")
         call element_tria06 (pnt,shp,drv,cnd,nnd)
      case ("tria07")
         call element_tria07 (pnt,shp,drv,cnd,nnd)
      case ("quad04")
         call element_quad04 (pnt,shp,drv,cnd,nnd)
      case ("quad08")
         call element_quad08 (pnt,shp,drv,cnd,nnd)
      case ("quad09")
         call element_quad09 (pnt,shp,drv,cnd,nnd)
      case ("tetr04")
         call element_tetr04 (pnt,shp,drv,cnd,nnd)
      case ("tetr10")
         call element_tetr10 (pnt,shp,drv,cnd,nnd)
      case ("hexa08")
         call element_hexa08 (pnt,shp,drv,cnd,nnd)
      case default
         write(*,1)namecap,nnode,ndime
         stop
    end select

    1 format(/,"Erreur (element_driver) : ",/,"--> nom d'element inconnu",/           &
         "--> nom : ",a6,/,"--> nbr de noeuds :",i5,/,"--> nbr de dimensions :",i5,   &
         /,"--> STOP",/)
         
    2 format("Erreur (element_driver) :",/, "--> element demande : ",a6,/,             &
         "--> nbr de noeud :",i3,/,"--> nbr de dim. :",i3,/,"--> non conforme avec ",  &
         "la valeur de (nnode,ndime) :",/,"--> nnode =",i5,/,"--> ndime =",i5,/,       &
         "--> STOP",/)

    end subroutine element_driver


!=============================================================================================
    SUBROUTINE element_Segm02 (pnt, shp, drv, cnd, info)
!=============================================================================================
    integer(Ikind), dimension(2), intent(   out), optional :: info
    real   (Rkind), dimension(1), intent(in    ), optional :: pnt
    real   (Rkind), dimension(2), intent(   out), optional :: shp, drv
    real   (Rkind), dimension(2), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 segment a 2 noeuds de type Lagrange
!
!     au point de coordonnee locale x (-1 =< x =< 1)
!
!     En entree : pnt ............... coordonnee du point
!     En sortie : shp (1:2) ......... valeurs des fonctions d'interpolation
!               : drv (1:2) ......... valeurs de leurs derivees
!               : cnd (1:2) ......... coordonnees locales des noeuds
!               : info(1:2) ......... nbr de noeuds (2) et dimensions (1)
!
!     Tous les arguments sont optionnels.
!
!
!     Numerotation locale :
!
!            1                   2
!            *-------------------*
!           -1                  +1
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: dm = 0.5_Rkind, u = 1.0_Rkind
    real(Rkind)            :: x
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then
       x = pnt(1)
       if (present(shp)) shp = dm * (/ u - x , u + x /)
       if (present(drv)) drv = dm * (/    -u ,     u /)
    end if

    if (present(cnd)) cnd = (/-u , u /)

    if (present(info)) info = (/ 2, 1 /)

    end subroutine element_Segm02


!=============================================================================================
    SUBROUTINE element_Segm03 (pnt, shp, drv, cnd, info)
!=============================================================================================
    integer(Ikind), dimension(2), intent(   out), optional :: info
    real   (Rkind), dimension(1), intent(in    ), optional :: pnt
    real   (Rkind), dimension(3), intent(   out), optional :: shp, drv
    real   (Rkind), dimension(3), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 ligne a 3 noeuds de type Lagrange
!
!     au point de coordonnee locale x (-1 =< x =< 1)
!
!     En entree : pnt ............... coordonnee du point
!     En sortie : shp (1:2) ......... valeurs des fonctions d'interpolation
!               : drv (1:2) ......... valeurs de leurs derivees
!               : cnd (1:2) ......... coordonnees locales des noeuds
!               : info(1:2) ......... nbr de noeuds (2) et dimensions (1)
!
!     Numerotation locale :
!
!            1         3         2
!            *-------- * --------*
!           -1         0        +1
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: dm = 0.5_Rkind, u = 1.0_Rkind, d = 2.0_Rkind
    real(Rkind)            :: x
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then
       x = pnt(1)
       if (present(shp)) shp = (/ dm*(x*x - x) , dm*(x*x + x) , u - x*x /)
       if (present(drv)) drv = (/ x - dm       , x + dm       ,-d*x     /)
    end if

    if (present(cnd)) cnd = (/-u , u , 0.0_Rkind /)

    if (present(info)) info = (/ 3, 1 /)

    end subroutine element_Segm03


!=============================================================================================
    SUBROUTINE element_Tria03 (pnt, shp, drv, cnd, info)
!=============================================================================================
    integer(Ikind), dimension(  2), intent(   out), optional :: info
    real   (Rkind), dimension(  2), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  3), intent(in out), optional :: shp
    real   (Rkind), dimension(2,3), intent(in out), optional :: drv
    real   (Rkind), dimension(2,3), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------    
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 triangle a 3 noeuds de type Lagrange
!
!     au point de coordonnee locale x,y (0 =< x =< 1, 0 =< y =< 1-x)
!
!     En entree : pnt ............... coordonnee du point
!     En sortie : shp (1:3) ......... valeurs des fonctions d'interpolation
!               : drv (1:2,1:3) ..... valeurs de leurs derivees
!               : cnd (1:2,1:3) ..... coordonnees locales des noeuds
!               : info(1:2) ......... nbr de noeuds (2) et dimensions (1)
!
!     Numerotation locale :
!
!                3 (0,1)
!                . .
!                .   .
!                .     .
!                .       .              y
!                .         .            .
!                1 . . . . . 2          .. x
!              (0,0)       (1,0)
!---------------------------------------------------------------------------------------------
 
!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: zr = 0.0_Rkind, u = 1.0_Rkind
    real(Rkind)            :: x, y
!---------------------------------------------------------------------------------------------
    if (present(pnt)) then
       x = pnt(1); y = pnt(2)
    
       if (present(shp)) then
          shp      = (/ u - x - y , x , y  /)
       end if  
        
       if (present(drv)) then 
          drv(1,:) = (/   -u      , u , zr /)
          drv(2,:) = (/       -u  , zr, u  /)
       end if                        
    
    end if

    if (present(cnd)) then
       cnd(1,:) = (/ zr , u  , zr /);
       cnd(2,:) = (/ zr , zr , u  /)
    end if

    if (present(info)) info = (/ 3, 2 /)

    end subroutine element_Tria03


!=============================================================================================
    SUBROUTINE element_Tria04 (pnt, shp, drv, cnd, info)
!=============================================================================================
    integer(Ikind), dimension(  2), intent(   out), optional :: info
    real   (Rkind), dimension(  2), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  4), intent(   out), optional :: shp
    real   (Rkind), dimension(2,4), intent(   out), optional :: drv
    real   (Rkind), dimension(2,4), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 triangle a 4 noeuds de type Lagrange
!
!     au point de coordonnee locale x,y (0 =< x =< 1, 0 =< y =< 1-x)
!
!     En entree : pnt ...................... coordonnees du point
!     En sortie : shp(1:4) ................. valeurs des fonctions d'interpolation
!               : drv(1:2,1:4) ............. valeurs de leurs derivees
!               : cnd(1:2,1:4) (optionnel) . coordonnees locales des noeuds
!               : nnd (optionnel) .......... nbr de noeuds (4)
!
!     Numerotation locale : sommet-sommet-sommet - centre
!
!                3 (0,1)
!                . .
!                .   .
!                .     .
!                .   4 (1/3,1/3)        y
!                .         .            .
!                1 . . . . . 2          .. x
!              (0,0)       (1,0)
!
!
!       les fonctions de forme  sont :
!
!             L_i -  9 * bulle    i = 1,2,3
!                   27 * bulle    i = 4
!
!       ou les L_i sont les fonctions de forme de l'element T3 et bulle = L_1 * L_2 * L_3
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: zr=0.0_Rkind, u=1.0_Rkind, t=3.0_Rkind, n=9.0_Rkind    
    real(Rkind)            :: b9, bx9, by9
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then
!
!--    fonctions de forme du T3
!
       call element_Tria03 (pnt,shp,drv)
!
!--    fonction bulle (*9) et son gradient (*9)
!
       b9  = n * shp(1) * shp(2) * shp(3)
       bx9 = n * shp(3) * (shp(1) - shp(2))
       by9 = n * shp(2) * (shp(1) - shp(3))
!
!--    fonctions de forme et leurs derives
!
       if (present(shp)) then
          shp(  1)= shp(1)-b9; shp(  2)= shp(2)-b9; shp(  3)= shp(3)-b9; shp(  4)= t*b9
       end if
       
       if (present(drv)) then  
          drv(1,1)=-u-bx9    ; drv(1,2)= u-bx9    ; drv(1,3)=-bx9      ; drv(1,4)= t*bx9
          drv(2,1)=-u-by9    ; drv(2,2)=  -by9    ; drv(2,3)= u-by9    ; drv(2,4)= t*by9
       end if
    
    end if

    if (present(cnd)) then
       cnd(1,:) = (/ zr , u  , zr , u/t /)
       cnd(2,:) = (/ zr , zr , u  , u/t /)
    end if

    if (present(info)) info = (/ 4, 2 /)

    end subroutine element_Tria04
!

!=============================================================================================
    SUBROUTINE element_Tria3H (pnt, shp, drv, cnd, info)
!=============================================================================================
    integer(Ikind), dimension(  2), intent(   out), optional :: info
    real   (Rkind), dimension(  2), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  3), intent(   out), optional :: shp
    real   (Rkind), dimension(2,3), intent(   out), optional :: drv
    real   (Rkind), dimension(2,3), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------    
! !! NON TESTEE !!
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 triangle a 3 noeuds de type Hermite
!
!     au point de coordonnee locale x,y (0 =< x =< 1, 0 =< y =< 1-x)
!
!      l'interpolation d'une fonction f est de la forme
!
!          f(x) = shp(1)*f(xc) + shp(2)*dfx(xc) + shp(3)*dfy(xc)
!
!      ou xc sont les coordonnees du centre de l'element et
!      dfx et dfy sont les derivees partielles premiere de f
!
!     En entree : pnt ...................... coordonnees du point
!     En sortie : shp(1:3) ................. valeurs des fonctions d'interpolation
!               : drv(1:2,1:3) ............. valeurs de leurs derivees
!               : cnd(1:2,1:3) (optionnel) . coordonnees locales des noeuds
!               : nnd (optionnel) .......... nbr de noeuds (3)
!
!
!     En entree : x,y
!     En sortie : shp(1:3), drv(1:2,1:3)
!
!     Numerotation locale :
!
!                3 (0,1)
!                . .
!                .   .
!                .     .
!                .       .              y
!                .         .            .
!                1 . . . . . 2          .. x
!              (0,0)       (1,0)
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: zr=0.0_Rkind, u=1.0_Rkind, ts=1.0_Rkind/3.0_Rkind
    real(Rkind)            :: x, y
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then
       x = pnt(1); y = pnt(2)

       if (present(shp)) then
          shp(  1) = u  ; shp(  2) = x - ts ; shp(  3) = y - ts
       end if
       if (present(drv)) then  
          drv(1,1) = zr ; drv(1,2) = u      ; drv(1,3) = zr
          drv(2,1) = zr ; drv(2,2) = zr     ; drv(2,3) = u
       end if   
    end if

    if (present(cnd)) then
       cnd(1,:) = (/ zr , u  , zr /)
       cnd(2,:) = (/ zr , zr , u  /)
    end if

    if (present(info)) info = (/ 3, 2 /)

    end subroutine element_Tria3H


!=============================================================================================
    SUBROUTINE element_Tria06 (pnt, shp, drv, cnd, info)
!=============================================================================================    
    integer(Ikind), dimension(  2), intent(   out), optional :: info
    real   (Rkind), dimension(  2), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  6), intent(   out), optional :: shp
    real   (Rkind), dimension(2,6), intent(   out), optional :: drv
    real   (Rkind), dimension(2,6), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------
!   
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 triangle a 6 noeuds de type Lagrange (P2)
!
!     au point de coordonnee locale x,y (0 =< x =< 1, 0 =< y =< 1-x)
!
!
!     En entree : pnt ...................... coordonnees du point
!     En sortie : shp(1:6) ................. valeurs des fonctions d'interpolation
!               : drv(1:2,1:6) ............. valeurs de leurs derivees
!               : cnd(1:2,1:6) (optionnel) . coordonnees locales des noeuds
!               : nnd (optionnel) .......... nbr de noeuds (6)
!
!     Numerotation locale : sommet-sommet-sommet - milieu-milieu-milieu
!
!                3 (0,1)
!                . .
!                .   .
!                6     5
!                .       .               y
!                .         .             .
!                1 . . 4. . . 2          .. x
!              (0,0)        (1,0)
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: zr=0.0_Rkind, dm=0.5_Rkind, u=1.0_Rkind, d=2.0_Rkind, q=4.0_Rkind
    real(Rkind)            :: L1, x, y
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then
       x = pnt(1); y = pnt(2); L1 = u - x - y;

       if (present(shp)) then
          shp(  1:3) = (/ L1*(d*L1 - u), x*(d*x - u), y*(d*y -u) /)
          shp(  4:6) = (/ q*x*L1       , q*x*y      , q*y*L1     /)
       end if
       
       if (present(drv)) then       
          drv(1,1:3) = (/ u - q*L1     , q*x -u     , zr         /)
          drv(2,1:3) = (/ u - q*L1     , zr         , q*y - u    /)
          drv(1,4:6) = (/ q*(L1 - x)   , q*y        ,-q*y        /)
          drv(2,4:6) = (/-q*x          , q*x        , q*(L1 - y) /)
       end if
          
    end if

    if (present(cnd)) then
      cnd(1,:) = (/ zr , u  , zr ,dm , dm ,zr  /)
      cnd(2,:) = (/ zr , zr , u  ,zr , dm ,dm  /)
    end if

    if (present(info)) info = (/ 6, 2 /)

    end subroutine element_Tria06


!=============================================================================================
    SUBROUTINE element_Tria07 (pnt, shp, drv, cnd, info)
!=============================================================================================
    integer(Ikind), dimension(  2), intent(   out), optional :: info
    real   (Rkind), dimension(  2), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  7), intent(   out), optional :: shp
    real   (Rkind), dimension(2,7), intent(   out), optional :: drv
    real   (Rkind), dimension(2,7), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 triangle a 7 noeuds de type Lagrange (P2-bulle)
!
!     au point de coordonnee locale x,y (0 =< x =< 1, 0 =< y =< 1-x)
!
!     En entree : pnt ...................... coordonnees du point
!     En sortie : shp(1:7) ................. valeurs des fonctions d'interpolation
!               : drv(1:2,1:7) ............. valeurs de leurs derivees
!               : cnd(1:2,1:7) (optionnel) . coordonnees locales des noeuds
!               : nnd (optionnel) .......... nbr de noeuds (7)
!
!     Numerotation locale : sommet-sommet-sommet - milieu-milieu-milieu - centre
!
!                3 (0,1)
!                . .
!                .   .
!                6     5
!                .   7   .               y
!                .         .             .
!                1 . . 4. . . 2          .. x
!              (0,0)        (1,0)
!
!       les fonctions de forme sont :
!
!             N_i +  3 * bulle    i = 1,2,3
!             N_i - 12 * bulle    i = 4,5,6
!                   27 * bulle    i = 7
!
!       ou les N_i sont les fonctions de forme de l'element T6
!       et bulle est la fonction bullle = L_1*L_2*L_3 (les L_i etant
!       les fonctions de forme du T3, ie les coordonnees barycentriques)
!       donc bulle = (1 - x - y) * x * y
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: v0=0.0_Rkind, v05= 0.5_Rkind, v1 =1.0_Rkind, v2=2.0_Rkind, &
                              v3=3.0_Rkind, v12=12.0_Rkind, v27=27.0_Rkind
    real(Rkind) :: N(6), dN(2,6)
    real(Rkind) :: x, y, b, bx, by
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then
       x = pnt(1); y = pnt(2)

       b = (v1 - x - y)*x*y; bx = (v1 - v2*x - y)*y; by = (v1 - v2*y - x)*x

       call element_Tria06 (pnt,N,dN)      ! fonctions d'interpolation du T6
       
       if (present(shp)) then
          shp(  1:3) = N(  1:3) + v3*b     ! aux trois noeuds sommets
          shp(  4:6) = N(  4:6) - v12*b    ! aux trois noeuds milieux
          shp(    7) = v27*b               ! au noeud central
       end if
       
       if (present(drv)) then        
          drv(1,1:3) = dN(1,1:3) + v3*bx  ! aux trois noeuds sommets
          drv(2,1:3) = dN(2,1:3) + v3*by

          drv(1,4:6) = dN(1,4:6) - v12*bx ! aux trois noeuds milieux
          drv(2,4:6) = dN(2,4:6) - v12*by
          
          drv(1,  7) = v27*bx             !   au noeud central
          drv(2,  7) = v27*by
       end if   
       
    end if
    

    if (present(cnd)) then
       cnd(1,:) = (/ v0 , v1 , v0 , v05 , v05 ,v0  , v1/v3 /)
       cnd(2,:) = (/ v0 , v0 , v1 , v0  , v05 ,v05 , v1/v3 /)
    end if

    if (present(info)) info = (/ 7, 2 /)

    end subroutine element_Tria07


!=============================================================================================    
    SUBROUTINE element_Quad04 (pnt, shp, drv, cnd, info)
!=============================================================================================        
    integer(Ikind), dimension(  2), intent(   out), optional :: info
    real   (Rkind), dimension(  2), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  4), intent(   out), optional :: shp
    real   (Rkind), dimension(2,4), intent(   out), optional :: drv
    real   (Rkind), dimension(2,4), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 quadrangle a 4 noeuds de type Lagrange (Q1)
!
!     au point de coordonnee locale x,y (-1 =< x =< 1, -1 =< y =< 1)
!
!     En entree : x,y ...................... coordonnees du point
!     En sortie : shp(1:4) ................. valeurs des fonctions d'interpolation
!               : drv(1:2,1:4) ............. valeurs de leurs derivees
!               : cnd(1:2,1:4) (optionnel) . coordonnees locales des noeuds
!               : nnd (optionnel) .......... nbr de noeuds (4)
!
!     Numerotation locale :
!
!                         4------------3
!                         |      y     |
!                         |      .     |
!                         |      . .x  |
!                         |            |
!                         |            |
!                         1------------2
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: v1=1.0_Rkind, v025=0.25_Rkind
    real(Rkind)            :: xm, xp, ym, yp, x, y
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then
       x = pnt(1); y = pnt(2)

       xm = v1 - x ; xp = v1 + x ; ym = v1 - y ; yp = v1 + y

       if (present(shp)) shp = v025*(/xm*ym, xp*ym, xp*yp, xm*yp/)

       if (present(drv)) then
          drv(1,:) = v025*(/ -ym , ym , yp ,-yp /)
          drv(2,:) = v025*(/ -xm ,-xp , xp , xm /)
       end if
          
    end if

    if (present(cnd)) then
       cnd(1,:)= (/ -v1 , v1 , v1 ,-v1 /)
       cnd(2,:)= (/ -v1 ,-v1 , v1 , v1 /);
    end if

    if (present(info)) info = (/ 4, 2 /)

    end subroutine element_Quad04


!=============================================================================================    
    SUBROUTINE element_Quad08 (pnt, shp, drv, cnd, info)
!=============================================================================================        
    integer(Ikind), dimension(  2), intent(   out), optional :: info
    real   (Rkind), dimension(  2), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  8), intent(   out), optional :: shp
    real   (Rkind), dimension(2,8), intent(   out), optional :: drv
    real   (Rkind), dimension(2,8), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 quadrangle a 8 noeuds de type Lagrange (Q2-s)
!
!     au point de coordonnee locale x,y (-1 =< x =< 1, -1 =< y =< 1)
!
!     En entree : pnt ...................... coordonnees du point
!     En sortie : shp(1:8) ................. valeurs des fonctions d'interpolation
!               : drv(1:2,1:8) ............. valeurs de leurs derivees
!               : cnd(1:2,1:8) (optionnel) . coordonnees locales des noeuds
!               : nnd (optionnel) .......... nbr de noeuds (8)
!
!     Numerotation locale : sommet-sommet-sommet-sommet -
!                           milieu-milieu-milieu-milieu
!
!                         4------7-----3
!                         |      y     |
!                         |      .     |
!                         8      . .x  6
!                         |            |
!                         |            |
!                         1------5-----2
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: v0=0.0_Rkind, v1=1.0_Rkind, v2=2.0_Rkind
    real(Rkind)            :: N(4), c1(4), c2(4), dN(2,4)
    real(Rkind)            :: x, y
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then
       x = pnt(1); y = pnt(2)

       call element_Quad04 (pnt,N,dN) ! fonctions d'interpolation du Q4

       c1 = (/ v1 + x + y , v1 - x + y , v1 - x - y , v1 + x -y /)
       c2 = (/ v1 + x     , v1 + y     , v1 - x     , v1 - y    /)

       if (present(shp)) then       
          shp(  1:4) =-N * c1       ! aux quatre noeuds sommets
          shp(  5:8) = N * c2 * v2  ! aux quatre noeuds milieux
       end if
       
       if (present(drv)) then
          drv(1,1:4) =  -dN(1,:) * c1 - N * (/ v1,-v1,-v1, v1 /)
          drv(2,1:4) =  -dN(2,:) * c1 - N * (/ v1, v1,-v1,-v1 /)
          drv(1,5:8) = ( dN(1,:) * c2 + N * (/ v1, v0,-v1, v0 /) ) * v2
          drv(2,5:8) = ( dN(2,:) * c2 + N * (/ v0, v1, v0,-v1 /) ) * v2
       end if   
       
    end if

    if (present(cnd)) then
       cnd(1,:)= (/ -v1 , v1 , v1 ,-v1 , v0 , v1 , v0 ,-v1 /)
       cnd(2,:)= (/ -v1 ,-v1 , v1 , v1 ,-v1 , v0 , v1 , v0 /)
    end if

    if (present(info)) info = (/ 8, 2 /)

    end subroutine element_Quad08


!=============================================================================================
    SUBROUTINE element_Quad09 (pnt, shp, drv, cnd, info)
!=============================================================================================    
    integer(Ikind), dimension(  2), intent(   out), optional :: info
    real   (Rkind), dimension(  2), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  9), intent(   out), optional :: shp
    real   (Rkind), dimension(2,9), intent(   out), optional :: drv
    real   (Rkind), dimension(2,9), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 quadrangle a 9 noeuds de type Lagrange (Q2)
!
!     au point de coordonnee locale x,y (-1 =< x =< 1, -1 =< y =< 1)
!
!     En entree : pnt ...................... coordonnees du point
!     En sortie : shp(1:9) ................. valeurs des fonctions d'interpolation
!               : drv(1:2,1:9) ............. valeurs de leurs derivees
!               : cnd(1:2,1:9) (optionnel) . coordonnees locales des noeuds
!               : nnd (optionnel) .......... nbr de noeuds (9)
!
!     Numerotation locale : sommet-sommet-sommet-sommet -
!                           milieu-milieu-milieu-milieu - centre
!
!                         4------7-----3
!                         |      y     |
!                         |      .     |
!                         8      9 .x  6
!                         |            |
!                         |            |
!                         1------5-----2
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: v0=0.0_Rkind, v1=1.0_Rkind
    real(Rkind)            :: Lx(3), Ly(3), dLx(3), dLy(3)
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then
!
!--    fonctions d'interpolation du S3 :
!
       call element_Segm03 (pnt(1),Lx,dLx); call element_Segm03 (pnt(2),Ly,dLy) 

       if (present(shp)) then
          shp(1:4) = (/ Lx(1)* Ly(1), Lx(2)* Ly(1), Lx(2)* Ly(2), Lx(1)* Ly(2) /) ! aux noeuds sommets
          
          shp(5:8) = (/ Lx(3)* Ly(1), Lx(2)* Ly(3), Lx(3)* Ly(2), Lx(1)* Ly(3) /) ! aux noeuds milieux
          
          shp(  9) =    Lx(3)* Ly(3)                                              ! au noeud central
       end if
       
       if (present(drv)) then   
          drv(1,1:4) = (/dLx(1)* Ly(1),dLx(2)* Ly(1),dLx(2)* Ly(2),dLx(1)* Ly(2) /)
          drv(2,1:4) = (/ Lx(1)*dLy(1), Lx(2)*dLy(1), Lx(2)*dLy(2), Lx(1)*dLy(2) /)
          
          drv(1,5:8) = (/dLx(3)* Ly(1),dLx(2)* Ly(3),dLx(3)* Ly(2),dLx(1)* Ly(3) /)
          drv(2,5:8) = (/ Lx(3)*dLy(1), Lx(2)*dLy(3), Lx(3)*dLy(2), Lx(1)*dLy(3) /)
          
          drv(1,9) = dLx(3)* Ly(3)
          drv(2,9) =  Lx(3)*dLy(3)
       end if   
    end if

    if (present(cnd)) then
       cnd(1,:)= (/ -v1 , v1 , v1 ,-v1 , v0 , v1 , v0 ,-v1 , v0 /)
       cnd(2,:)= (/ -v1 ,-v1 , v1 , v1 ,-v1 , v0 , v1 , v0 , v0 /);
    end if

    if (present(info)) info = (/ 9, 2 /)

    end subroutine element_Quad09


!=============================================================================================
    SUBROUTINE element_Tetr04 (pnt, shp, drv, cnd, info)
!=============================================================================================
    real   (Rkind), dimension(  3), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  4), intent(   out), optional :: shp
    real   (Rkind), dimension(3,4), intent(   out), optional :: drv
    real   (Rkind), dimension(3,4), intent(   out), optional :: cnd
    integer(Ikind), dimension(  2), intent(   out), optional :: info
!---------------------------------------------------------------------------------------------
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 tetraedre a 4 noeuds de type Lagrange (P1)
!
!     au point de coordonnee locale x,y,z (0=<x=<1, 0=<y=<1, 0=<z<=1-x-y)
!
!     En entree : pnt ...................... coordonnees du point
!     En sortie : shp(1:3) ................. valeurs des fonctions d'interpolation
!               : drv(1:3,1:4) ............. valeurs de leurs derivees
!               : cnd(1:3,1:4) (optionnel) . coordonnees locales des noeuds
!               : nnd (optionnel) .......... nbr de noeuds (4)
!
!      numerotation : (sommet)_i=1,4 - (milieu)_i=1,6  comme suit :
!
!                      4 (0,0,1)
!                      |
!                      |
!                      |
!                      |
!                      |____________ 3 (0,1,0)
!                     / 1 (0,0,0)
!                    /
!                   /
!                  /
!                 /
!                2 (1,0,0)
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: v0=0.0_Rkind, v1=1.0_Rkind
    real(Rkind)            :: x, y, z
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then

       x = pnt(1); y = pnt(2); z = pnt(3)

       if (present(shp)) shp = (/ v1  -x  -y  -z  ,  x  ,  y  ,   z /)
       
       if (present(drv)) then
          drv(1,:) = (/    -v1          , v1  ,  v0 ,  v0 /)
          drv(2,:) = (/        -v1      , v0  ,  v1 ,  v0 /)
          drv(3,:) = (/            -v1  , v0  ,  v0 ,  v1 /)
       end if   
       
    end if

    if (present(cnd)) then
       cnd(1,:) = (/ v0 , v1 , v0 , v0 /)
       cnd(2,:) = (/ v0 , v0 , v1 , v0 /)
       cnd(3,:) = (/ v0 , v0 , v0 , v1 /)
    end if

    if (present(info)) info = (/ 4, 3 /)

    end subroutine element_Tetr04


!=============================================================================================
    SUBROUTINE element_Tetr10 (pnt, shp, drv, cnd, info)
!=============================================================================================    
    integer(Ikind), dimension(   2), intent(   out), optional :: info
    real   (Rkind), dimension(   3), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  10), intent(   out), optional :: shp
    real   (Rkind), dimension(3,10), intent(   out), optional :: drv
    real   (Rkind), dimension(3,10), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 tetraedre a 10 noeuds de type Lagrange (P2)
!
!     au point de coordonnee locale x,y,z (0=<x=<1, 0=<y=<1, 0=<z<=1-x-y)
!
!     En entree : pnt ....................... coordonnees du point
!     En sortie : shp(1:3) .................. valeurs des fonctions d'interpolation
!               : drv(1:3,1:10) ............. valeurs de leurs derivees
!               : cnd(1:3,1:10) (optionnel) . coordonnees locales des noeuds
!               : nnd (optionnel) ........... nbr de noeuds (10)
!
!      numerotation : (sommet)_i=1,4 - (milieu)_i=1,6  comme suit :
!
!                      4
!                      |
!                      |
!                      8     10
!                      |
!                  9   |____7______
!                     /1           3
!                    /
!                   5
!                  /     6
!                 /
!                2
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: v0=0.0_Rkind, v05=0.5_Rkind, v1=1.0_Rkind, &
                              v2=2.0_Rkind, v4=4.0_Rkind
    real(Rkind)            :: L1,x,y,z
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then

       x = pnt(1); y = pnt(2); z = pnt(3); L1 = v1 - x - y - z

       if (present(shp)) then
!       
!--       aux 4 noeuds sommets :
!
!                       (0,0,0)       (1,0,0)       (0,1,0)      (0,0,1) 
          shp(1:4) = (/ L1*(v2*L1-v1), x*(v2*x-v1), y*(v2*y-v1), z*(v2*z-v1) /)
!
!--    aux noeuds milieux des aretes :
!
!                           1-2    2-3   3-1    1-4   2-4   3-4
          shp(5:10) = v4*(/ x*L1 , x*y , y*L1 , z*L1, z*x , y*z /)   
          
       end if          
          
       if (present(drv)) then 
!       
!--       aux 4 noeuds sommets :
!
!                        (0,0,0)       (1,0,0)       (0,1,0)      (0,0,1) 
          drv(1,1:4) = (/ v1-v4*L1     , v4*x-v1    , v0         , v0          /)
          drv(2,1:4) = (/ v1-v4*L1     , v0         , v4*y-v1    , v0          /)
          drv(3,1:4) = (/ v1-v4*L1     , v0         , v0         , v4*z-v1     /)
!
!--       aux noeuds milieux des aretes :
!
!          aretes :          1-2    2-3   3-1    1-4   2-4   3-4
          drv(1,5:10) = v4*(/ L1-x ,   y ,-y    ,-z   , z   , v0  /)
          drv(2,5:10) = v4*(/-x    , x   , L1-y ,-z   , v0  , z   /)
          drv(3,5:10) = v4*(/-x    , v0  ,-y    , L1-z, x   , y   /)
       end if    
       
    end if

    if (present(cnd)) then
       cnd(1,:) = (/ v0, v1, v0, v0, v05, v05, v0 , v0 , v05, v0 /)
       cnd(2,:) = (/ v0, v0, v1, v0, v0 , v05, v05, v0 , v0 , v05/)
       cnd(3,:) = (/ v0, v0, v0, v1, v0 , v0 , v0 , v05, v05, v05/)
    end if

    if (present(info)) info = (/10, 3 /)

    end subroutine element_Tetr10


!=============================================================================================
    SUBROUTINE element_Hexa08 (pnt, shp, drv, cnd, info)
!=============================================================================================
    integer(Ikind), dimension(2  ), intent(   out), optional :: info
    real   (Rkind), dimension(3  ), intent(in    ), optional :: pnt
    real   (Rkind), dimension(  8), intent(   out), optional :: shp
    real   (Rkind), dimension(3,8), intent(   out), optional :: drv
    real   (Rkind), dimension(3,8), intent(   out), optional :: cnd
!---------------------------------------------------------------------------------------------
!
!   Fonctions d'interpolation et leurs derivees pour un element
!
!                 hexaedre a 8 noeuds de type Lagrange (Q1)
!
!     au point de coordonnee locale x,y,z (-1=<x=<1, -1=<y=<1, -1=<z<=1)
!
!     En entree : pnt ...................... coordonnees du point
!     En sortie : shp(1:3) ................. valeurs des fonctions d'interpolation
!               : drv(1:3,1:8) ............. valeurs de leurs derivees
!               : cnd(1:3,1:8) (optionnel) . coordonnees locales des noeuds
!               : nnd (optionnel) .......... nbr de noeuds (8)
!
!      numerotation : (sommet)_i=1,8 comme suit :
!
!                      ___________
!                    /|2         /|6
!                   / |         / |
!                  /  |        /  |
!                 /   |       /   |
!               3/____|______/7   |
!                |    |______|____|
!                |    /1     |    /5
!                |   /       |   /
!                |  /        |  /
!                | /         | /
!                |/__________|/
!                4           8
!---------------------------------------------------------------------------------------------

!-- local variables: -------------------------------------------------------------------------
    real(Rkind), parameter :: v0125=0.125_Rkind, v1=1.0_Rkind
    real(Rkind)            :: x, y, z, xm, xp, ym, yp, zm, zp
!---------------------------------------------------------------------------------------------

    if (present(pnt)) then

       x = pnt(1); y = pnt(2); z = pnt(3)

       xm = v1-x; xp = v1+x; ym = v1-y; yp = v1+y; zm = v1-z; zp = v1+z

       if (present(shp)) then
          shp = v0125 * (/ xm*ym*zm, xm*ym*zp, xp*ym*zp, xp*ym*zm, &
                           xm*yp*zm, xm*yp*zp, xp*yp*zp, xp*yp*zm /)
       end if
       
       if (present(drv)) then                    
          drv(1,:) = v0125 * (/-ym*zm   ,-ym*zp   , ym*zp   , ym*zm   , &
                               -yp*zm   ,-yp*zp   , yp*zp   , yp*zm    /)

          drv(2,:) = v0125 * (/-xm*zm   ,-xm*zp   ,-xp*zp   ,-xp*zm   , &
                                xm*zm   , xm*zp   , xp*zp   , xp*zm    /)
          drv(3,:) = v0125 * (/-xm*ym   , xm*ym   , xp*ym   ,-xp*ym   , &
                               -xm*yp   , xm*yp   , xp*yp   ,-xp*yp    /)
       end if
                               
    end if

    if (present(cnd)) then
       cnd(1,:) = (/-v1,-v1, v1, v1,-v1,-v1, v1, v1 /)
       cnd(2,:) = (/-v1,-v1,-v1,-v1, v1, v1, v1, v1 /)
       cnd(3,:) = (/-v1, v1, v1,-v1,-v1, v1, v1,-v1 /)
    end if

    if (present(info)) info = (/ 8, 3 /)

    end subroutine element_Hexa08
    
    
!=============================================================================================
    SUBROUTINE element_jacob (djacb, elcod, deriv, cartd, ndime, nnode)
!=============================================================================================
    integer(Ikind), intent(in ) :: ndime, nnode
    real   (Rkind), intent(in ) :: elcod(ndime,nnode), deriv(ndime,nnode)
    real   (Rkind), intent(out) :: cartd(ndime,nnode), djacb    
!---------------------------------------------------------------------------------------------
!
!   Calcul du jacobien et des derivees cartesiennes en 1, 2 ou 3 dimensions
!
!      (source : Dhatt et Touzot : une presentation de la M.E.F.)
!
!   - en entree :
!
!       deriv  : derivees locales des fonctions de formes
!       elcod  : coordonnees des noeuds de l'element
!
!   - en sortie :
!
!       djacb  : determinant de la matrice jacobienne
!       cartd  : derivees cartesiennes des fonctions de formes (calculees que si djacb > 0)
!
!   Attention : Si le jacobien est negatif ou nul, sortie sans calcul des derivees et sans
!               message d'erreur. 
!               Il appartient a la procedure appelante de faire le test et, si necessaire,
!               d'arreter les calculs. 
!---------------------------------------------------------------------------------------------

!-- local variables: ------------------------------------------------------------------------
    real   (Rkind), parameter   :: zero = 0.0_Rkind, one = 1.0_Rkind
    real   (Rkind)              :: djacbi, c
    integer(Ikind)              :: in, id, jd, j
    real   (Rkind)              :: xjacm(9), xjaci(9)
!---------------------------------------------------------------------------------------------

!
!-- 1) calcul de la matrice jacobienne xjacm
!
    j = 1
    do id = 1, ndime
       do jd = 1, ndime
          c = zero
          do in = 1, nnode
             c = c + deriv(jd,in) * elcod(id,in)
          end do
          xjacm(j) = c
          j = j + 1
       end do
    end do
!
!-- 2) calcul du determinant de la matrice jacobienne et de la matrice inverse
!
    if (ndime == 1) then  ! cas 1D
    
         djacb = xjacm(1)
    
         if (djacb <= 0) return
      
         xjaci(1) = one / djacb
    
    end if

    if (ndime == 2) then ! cas 2D
 
       djacb = xjacm(1)*xjacm(4) - xjacm(2)*xjacm(3)

       if (djacb <= 0) return 

       djacbi = one / djacb
       xjaci(1:4) = [ xjacm(4), -xjacm(2), -xjacm(3), xjacm(1) ] * djacbi

    end if

    if (ndime == 3) then ! cas 3D

       djacb = xjacm(1)*(xjacm(5)*xjacm(9) - xjacm(8)*xjacm(6)) +   &
               xjacm(4)*(xjacm(8)*xjacm(3) - xjacm(2)*xjacm(9)) +   &
               xjacm(7)*(xjacm(2)*xjacm(6) - xjacm(5)*xjacm(3))

       if (djacb <= 0) return 

       djacbi = one / djacb
       
       xjaci = [ xjacm(5)*xjacm(9) - xjacm(6)*xjacm(8),            &
                 xjacm(3)*xjacm(8) - xjacm(2)*xjacm(9),            & 
                 xjacm(2)*xjacm(6) - xjacm(3)*xjacm(5),            &
                 xjacm(7)*xjacm(6) - xjacm(4)*xjacm(9),            &
                 xjacm(1)*xjacm(9) - xjacm(7)*xjacm(3),            &
                 xjacm(4)*xjacm(3) - xjacm(6)*xjacm(1),            &
                 xjacm(4)*xjacm(8) - xjacm(7)*xjacm(5),            &
                 xjacm(2)*xjacm(7) - xjacm(8)*xjacm(1),            &
                 xjacm(1)*xjacm(5) - xjacm(4)*xjacm(2) ] * djacbi
    end if
!
!-- 3) calcul des derivees cartesiennes des fonctions de formes
!
    do id = 1, ndime
       do in = 1, nnode
          c = zero
          do jd = 1, ndime
             j = (jd-1) * ndime + id
             c = c + xjaci(j) * deriv(jd,in)
          enddo
          cartd(id,in) = c
       enddo
    enddo

   end subroutine element_jacob

!=============================================================================================
   FUNCTION element_det ( a, n, stat ) result ( det )
!=============================================================================================
   implicit none
   integer(Ikind), intent(in    ) :: n
   real   (Rkind), intent(in    ) :: a(n,n)
   type   (err_t), intent(in out) :: stat
   real   (Rkind)                 :: det
!---------------------------------------------------------------------------------------------
!  computes the determinant of a n x n matrix with 1 <= n <=3
!----------------------------------------------------------------------------------------- R.H.
 
!- local variables: --------------------------------------------------------------------------
!---------------------------------------------------------------------------------------------
   
   det = 0.0_Rkind
    
   select case(n)
      case(1)
         det = a(1,1)
    
      case(2)
         det = a(1,1)*a(2,2) - a(1,2)*a(2,1)

      case(3)
         det = a(1,1)*(a(2,2)*a(3,3) - a(3,2)*a(2,3)) +  &
               a(1,2)*(a(3,1)*a(2,3) - a(2,1)*a(3,3)) +  &
               a(1,3)*(a(2,1)*a(3,2) - a(3,1)*a(2,2))   
                
      case default
         stat = err_t ( stat = UERROR, where = 'element_det', msg =     &
                'The matrix must be 1x1, 2x2 or 3x3 only (here n = ' // &
                 util_intToChar(n) // ')')
   end select
    
   end function element_det

    
!=============================================================================================
   FUNCTION element_inverse ( a, n, stat ) result ( b )
!=============================================================================================
   implicit none
   integer(Ikind), intent(in    ) :: n
   real   (Rkind), intent(in    ) :: a(n,n)
   type   (err_t), intent(in out) :: stat
   real   (Rkind)                 :: b(n,n)
!---------------------------------------------------------------------------------------------
!  Computes the inverse of a n x n matrix with 1 <= n <=3
!----------------------------------------------------------------------------------------- R.H.
 
!- local variables: --------------------------------------------------------------------------
   real(Rkind ) :: deta
!---------------------------------------------------------------------------------------------                   

   deta = element_det ( a, n, stat )
   error_TraceNreturn(stat>0, 'element_inverse', stat) 
    
   if (deta == 0.0_Rkind) then
      stat = err_t ( stat = UERROR, where = 'element_inverse', msg = 'singular matrix')
      return
   end if              
       
   deta = 1.0_Rkind / deta
   
   select case(n)
       case(1)
          b(1,1) = 1.0_Rkind / deta
          
       case(2)
          b(1:2,1) = [ a(2,2) , -a(2,1)] * deta
          b(1:2,2) = [-a(1,2) ,  a(1,1)] * deta
          
       case(3)
          b(1:3,1) = [ a(2,2)*a(3,3) - a(2,3)*a(3,2)  ,          &
                       a(3,1)*a(2,3) - a(2,1)*a(3,3)  ,          &
                       a(2,1)*a(3,2) - a(3,1)*a(2,2)  ] * deta

          b(1:3,2) = [ a(1,3)*a(3,2) - a(1,2)*a(3,3)  ,           &
                       a(1,1)*a(3,3) - a(1,3)*a(3,1)  ,           &
                       a(1,2)*a(3,1) - a(3,2)*a(1,1)  ] * deta

          b(1:3,3) = [ a(1,2)*a(2,3) - a(1,3)*a(2,2)  ,           &
                       a(2,1)*a(1,3) - a(2,3)*a(1,1)  ,           &
                       a(1,1)*a(2,2) - a(1,2)*a(2,1)  ] * deta
        
       case default
         stat = err_t ( stat = UERROR, where = 'element_inverse', msg = &
                'The matrix must be 1x1, 2x2 or 3x3 only (here n = ' // &
                 util_intToChar(n) // ')')
   end select
    
   end function element_inverse   

end module element_m
