!---------------------------------------------------------------------------------------------
! marker, version 2023.4.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       main
!
! Description: 
!       Reads the outputs of a f.e. analysis (in "pfile" format) and extractes/interpolates
!       variables attached to a given node (or to a given point (in progress))
! 
! Note: 
!       In  this version the old adeli pfile format not considered
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        04/23
!
! Changes:
!        04/23
!        
!---------------------------------------------------------------------------------------------

PROGRAM main

   use marker_m
   use version_m   
   
   implicit none
      
   call signalHandler_SignalCatch (unit = STDOUT, title = '--> ptovtu Info:')
!
!- Disable halting after an error is detected in any procedures (callers must check "stat"):
!
   call err_SetHaltingMode ( halting = OFF, unit = STDOUT, DisplayWarning = ON ) 
!
!- Get the version ID:
!
   call version_getVersion ()
!
!- Get the command line arguments:
!   
   call utilmarker_getCommandArg ()
!
!- Read and parse the user's default file:
!
   call utilmarker_userDefault ()      
!
!- Proceed:
!      
   call marker ()
!
!- Report errors if any and print an end message:
!
   call utilmarker_end ()
   
END PROGRAM main
