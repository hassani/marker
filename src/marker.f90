!---------------------------------------------------------------------------------------------
! marker, version 2023.4.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       marker_m
!
! Description: 
!       Reads the outputs of a f.e. analysis (in "pfile" format) and extracts/computes 
!       variables attached to a given node/point/element
! 
! Notes: 
!       for compatibility reasons, the old adeli pfile format is also supported (but not
!       for long!)
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        04/23
!
! Changes:
!        04/23
!---------------------------------------------------------------------------------------------

#include "error.fpp"

MODULE marker_m

   use globalParameters_m
   use utilmarker_m
   use markerForOldPfile_m
   
   implicit none

   integer         , parameter   :: OldAsciiPfile = 0, AsciiPfile = 1, BinaryPfile = 2

   character(len=:), allocatable :: rec, recwk, rhs, lhs

   type     (str_t), allocatable :: subRec(:,:)

   integer  (Ikind)              :: ntok = 0, numStep = 0

   logical                       :: present_meshCells      = .false., &
                                    present_meshFacets     = .false., &
                                    present_meshEdges      = .false., &
                                    present_nodalCoord     = .false., &
                                    present_cellsDomainId  = .false., &
                                    present_facetsDomainId = .false., &
                                    present_edgesDomainId  = .false., &
                                    present_cellsVtkId     = .false., &
                                    present_facetsVtkId    = .false., &
                                    present_edgesVtkId     = .false., &
                                    first                  = .true.
CONTAINS

!=============================================================================================   
   SUBROUTINE marker
!============================================================================================= 

!-------------------------------------------------------------------------- RH 06/22 - 07/22 -

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'marker'
   integer                     :: pfileType
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE

   if ( stat > IZERO ) return 
!
!- Open the input file:
!   
   call marker_openPfile ( pfileType )
   error_TraceNreturn(stat>IZERO, HERE, stat) 
!
!- Execute:
!                      
   if ( pfileType == OldAsciiPfile ) then
      ! for the old ascii p-file:
      
      call markerForOldPfile_initialize ()
      error_TraceNreturn(stat>IZERO, HERE, stat)   
      
      call markerForOldPfile_exec ()
      error_TraceNreturn(stat>IZERO, HERE, stat)   
      
      !stat = err_t ( stat = EOF, where = HERE, msg = 'Old pfile not yet supported' )
      !return
   else
      ! for ascii and binary p-file:
      
      call marker_initialize ( pfileType )
      error_TraceNreturn(stat>IZERO, HERE, stat)   
      
      call marker_exec ()
      error_TraceNreturn(stat>IZERO, HERE, stat)
   end if
      
!print*,'end '//HERE 
   END SUBROUTINE marker
      
   
!=============================================================================================   
   SUBROUTINE marker_exec
!============================================================================================= 

!---------------------------------------------------------------------------------------------
!  Proceed
!---------------------------------------------------------------------------------- RH 06/22 - 

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'marker_exec'
   logical                     :: is_readyToPrint = .false.
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE
      
   do while ( stat <= IZERO )

      call pfile%read ( rec, stat ) 
      
      recwk = util_RemoveSpaces1 ( rec )
      
      if ( stat > IZERO .or. stat == EOF ) exit
            
      if ( len_trim(recwk) == 0 ) cycle ! blank line
      
      if ( index(adjustl(recwk),'#') == 1 ) cycle ! a comment
      
      ntok = util_CountTokens ( str = recwk, delims = ",", BlkToken = .false., opcl = "()", &
                                tokens = subRec, stat = stat )
  
      if (subRec(1,1)%str(1:1) /= '$') then
         call utilmarker_error ( msg='A keyword (starting with a "$") is expected',    & 
                                 err=UERROR, where=HERE, rec=rec, line=pfile%GetLine() ) 
         return
      end if    
   
      select case ( subRec(1,1)%str )
   
         case ( '$' )
            cycle ! a comment
            
         case ( '$Begin_step' )
            call marker_beginStepKeyword ; is_readyToPrint = .false.
            if ( stat > IZERO .or. stat == EOF ) exit
         
         case ( '$Fields' )
            call marker_fieldKeyword  
            if ( stat > IZERO .or. stat == EOF ) exit
            
         case ( '$Mesh:Connectivity' )
            call marker_ConnectivityKeyword 
            if ( stat > IZERO .or. stat == EOF ) exit

         case ( '$Mesh:DomainId' )
            call marker_DomainIdKeyword 
            if ( stat > IZERO .or. stat == EOF ) exit

         case ( '$Mesh:VtkTypeId' )
            call marker_VtkTypeIdKeyword 
            if ( stat > IZERO .or. stat == EOF ) exit
                                    
         case ( '$Mesh:Coordinates' )
            call marker_CoordinatesKeyword 
            if ( stat > IZERO .or. stat == EOF ) exit
             
         case ( '$End_step' )
            call marker_endStepKeyword ; is_readyToPrint = .true.
            if ( stat > IZERO .or. stat == EOF ) exit
         
            call marker_checkCoherency ()
            if ( stat > IZERO ) exit

            call utilmarker_InvariantsNEigenvalues ()
            if ( stat > IZERO ) exit
            
            call femval_displayVark1 ( vals, &
                 title = 'Description of the variables (only new ones) in your output file:' )    

            if ( currentStep == 1 ) then
               call utilmarker_findElem ()
               if ( stat > IZERO ) exit
            end if
            
            call utilmarker_write () 
            if ( stat > IZERO ) exit
            
            call marker_resetPresent ()    
             
         case ( '$End' )
            stat = err_t(stat=EOF,msg='(tag "$end" reached in the file "'//trim(myPfile)//'")')
            exit
         
         case default
            call utilmarker_error &
                ( err = UERROR, where = HERE, line = pfile%Getline(), rec = rec,  &
                  msg = 'Unknown keyword "'//subRec(1,1)%str//'"'                 )
            return     
      
      end select
   
   end do

   if ( .not. is_readyToPrint ) then
      if ( stat == RERROR ) then
         call stat%AddMsg ( ' (the step #'//util_intToChar(currentStep)//' is incomplete?)' )
      else if ( stat == EOF ) then
         call utilmarker_error ( err = WARNING, where = HERE, line = pfile%Getline(),     &
              msg = 'Last step (num='//util_intToChar(currentStep)//') is incomplete' )
      end if
   end if
   
   error_TraceNreturn(stat>IZERO, HERE, stat)
   
!print*,'end '//HERE 
   END SUBROUTINE marker_exec
   

!=============================================================================================   
   SUBROUTINE marker_openPfile ( pfileType )
!=============================================================================================   
   integer, intent(out) :: pfileType
!---------------------------------------------------------------------------------------------
!  Opens the input file and determines its type (ascii/binary, old adeli ascii pfile)
!  Returns:
!  . pfileType = AsciiPfile for an ascii p-file
!  . pfileType = BinaryPfile for a binary p-file
!  . pfileType = OldAsciiPfile for the old ascii p-file format
!---------------------------------------------------------------------------------- RH 06/22 -
   
!- local variables: --------------------------------------------------------------------------   
   character(len=* ), parameter   :: HERE = 'marker_openPfile'
   integer  (Ikind )              :: n, p
   character(len=80)              :: buf 
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE

   myPfile = trim(adjustl(filein))
   
   n = len_trim(wildcard)
   if ( n /= 0 ) then
      p = index(wildcard,'*')
      if ( p /= 0 ) then
         myPfile = wildcard(1:p-1) // myPfile // wildcard(p+1:n)
      else
         call utilmarker_error ( err = WARNING, where = HERE, &
                   msg = 'Bad wildcard: "'//wildcard//'" (must contains the * symbol)')
      end if
   end if
   
   write(*,'(/,a,a,/)') 'openning the file: ',trim(myPfile)    
!
!- attempt to open the file in ascii format:
!   
   call pfile%open ( name = myPfile, form = 'formatted', stat = stat )   
   error_TraceNreturn(stat>IZERO, HERE, stat)

   n = len(buf)
   call pfile%read ( n, buf, stat, 'header' ) 
   error_TraceNreturn( (stat>IZERO .or. stat==EOF), HERE, stat )
   
   if ( index(buf,'Ascii') > 0 ) then
      pfileType = AsciiPfile   
      rec = buf
      
      recwk = util_RemoveSpaces1 ( rec )
      
   else if ( index(buf,'Binary') >  0 ) then
      pfileType = BinaryPfile
      
      call pfile%close ( )

      call pfile%open ( name = myPfile, form = 'unformatted', stat = stat )
      error_TraceNreturn( stat>IZERO, HERE, stat )
 
      call pfile%read ( rec, stat, 'header' )
      error_TraceNreturn( stat>IZERO .or. stat==EOF, HERE, stat )

      recwk = util_RemoveSpaces1 ( rec )
        
   else
!
!-    see if the old ascii pfile format is used (the file must start with an integer):
!      
      read(buf,*,iostat = iostat) n
      if ( iostat == IZERO ) then
         pfileType = OldAsciiPfile
         call pfile%rewind ( )
      else
         call utilmarker_error &
         ( err = UERROR, where = HERE, msg = 'The file must start with $binary or $ascii' )
         return       
      end if
      
   end if
!
!- Open the output file:
!
   open(newunit = uout, file = myPfile//'.mark', status = 'replace')
   
!print*,'end '//HERE    
   END SUBROUTINE marker_openPfile


!=============================================================================================   
   SUBROUTINE marker_initialize ( pfileType )
!=============================================================================================   
   integer, intent(in) :: pfileType
!---------------------------------------------------------------------------------------------
!  Initializes the structures that will contain the mesh and the values 
!---------------------------------------------------------------------------------- RH 06/22 -
   
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'marker_initialize'
   integer  (Ikind)            :: i, n, pos
   type     (str_t)            :: valNames(4)
!---------------------------------------------------------------------------------------------
!print*,'in '//HERE

   n = util_CountTokens ( str = recwk, delims = ",", BlkToken = .false., opcl = "()", &
                          tokens = subRec, stat = stat )

   do i = 2, n
      call util_getLhsNRhs ( subRec(i,1)%str, '=', lhs, rhs, pos )
      if ( pos == 0 ) then
         call utilmarker_error ( msg = 'Missing a symbol "="', err = UERROR,     &
                                 where = HERE, rec = rec, line = pfile%Getline() )
         return
      end if
      if ( lhs == 'sizeOfInt' ) then
         read(rhs,*,iostat=iostat) sizeOfInt
      else if ( lhs == 'sizeOfReal' ) then
         read(rhs,*,iostat=iostat) sizeOfReal
      else
         call utilmarker_error ( msg = 'Unknown argument "'//lhs//'"', err = UERROR, & 
                                 where = HERE, rec = rec, line = pfile%Getline()     )
         return   
      end if
      if ( utilmarker_readError( HERE, pfile%Getline(), rec ) ) return
   end do

   if ( sizeOfInt == IZERO ) then
      if ( pfileType == BinaryPfile ) then
         call utilmarker_error &
             ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
               msg = 'Missing "sizeOfInt" (required as your input file is binary)')   
         return
      else
         call utilmarker_error &
             ( err = WARNING, where = HERE, rec = rec, line = pfile%Getline(), &
               msg = 'Missing "sizeOfInt" (default size is choosen)')
         sizeOfInt  = Ikind      
      end if
   end if
   
   if ( sizeOfReal == IZERO ) then
      if ( pfileType == BinaryPfile ) then
         call utilmarker_error &
             ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
               msg = 'Missing "sizeOfReal" (required as your input file is binary)')   
         return
      else
         call utilmarker_error &
             ( err = WARNING, where = HERE, rec = rec, line = pfile%Getline(), &
               msg = 'Missing "sizeOfReal" (default size is choosen)')
         sizeOfReal = Rkind
      end if
   end if

   if ( pfileType == BinaryPfile ) then
      write(*,'(a)')'--> marker info: '
      write(*,'(a,i0,a,i0,a)') &
            '    . kind of integers in the input file: ',sizeOfInt,' (',sizeOfInt*8,'-bits)'
      write(*,'(a,i0,a,i0,a)') &
            '    . kind of reals in the input file   : ',sizeOfReal,' (',sizeOfReal*8,'-bits)'
      write(*,*) 
   end if         
                  
   valNames(NODV)%str = 'Nodal values'               
   valNames(ELMV)%str = 'Element values'               
   valNames(CSTV)%str = 'Constant values'  
   valNames(ELMI)%str = 'Element invariants'               
                
   call femval_init ( valNames, sizeOfReal, stat, vals )
   error_TraceNreturn(stat>IZERO, HERE, stat)
      
   call msh_init ( sizeOfInt, sizeOfReal, stat, mesh )
   error_TraceNreturn(stat>IZERO, HERE, stat)
  
!print*,'end '//HERE 
   END SUBROUTINE marker_initialize
   
      
!=============================================================================================   
   SUBROUTINE marker_beginStepKeyword
!=============================================================================================

!---------------------------------------------------------------------------------------------
!  Reads the data corresponding to the keyword "$Begin_step" 
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'marker_beginStepKeyword'
!--------------------------------------------------------------------------------------------- 
!print*,'in '//HERE

   call util_getLhsNRhs ( subRec(2,1)%str, '=', lhs, rhs )

   if (lhs == 'num') then
      read(rhs,*,iostat=iostat) numStep
      if ( utilmarker_readError( HERE, pfile%Getline(), rec ) ) return                    
   else  
      call utilmarker_error ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
                              msg = 'Unknown argument "'// lhs//'"'                          )   
      return
   end if   
   currentStep = currentStep + IONE
   
   write(*,'(/,a,i0,a,a)')'=> Reading step #',currentStep,' from ',myPfile
   

!print*,'end '//HERE 
   END SUBROUTINE marker_beginStepKeyword


!=============================================================================================   
   SUBROUTINE marker_endStepKeyword
!=============================================================================================

!---------------------------------------------------------------------------------------------
!  Reads the data corresponding to the keyword "$End_step" 
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'marker_endStepKeyword'
   integer  (Ikind)            :: num
!--------------------------------------------------------------------------------------------- 
!print*,'in '//HERE

   call util_getLhsNRhs ( subRec(2,1)%str, '=', lhs, rhs )

   if (lhs == 'num') then
      read(rhs,*,iostat=iostat) num
      if ( utilmarker_readError( HERE, pfile%Getline(), rec ) ) return                    
      
      if (num /= numStep) &
         call utilmarker_error ( err = WARNING, where = HERE, rec = rec,                  &
                     msg = 'Argument "num" of "end_step" has the value ' // rhs // NLT // &
                           'while it had the value '// util_intToChar(numStep) //         &
                           ' with the keyword "begin_step"', line = pfile%Getline()       )
         return
   else 
      call utilmarker_error ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
                              msg = 'Unknown argument "'// lhs//'"'                          ) 
      return              
   end if   

!print*,'end '//HERE            
   END SUBROUTINE marker_endStepKeyword
      
   
!=============================================================================================
   SUBROUTINE marker_fieldKeyword 
!=============================================================================================

!--------------------------------------------------------------------------------------------- 
!  Reads the data corresponding to the keyword "$Value" (nodal or elemental values)
!---------------------------------------------------------------------------------- RH 06/22 -

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter   :: HERE = 'marker_fieldKeyword'
   integer  (Ikind), parameter   :: i1 = 1, i2 = 2
   integer  (Ikind)              :: i, nval, nGeomEntities
   character(len=:), allocatable :: geomEntities
   logical                       :: desc
!--------------------------------------------------------------------------------------------- 
!print*,'in '//HERE

   desc = .true.
   nGeomEntities = 0
   geomEntities = ''
   nval = 0
   
   do i = 2, ntok
      call util_getLhsNRhs ( subRec(i,1)%str, '=', lhs, rhs )

      if ( lhs == 'nField' ) then
         read(rhs,*,iostat=iostat) nval
      else if (lhs == 'nGeomEntities') then
         read(rhs,*,iostat=iostat) nGeomEntities
      else if ( lhs == 'geomEntities' ) then
         geomEntities = rhs
      else if ( lhs == 'desc' ) then
         if ( rhs == 'no' ) desc = .false.
      else  
         call utilmarker_error ( msg = 'Unknown argument "'// lhs//'"', err = UERROR, & 
                                 where = HERE, rec = rec, line = pfile%Getline()      )
         return
      end if  
      
      if ( utilmarker_readError( HERE, pfile%Getline(), rec ) ) return         
   end do
   
   select case ( geomEntities )
      case ( 'nodes' )
         if ( vals(NODV)%is_active ) then
            call utilmarker_error ( err=UERROR, where=HERE, rec=rec, line=pfile%Getline(), &
                                    msg='Only one block of nodal fields is allowed'        )
            return
         end if
         vals(NODV)%is_active = .true.
         call marker_readValues ( nval, nGeomEntities, desc, vals(NODV) )
         vals(NODV)%geomEntities = geomEntities
         
      case ( 'cells' )
         if ( vals(ELMV)%is_active ) then
            call utilmarker_error ( err=UERROR, where=HERE, rec=rec, line=pfile%Getline(), &
                                    msg='Only one block of element fields is allowed'       )
            return
         end if
         vals(ELMV)%is_active = .true.
         call marker_readValues ( nval, nGeomEntities, desc, vals(ELMV) )
         vals(ELMV)%geomEntities = geomEntities
      
      case ( '-' )
         if ( vals(CSTV)%is_active ) then
            call utilmarker_error ( err=UERROR, where=HERE, rec=rec, line=pfile%Getline(), &
                                    msg='Only one block of constant fields is allowed'       )
            return
         end if
         vals(CSTV)%is_active = .true.
         nGeomEntities = IZERO
         call marker_readValues ( nval, nGeomEntities, desc, vals(CSTV) )
         vals(CSTV)%geomEntities = geomEntities

      case default
         call utilmarker_error &
              ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
                msg = 'Invalid value "'// geomEntities // '" for geomEntities' )  
         return    
   end select
   
   error_TraceNreturn( stat>IZERO.or.stat==EOF, HERE, stat )

!print*,'end '//HERE         
   END SUBROUTINE marker_fieldKeyword
   

!=============================================================================================
   SUBROUTINE marker_readValues (  nval, nent, desc, vals )
!=============================================================================================
   integer  (Ikind   ), intent(in    ) :: nval, nent 
   logical            , intent(in    ) :: desc
   class    (femval_t), intent(in out) :: vals
!--------------------------------------------------------------------------------------------- 
!  
!--------------------------------------------------------------------------------------------- 

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'marker_readValues'
   integer  (Ikind)            :: i, j, n, nentities
   logical                     :: present_name, present_phyq, present_unit, present_ncomp  
!--------------------------------------------------------------------------------------------- 
!print*,'in '//HERE
   
   if ( .not. desc ) then
      vals%is_new = .false.
      if ( vals%nvar /= nval ) then
         call utilmarker_error ( err=UERROR, where=HERE, rec=rec, line=pfile%Getline(),   &
         msg='When "desc=no" is used the number of values is assumed to be'//NLT//        &
             'the same as in the previous step (and the description of the values'//NLT// &
             'is then not to read).'//NLT//'Here nval = '//util_intToChar(nval)//         &
             ' while at the previous step nval = '//util_intToChar(vals%nvar)             ) 
         return   
      end if
      nentities = vals%nent
   else
      vals%is_new = .true. 
      nentities = nent    
   end if
   
   call vals%alloc ( nvar = nval, nent = nentities) 
   
   if ( desc ) then   
      do i = 1, nval

         present_name = .false. ; present_phyq  = .false.
         present_unit = .false. ; present_ncomp = .false.   
      
         call pfile%read ( rec, stat, 'description of '//vals%name )
                  
         error_TraceNreturn( stat>IZERO.or.stat==EOF, HERE, stat )
         
         recwk = util_RemoveSpaces1( rec )

         n = util_CountTokens ( str = recwk, delims = ",", BlkToken = .false., opcl = "()", &
                                tokens = subRec, stat = stat )
         do j = 1, n
            call util_getLhsNRhs ( subRec(j,1)%str, '=', lhs, rhs )

            if (lhs == 'name') then
               vals%varNames(i) = rhs
               present_name = .true.
            else if (lhs == 'physicalQuantity') then
               vals%physNames(i) = rhs
               present_phyq = .true.
            else if (lhs == 'unit') then
               vals%physUnits0(i) = rhs ! save a copie of the original unit
               vals%physUnits (i) = rhs
               present_unit = .true.
            else if (lhs == 'nComp') then
               read(rhs,*,iostat=iostat) vals%varNcomp(i)
               if ( utilmarker_readError( HERE, pfile%Getline(), rec ) ) return 
               present_ncomp = .true.
            else  
               call utilmarker_error ( err=UERROR, where=HERE, rec=rec,    &
                                       line=pfile%Getline(),               &
                                       msg='Unknown argument "'// lhs//'"' ) 
               return        
            end if         
         end do
      
         vals%please_convertMe(i) = .false.
      
         if ( .not. present_name ) then
            call utilmarker_error ( err=UERROR, where=HERE, rec=rec, line=pfile%Getline(), &
                                    msg='Name ("name") of the variable not specified'      ) 
            return        
         end if         
      
         if ( .not. present_phyq ) vals%physNames(i) = ''
      
         if ( .not. present_unit ) then
            vals%physUnits0(i) = ''
            vals%physUnits (i) = ''
         end if
      
         if ( .not. present_ncomp) then
            call utilmarker_error ( err=UERROR, where=HERE, rec=rec, line=pfile%Getline(), &
                                    msg='Number of components ("nComp") not specified'     ) 
            return        
         end if         
      
      end do   
   end if   

   if ( vals%is_new ) call vals % InitUnitConversion ( physQuantCatalog, convert, stat )  
     
   call vals % read ( file = pfile, stat = stat )
   error_TraceNreturn( stat>IZERO.or.stat==EOF, HERE, stat )
  
!print*,'end '//HERE         
   END SUBROUTINE marker_readValues


!=============================================================================================
   SUBROUTINE marker_ConnectivityKeyword
!=============================================================================================

!--------------------------------------------------------------------------------------------- 
!  Reads a connectivity table
!-------------------------------------------------------------------------- RH 06/22 - 07/22 -
      
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter   :: HERE = 'marker_ConnectivityKeyword'
   integer  (Ikind)              :: i
   integer  (Ikind)              :: nEnt
   integer  ( i64 )              :: msize
   logical                       :: present_nEnt, present_size, present_ent
   character(len=:), allocatable :: ent
!--------------------------------------------------------------------------------------------- 
!print*,'in '//HERE

   present_nEnt = .false. ; present_size  = .false. ; present_ent = .false.
   msize = 0_i64 ; nEnt = IZERO 
      
   do i = 2, ntok
      call util_getLhsNRhs ( subRec(i,1)%str, '=', lhs, rhs )

      if (lhs == 'size') then
         read(rhs,*,iostat=iostat) msize
         present_size = .true.
      else if (lhs == 'geomEntities') then
         present_Ent = .true.
         ent = rhs
      else if (lhs == 'nGeomEntities') then
         read(rhs,*,iostat=iostat) nEnt
         present_nEnt = .true.
      else  
         call utilmarker_error &
                          ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
                            msg = 'Unknown attribute "'// lhs//'"'                         ) 
         return
      end if   
      
      if ( utilmarker_readError( HERE, pfile%Getline(), rec ) ) return
   end do

   if ( .not. present_Ent ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
         msg = 'Geometrical entities ("geomEntities") not specified' )
      return
   end if
   
   if ( .not. present_nEnt ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
         msg = 'Number of geometrical entities ("nGeomEntities") not specified' )
      return
   end if
   
   if ( .not. present_size ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(),                &
         msg = 'Size of connectivity table ("size") not specified' )
      return
   end if
   
   if ( msize <= IZERO .or. nEnt <= IZERO ) then 
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(),  &
         msg = 'Bad value (<= 0) for "nGeomEntities" or "size"' )
      return    
   end if   
   
   select case ( ent )
      case ( "cells" )
         present_meshCells = .true.
         call mesh % alloc ( sizeConnect = msize, nCell = nEnt )
         call mesh % readConnect ( file=pfile, nCell=nEnT, stat=stat )
      case ( "facets" )
         present_meshFacets = .true.
         call mesh % alloc ( sizeConnect = msize, nFacet = nEnt )
         call mesh % readConnect ( file=pfile, nFacet=nEnT, stat=stat )
      case ( "edges" )
         present_meshEdges = .true.
         call mesh % alloc ( sizeConnect = msize, nEdge = nEnt )
         call mesh % readConnect ( file=pfile, nEdge=nEnT, stat=stat )   
      case default
         call utilmarker_error &
               ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(),  &
                 msg = 'Unknown geometrical entities: "'//ent//'"'               )
         return    
   end select
   
   error_TraceNreturn( stat>IZERO.or.stat==EOF, HERE, stat )         

!print*,'end '//HERE                                  
   END SUBROUTINE marker_ConnectivityKeyword


!=============================================================================================
   SUBROUTINE marker_DomainIdKeyword
!=============================================================================================

!--------------------------------------------------------------------------------------------- 
!  Reads a connectivity table
!-------------------------------------------------------------------------- RH 06/22 - 07/22 -
      
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter   :: HERE = 'marker_DomainIdKeyword'
   integer  (Ikind)              :: i
   integer  (Ikind)              :: nEnt, msize
   logical                       :: present_nEnt, present_size, present_ent
   character(len=:), allocatable :: ent
!--------------------------------------------------------------------------------------------- 
!print*,'in '//HERE

   present_nEnt = .false. ; present_size  = .false. ; present_ent = .false.
   msize = IZERO ; nEnt = IZERO 
      
   do i = 2, ntok
      call util_getLhsNRhs ( subRec(i,1)%str, '=', lhs, rhs )

      if (lhs == 'size') then
         read(rhs,*,iostat=iostat) msize
         present_size = .true.
      else if (lhs == 'geomEntities') then
         present_Ent = .true.
         ent = rhs
      else if (lhs == 'nGeomEntities') then
         read(rhs,*,iostat=iostat) nEnt
         present_nEnt = .true.
      else  
         call utilmarker_error &
                          ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
                            msg = 'Unknown attribute "'// lhs//'"'                         ) 
         return
      end if   
      
      if ( utilmarker_readError( HERE, pfile%Getline(), rec ) ) return
   end do

   if ( .not. present_Ent ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
         msg = 'Geometrical entities ("geomEntities") not specified' )
      return
   end if
   
   if ( .not. present_nEnt ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
         msg = 'Number of geometrical entities ("nGeomEntities") not specified' )
      return
   end if
   
   if ( .not. present_size ) then
      msize = nEnt
   else if ( msize /= nEnt .and. msize /= 1 ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
         msg = 'Bad value for "size". Must be equal to 1 or to "nGeomEntities"' )
      return  
   end if
   
   if ( msize <= IZERO .or. nEnt <= IZERO ) then 
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(),  &
         msg = 'Bad value (<= 0) for "nGeomEntities" or "size"' )
      return    
   end if   
   
   select case ( ent )
      case ( "cells" )
         present_cellsDomainId = .true. 
         call mesh % alloc ( nCell = nEnt )      
         call mesh % readDomainId ( file=pfile, length = msize, nCell=nEnT, stat=stat )
      case ( "facets" )
         present_facetsDomainId = .true.    
         call mesh % alloc ( nFacet = nEnt )        
         call mesh % readDomainId ( file=pfile, length = msize, nFacet=nEnT, stat=stat )
      case ( "edges" )
         present_edgesDomainId = .true.  
         call mesh % alloc ( nEdge = nEnt )        
         call mesh % readDomainId ( file=pfile, length = msize, nEdge=nEnT, stat=stat )     

      case default
         call utilmarker_error &
               ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(),  &
                 msg = 'Unknown geometrical entities: "'//ent//'"'               )
         return    
   end select
   
   error_TraceNreturn( stat>IZERO.or.stat==EOF, HERE, stat )         

!print*,'end '//HERE                                  
   END SUBROUTINE marker_DomainIdKeyword


!=============================================================================================
   SUBROUTINE marker_VtkTypeIdKeyword
!=============================================================================================

!--------------------------------------------------------------------------------------------- 
!  Reads a connectivity table
!-------------------------------------------------------------------------- RH 06/22 - 07/22 -
      
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter   :: HERE = 'marker_VtkTypeIdKeyword'
   integer  (Ikind)              :: i
   integer  (Ikind)              :: nEnt, msize
   logical                       :: present_nEnt, present_size, present_ent
   character(len=:), allocatable :: ent
!--------------------------------------------------------------------------------------------- 
!print*,'in '//HERE

   present_nEnt = .false. ; present_size  = .false. ; present_ent = .false.
   msize = IZERO ; nEnt = IZERO 
      
   do i = 2, ntok
      call util_getLhsNRhs ( subRec(i,1)%str, '=', lhs, rhs )

      if (lhs == 'size') then
         read(rhs,*,iostat=iostat) msize
         present_size = .true.
      else if (lhs == 'geomEntities') then
         present_Ent = .true.
         ent = rhs
      else if (lhs == 'nGeomEntities') then
         read(rhs,*,iostat=iostat) nEnt
         present_nEnt = .true.
      else  
         call utilmarker_error &
                          ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
                            msg = 'Unknown attribute "'// lhs//'"'                         ) 
         return
      end if   
      
      if ( utilmarker_readError( HERE, pfile%Getline(), rec ) ) return
   end do

   if ( .not. present_Ent ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
         msg = 'Geometrical entities ("geomEntities") not specified' )
      return
   end if
   
   if ( .not. present_nEnt ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
         msg = 'Number of geometrical entities ("nGeomEntities") not specified' )
      return
   end if
   
   if ( .not. present_size ) then
      msize = nEnt
   else if ( msize /= nEnt .and. msize /= 1 ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
         msg = 'Bad value for "size". Must be equal to 1 or to "nGeomEntities"' )
      return  
   end if
   
   if ( msize <= IZERO .or. nEnt <= IZERO ) then 
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(),  &
         msg = 'Bad value (<= 0) for "nGeomEntities" or "size"' )
      return    
   end if   
   
   select case ( ent )
      case ( "cells" )
         present_cellsVtkId = .true.
         call mesh % alloc ( nCell = nEnt )      
         call mesh % readVtkTypeId ( file=pfile, length = msize, nCell=nEnT, stat=stat )
      case ( "facets" )
         present_facetsVtkId = .true.
         call mesh % alloc ( nFacet = nEnt )      
         call mesh % readVtkTypeId ( file=pfile, length = msize, nFacet=nEnT, stat=stat )
      case ( "edges" )
         present_edgesVtkId = .true.
         call mesh % alloc ( nEdge = nEnt )      
         call mesh % readVtkTypeId ( file=pfile, length = msize, nEdge=nEnT, stat=stat )   

      case default
         call utilmarker_error &
               ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(),  &
                 msg = 'Unknown geometrical entities: "'//ent//'"'               )
         return    
   end select
   
   error_TraceNreturn( stat>IZERO.or.stat==EOF, HERE, stat )         

!print*,'end '//HERE                                  
   END SUBROUTINE marker_VtkTypeIdKeyword


!=============================================================================================
   SUBROUTINE marker_CoordinatesKeyword
!=============================================================================================

!--------------------------------------------------------------------------------------------- 
!  Reads the data corresponding to the keyword "$Mesh:Nodes" (nodal coordinates)
!-------------------------------------------------------------------------- RH 06/22 - 07/22 -
      
!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'marker_CoordinatesKeyword'
   integer  (Ikind)            :: i
   integer  (Ikind)            :: dimCoord(2)
   logical                     :: present_ndime, present_nnode, present_unit
!--------------------------------------------------------------------------------------------- 
!print*,'in '//HERE

   present_ndime = .false. ; present_nnode  = .false. ; present_unit = .false.

   dimCoord(:) = IZERO 
   
   do i = 2, ntok
      call util_getLhsNRhs ( subRec(i,1)%str, '=', lhs, rhs )

      if (lhs == 'nDime') then
         read(rhs,*,iostat=iostat) dimCoord(1)
         present_ndime = .true.
      else if (lhs == 'nNode') then
         read(rhs,*,iostat=iostat) dimCoord(2)   
         present_nnode = .true.           
      else if (lhs == 'unit' ) then 
         mesh%coordUnit0 = rhs ! save a copie of the original unit
         mesh%coordUnit  = rhs
         present_unit = .true.
      else  
         call utilmarker_error &
                          ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
                            msg = 'Unknown argument "'// lhs//'"'                          ) 
         return
      end if   
      
      if ( utilmarker_readError( HERE, pfile%Getline(), rec ) ) return
   end do
   
   if ( .not. present_ndime ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
         msg = 'Physical dimension ("nDime") not specified'            )
      return
   end if
   
   if ( .not. present_nnode ) then
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(), &
         msg = 'Number of nodes ("nNode") not specified'               )
      return
   end if
   
   if ( .not. present_unit ) then
      mesh%coordUnit0 = ''
      mesh%coordUnit  = ''
   end if
      
   if (dimCoord(1) > IZERO .and. dimCoord(2) > IZERO ) then
      present_nodalCoord = .true.   
      call mesh % InitUnitConversion ( physQuantCatalog, convert, stat )  
      call mesh % alloc ( dimCoord = dimCoord )
      call mesh % readCoord ( file = pfile, stat = stat )
      error_TraceNreturn( stat>IZERO.or.stat==EOF, HERE, stat )
   else
      call utilmarker_error &
      ( err = UERROR, where = HERE, rec = rec, line = pfile%Getline(),  &
         msg = 'Bad value (0) for "nDime" or "nNode"'                   )
      return    
   end if

!print*,'end '//HERE                                  
   END SUBROUTINE marker_CoordinatesKeyword


!=============================================================================================   
   SUBROUTINE marker_resetPresent
!=============================================================================================  

   present_meshCells      = .false.
   present_meshFacets     = .false.
   present_meshEdges      = .false.
   present_nodalCoord     = .false.   

   present_cellsDomainId  = .false.
   present_facetsDomainId = .false.
   present_edgesDomainId  = .false.
   present_cellsVtkId     = .false.
   present_facetsVtkId    = .false.
   present_edgesVtkId     = .false.
                                       
   vals(NODV)%is_active = .false.
   vals(ELMV)%is_active = .false.
   vals(CSTV)%is_active = .false.
   
   if ( first ) first = .false.
   
   END SUBROUTINE marker_resetPresent
   
            
!=============================================================================================
   SUBROUTINE marker_checkCoherency
!=============================================================================================

!- local variables: --------------------------------------------------------------------------   
   character(len=*), parameter :: HERE = 'marker_checkCoherency'
!--------------------------------------------------------------------------------------------- 
!print*,'in '//HERE

   if ( first ) then
      if ( .not. present_meshCells ) then
         call utilmarker_error ( err = UERROR, where = HERE,                                & 
                   msg = 'Mesh connectivity not found (needed at least for the first step)' )
         return
      end if
      if ( .not. present_nodalCoord ) then
         call utilmarker_error ( err = UERROR, where = HERE,                                &
                   msg = 'Nodal coordinates not found (needed at least for the first step)' )
         return
      end if
   end if
   
   if ( present_meshCells ) then
      if ( .not. present_cellsVtkId ) then
         call utilmarker_error ( err = UERROR, where = HERE,  &
                                 msg = 'Vtk types not found for cells' )
         return
      else
         call mesh % computeIndx ( stat, cells = .true. )
         error_TraceNreturn( stat>IZERO, HERE, stat )
      end if
   end if
   
   if ( present_meshFacets ) then
      if ( .not. present_facetsVtkId ) then
         call utilmarker_error ( err = UERROR, where = HERE,  &
                                 msg = 'Vtk types not found for facets' )
         return
      else
         call mesh % computeIndx ( stat, facets = .true. )
         error_TraceNreturn( stat>IZERO, HERE, stat )
      end if
   end if   

   if ( present_meshEdges ) then
      if ( .not. present_edgesVtkId ) then
         call utilmarker_error ( err = UERROR, where = HERE,  &
                                 msg = 'Vtk types not found for edges' )
         return
      else
         call mesh % computeIndx ( stat, edges = .true. )
         error_TraceNreturn( stat>IZERO, HERE, stat )
      end if
   end if   
      
   if ( vals(NODV)%is_active ) then
      if ( vals(NODV)%nent /= mesh%npoin ) then
         call utilmarker_error ( err = UERROR, where = HERE,                            &  
                msg = 'The number of nodes given to the keyword "nodal_values" ('//     &
                       util_intToChar(vals(NODV)%nent)//')' // NLT //                   &
                      'is different from the number of nodes in the mesh ('  //         &
                       util_intToChar(mesh%npoin)//')'                                  )
         return
      end if
   end if

   if ( vals(ELMV)%is_active ) then
      if ( vals(ELMV)%nent /= mesh%ncell) then
         call utilmarker_error ( err = UERROR, where = HERE,                           &
              msg = 'The number of elements given to the keyword "element_values" ('// &
                     util_intToChar(vals(ELMV)%nent)//')' // NLT //                    &
                    'is different from the number of elements in the mesh ('  //       &
                     util_intToChar(mesh%ncell)//')'                                   )
         return
      end if
   end if     

!print*,'end '//HERE    
   END SUBROUTINE marker_checkCoherency
       
END MODULE marker_m
