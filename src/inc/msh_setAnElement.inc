!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! include file "msh_setAnElement.inc" for the module msh_m. Common lines for subroutines:
!      . msh_setAnElement32Sp
!      . msh_setAnElement32Dp
!      . msh_setAnElement64Sp
!      . msh_setAnElement64Dp
!---------------------------------------------------------------------------------------------

   integer(Ikind) :: i1, i2, n
   
   if ( elemId > self%nCell ) then
      stat = err_t (stat = UERROR, where = HERE, &
                     msg = 'The element Id is greater than the number of cells')
      return
   end if

   n = size(connect)
   
   if ( elemId == IONE ) then
      i2 = IZERO
   else
      i2 = self % cellConnect % indx(elemId-1)
   end if
   i1 = i2 + IONE ; i2 = i2 + n

   if ( i2 > self % cellConnect % usedSize ) then
      stat = err_t (stat = UERROR, where = HERE, msg = 'Size of connectivity table exceeded')
      return
   end if
   
   self % cellConnect % connect (i1:i2) = connect(:)

   self % cellConnect % domains(elemId) = domain
   self % cellConnect % types  (elemId) = type
   self % cellConnect % indx   (elemId) = i2
