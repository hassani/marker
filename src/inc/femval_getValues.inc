!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! include file "femval_getValues.inc" for the module femval_m. Common lines for subroutines:
!      . femval_getValuesSp
!      . femval_getValuesDp
!---------------------------------------------------------------------------------------------

!- local variables ---------------------------------------------------------------------------
   integer  ( i64 ) :: k0, k1, k2
   integer  (Ikind) :: i1, i2, n, ivar, ient, nent
!--------------------------------------------------------------------------------------------- 

   if ( present(varId) ) then
      if ( varId <= 0 .or. varId > self%nvar ) then
         stat = err_t ( stat = UERROR, where = HERE, msg = &
                        'Variable #'//util_IntToChar(varId)//' is out off range ' // &
                        '(must be between 1 and '//util_IntToChar(self%nvar) //')'   )
         return
      end if
   end if

   if ( present(entId) ) then
      if ( self%nent > 0 .and. (entId <= 0 .or. entId > self%nent) ) then
         stat = err_t ( stat = UERROR, where = HERE, msg = &
                        'Entity #'//util_IntToChar(entId)//' is out off range ' // &
                        '(must be between 1 and '//util_IntToChar(self%nent) //')' )
         return
      end if
      if ( self%nent == 0 ) then
         ient = IONE
      else
         ient = entId
      end if
   end if
   
   nent = max(IONE,self%nent)
   
   if ( present(varId) .and. present(entId) ) then
   
      n = self%varNcomp(varId)
      
      if ( allocated(values) ) then
         if ( size(values) < n ) then
            deallocate(values) ; allocate(values(n))
         end if
      else
         allocate(values(n))
      end if
            
      if ( varId == 1 ) then
         k0 = 0
      else
         k0 = sum ( self%varNcomp(1:varId-1) ) * nent
      end if

      k2 = k0 + ient * n
      k1 = k2 - n + 1

      if ( self%please_convertMe(varId) ) then
         if ( self%convOper(varId) == '*' ) then
            values(1:n) = self%v(k1:k2) * self%convUnit(varId)  
         else 
            values(1:n) = self%v(k1:k2) + self%convUnit(varId) 
         end if
      else
         values(1:n) = self%v(k1:k2)
      end if                   
               
   else if ( present(varId) ) then

      if ( varId == 1 ) then
         k1 = 1
      else
         k1 = sum ( self%varNcomp(1:varId-1) ) * nent + 1
      end if
   
      n = self%varNcomp(varId) * nent
   
      if ( allocated(values) ) then
         if ( size(values) < n ) then
            deallocate(values) ; allocate(values(n))
         end if
      else
         allocate(values(n))
      end if
   
      k2 = k1 + n - 1
   
      if ( self%please_convertMe(varId) ) then
         if ( self%convOper(varId) == '*' ) then
            values(1:n) = self%v(k1:k2) * self%convUnit(varId)  
         else 
            values(1:n) = self%v(k1:k2) + self%convUnit(varId) 
         end if
      else
         values(1:n) = self%v(k1:k2)
      end if  
            
   else if ( present(entId) ) then
   
      n = sum ( self%varNcomp ) 
      
      if ( allocated(values) ) then
         if ( size(values) < n ) then
            deallocate(values) ; allocate(values(n))
         end if
      else
         allocate(values(n))
      end if

      i1 = IONE ; k0 = IZERO
            
      do ivar = 1, self%nvar      

         n = self%varNcomp(ivar)

         k2 = k0 + ient * n ; k1 = k2 - n + IONE
         i2 = i1 + n - IONE

         if ( self%please_convertMe(ivar) ) then
            if ( self%convOper(ivar) == '*' ) then
               values(i1:i2) = self%v(k1:k2) * self%convUnit(ivar)  
            else 
               values(i1:i2) = self%v(k1:k2) + self%convUnit(ivar) 
            end if
         else
            values(i1:i2) = self%v(k1:k2)
         end if  
      
         i1 = i2 + IONE ; k0 = i2 * nent
      end do
   else
      stat = err_t ( stat = UERROR, where = HERE, msg = 'Missing "entId" or "varId"')
      return
   end if