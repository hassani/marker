!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! include file "msh_readConnect.inc" for the module msh_m. Common lines for subroutines:
!      . msh_readConnect32Sp
!      . msh_readConnect32Dp
!      . msh_readConnect64Sp
!      . msh_readConnect64Dp
!---------------------------------------------------------------------------------------------

   if ( present(nCell) ) then
      call self % cellConnect % readConnect (file, stat, what = 'Cell-to-Node connectivity')

   else if ( present(nFacet) ) then
      call self % facetConnect % readConnect (file, stat, what = 'Facet-to-Node connectivity')
   
   else if ( present(nEdge) ) then
      call self % edgeConnect % readConnect (file, stat, what = 'Edge-to-Node connectivity')

   end if   
      