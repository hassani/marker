!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! include file "utilptovtu_dsyevj3.inc" for the module utilptovtu_m. Common lines for 
! subroutines:
!      . utilptovtu_DSYEVJ3sp
!      . utilptovtu_DSYEVJ3dp
!
! These lines are a copie of the core DSYEVJ3 subroutine of Joachim Kopp (Copyright (C) 2006)
!---------------------------------------------------------------------------------------------

!     initialize q to the identitity matrix
!     --- this loop can be omitted if only the eigenvalues are desired ---
      
      sd = ZERO
      do 10 x = 1, n
        q(x,x) = ONE
        do 11, y = 1, x-1
          q(x, y) = ZERO
          q(y, x) = ZERO
          sd = max(sd,abs(a(x,y)))
   11   continue
   10 continue

!     initialize w to diag(a)
      do 20 x = 1, n
        w(x) = a(x, x)
   20 continue
   
      if (sd == ZERO) return

!     calculate sqr(tr(a))  
      sd = ZERO
      do 30 x = 1, n
        sd = sd + abs(w(x))
   30 continue
      sd = sd**2
 
!     main iteration loop
      do 40 i = 1, 50
!       test for convergence
        so = ZERO
        do 50 x = 1, n
          do 51 y = x+1, n
            so = so + abs(a(x, y))
   51     continue
   50   continue
        if (so .eq. ZERO) then
          return
        end if

        if (i .lt. 4) then
          thresh = TWOTENTH * so / n**2
        else
          thresh = ZERO
        end if

!       do sweep
        do 60 x = 1, n
          do 61 y = x+1, n
            g = HUND * ( abs(a(x, y)) )
            if ( i .gt. 4 .and. abs(w(x)) + g .eq. abs(w(x))       &
                          .and. abs(w(y)) + g .eq. abs(w(y)) ) then
              a(x, y) = ZERO
            else if (abs(a(x, y)) .gt. thresh) then
!             calculate jacobi transformation
              h = w(y) - w(x)
              if ( abs(h) + g .eq. abs(h) ) then
                t = a(x, y) / h
              else
                theta = HALF * h / a(x, y)
                if (theta .lt. ZERO) then
                  t = -ONE / (sqrt(ONE + theta**2) - theta)
                else
                  t = ONE / (sqrt(ONE + theta**2) + theta)
                end if
              end if

              c = ONE / sqrt( ONE + t**2 ) 
              s = t * c
              z = t * a(x, y)
              
!             apply jacobi transformation
              a(x, y) = ZERO
              w(x)    = w(x) - z
              w(y)    = w(y) + z
              do 70 r = 1, x-1
                t       = a(r, x)
                a(r, x) = c * t - s * a(r, y)
                a(r, y) = s * t + c * a(r, y)
   70         continue
              do 80, r = x+1, y-1
                t       = a(x, r)
                a(x, r) = c * t - s * a(r, y)
                a(r, y) = s * t + c * a(r, y)
   80         continue
              do 90, r = y+1, n
                t       = a(x, r)
                a(x, r) = c * t - s * a(y, r)
                a(y, r) = s * t + c * a(y, r)
   90         continue

!             update eigenvectors
!             --- this loop can be omitted if only the eigenvalues are desired ---
              do 100, r = 1, n
                t       = q(r, x)
                q(r, x) = c * t - s * q(r, y)
                q(r, y) = s * t + c * q(r, y)
  100         continue
            end if
   61     continue
   60   continue
   40 continue

      print *, "dsyevj3: no convergence."