!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! include file "file_fbaseReadAsii.inc" for the module file_m. Common lines for subroutines:
!      . file_fbaserSpk0ReaderAscii
!      . file_fbaserSpk1ReaderAscii
!      . file_fbaserSpk2ReaderAscii
!
!      . file_fbaserDpk0ReaderAscii
!      . file_fbaserDpk1ReaderAscii
!      . file_fbaserDpk2ReaderAscii
!
!      . file_fbaseI32k0ReaderAscii
!      . file_fbaseI32k1ReaderAscii
!      . file_fbaseI32k2ReaderAscii
!
!      . file_fbaseI64k0ReaderAscii
!      . file_fbaseI64k1ReaderAscii
!      . file_fbaseI64k2ReaderAscii
!---------------------------------------------------------------------------------------------

   integer                       :: ios
   character(len=:), allocatable :: msg

   self%line = self%line + 1 ; read(self%unit,*,iostat=ios) x

   if ( ios > 0 ) then
      if ( present(what) ) then
         msg = 'while reading '//what
      else
         msg = 'Read error'
      end if
      msg = msg // ' at record #'//i2a(self%line)// ' of the file "'//self%name//'"'      
      stat = err_t ( stat = RERROR, where = HERE, msg = msg )
   else if ( ios < 0 ) then
      stat = err_t ( stat = EOF, where = HERE, &
                      msg = '(end of file "'//self%name//'" reached)')
   end if
