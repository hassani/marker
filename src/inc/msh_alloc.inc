!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! include file "msh_alloc.inc" for the module msh_m. Common lines for subroutines:
!      . msh_alloc32Sp
!      . msh_alloc32Dp
!      . msh_alloc64Sp
!      . msh_alloc64Dp
!---------------------------------------------------------------------------------------------

!
!- for cells:
!
   if ( present(nCell) ) then
   
      call self % cellConnect % alloc ( n = nCell, msize = sizeConnect )
      self % nCell = nCell
   
   end if
   
   if ( present(nFacet) ) then
   
      call self % facetConnect % alloc ( n = nFacet, msize = sizeConnect )
      self % nFacet = nFacet
 
   end if
   
   if ( present(nEdge) ) then

      call self % edgeConnect % alloc ( n = nEdge, msize = sizeConnect )
      self % nEdge = nEdge
      
   end if

!
!- for node coordinates:
!         
   if ( present(dimCoord) ) then
   
      ! if the two dimensions of %coord are different from dimCoord(:), resize it:
      if ( allocated(self % coord) ) then
         if ( size(self%coord,1) /= dimCoord(1) .or. size(self%coord,2) /= dimCoord(2) ) &
            deallocate( self % coord )
      end if 
      if ( .not. allocated(self % coord) ) then
         allocate( self % coord(dimCoord(1),dimCoord(2)) )
      
         ! save the dimensions of %coord:
         self % nDime = dimCoord(1) ; self % nPoin = dimCoord(2)
      end if
      
   end if
