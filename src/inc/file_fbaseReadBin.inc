!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! include file "file_fbaseReadBin.inc" for the module file_m. Common lines for subroutines:
!      . file_fbaserSpk0ReaderBin
!      . file_fbaserSpk1ReaderBin
!      . file_fbaserSpk2ReaderBin
!
!      . file_fbaserDpk0ReaderBin
!      . file_fbaserDpk1ReaderBin
!      . file_fbaserDpk2ReaderBin
!
!      . file_fbaseI32k0ReaderBin
!      . file_fbaseI32k1ReaderBin
!      . file_fbaseI32k2ReaderBin
!
!      . file_fbaseI64k0ReaderBin
!      . file_fbaseI64k1ReaderBin
!      . file_fbaseI64k2ReaderBin
!---------------------------------------------------------------------------------------------

   integer                       :: ios
   character(len=:), allocatable :: msg

   self%line = self%line + 1 ; read(self%unit,iostat=ios) x

   if ( ios > 0 ) then
      if ( present(what) ) then
         msg = 'while reading '//what
      else
         msg = 'Read error'
      end if
      msg = msg // ' at record #'//i2a(self%line)// ' of the file "'//self%name//'"'   
      stat = err_t ( stat = RERROR, where = HERE, msg = msg )
   else if ( ios < 0 ) then
      stat = err_t ( stat = EOF, where = HERE, &
                      msg = '(end of file "'//self%name//'" reached)')
   end if