!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! include file "msh_readDomainId.inc" for the module msh_m. Common lines for subroutines:
!      . msh_readDomainId32Sp
!      . msh_readDomainId32Dp
!      . msh_readDomainId64Sp
!      . msh_readDomainId64Dp
!---------------------------------------------------------------------------------------------

   if ( present(nCell) ) then
      call self % cellConnect % readDomains (file, length, stat, what = 'Volume Id of cells')

   else if ( present(nFacet) ) then
      call self % facetConnect % readDomains (file, length, stat, what = 'Face Id of facets')
   
   else if ( present(nEdge) ) then
      call self % edgeConnect % readDomains (file, length, stat, what = 'Line Id of edges')

   end if   