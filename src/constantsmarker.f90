!---------------------------------------------------------------------------------------------
! ptovtu, version 2022.7.0
!---------------------------------------------------------------------------------------------
!
! Module name: 
!       constantsmarker_m
!
! Description: 
!       Define and import some useful constants
! 
! Notes: 
!       
!
! Author(s): 
!        R. Hassani, Universite Côte d'Azur
!
! Start date: 
!        06/22
!
! Changes:
!        
!---------------------------------------------------------------------------------------------

MODULE constantsmarker_m
   
!  Imported from pk2 module:
   use pk2mod_m, only: Ikind, i32, i64, & ! kind parameters for integers
                       Rkind, rSp, rDp, & ! kind parameters for reals
                     
                       IERROR, UERROR, RERROR, & ! error codes: internal, user/general, read
                       WARNING, EOF,           & ! code for warning and end-of-file
            
                       IZERO, IONE, RZERO, RONE, ON, OFF, LGSTR, STDOUT, NL, NLT ! constants
   
   ! The F.E. values will be stored in an array of femval_t objects at positions:
   !   #NODV for nodal values,
   !   #ELMV for elemental values,
   !   #CSTV for constant values,
   !   #ELMI for auxiliary elemental values (invariants, eigenvalues, ...)
   
   integer  (Ikind), parameter :: NODV = 1, ELMV = 2, CSTV = 3, ELMI = 4
 
END MODULE constantsmarker_m
