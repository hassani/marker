################################################################################
#                        Makefile for marker 
#-------------------------------------------------------------------------------

define colorecho
      @tput setaf 1
      @echo $1
      @tput sgr0
endef

################################################################################
# Help:
#-------------------------------------------------------------------------------

#h |-------------------------------------------------------------------------|
#h | Usage: make comp = <your compiler>  [opt = "<your options>"] [clean]    |
#h |                                                                         |
#h | where                                                                   |
#h |   . <your compiler>: is the name of your compiler. Note: this version   |
#h |                      was tested with ifort, gfortran and nagfor. For    |
#h |                      another compiler, first adapt this makefile.       |
#h |   . <your options> : is a string corresponding to a set of compilation  |
#h |                      options (default is opt="-O3").                    |
#h |                                                                         |
#h | Examples:                                                               |
#h |   . make                                                                |
#h |        prints this help.                                                |
#h |   . make comp=ifort clean                                               |
#h |        deletes the object files (from the directory obj/ifort),         |
#h |                the module files (from the directory mod/ifort),         |
#h |   . make comp=gfortran                                                  |
#h |        compiles with -O3 optimization flag.                             |
#h |   . make comp=ifort opt="-O3 -fp-model fast=2 -parallel"                |
#h |         compiles with the specified options.                            |
#h |   . make comp=ifort opt=check                                           |
#h |         compiles with checking options (predefined according to one of  |
#h |         the three compilers cited above).                               |
#h |-------------------------------------------------------------------------|

ifeq ($(comp),)
   help:     #// Show this help.
	@sed -ne '/@sed/!s/#h //p' $(MAKEFILE_LIST)
endif



################################################################################
#  Settings:
#-------------------------------------------------------------------------------

#
#  Name of the executable:
#
exec = marker

#
#  Directory of the pk2 library (libpk2.a) and directory of the corresponding
#  module files:
#

dirmodpk2 = $(dirpk2)/mod/$(comp)
dirlibpk2 = $(dirpk2)/lib/$(comp)

#
#  Directory of the calmat library (libcalmat.a) and directory of the corresponding
#  module files:
#

dirmodcalmat = $(dircalmat)/mod/$(comp)
dirlibcalmat = $(dircalmat)/lib/$(comp)

#
#  Blas and Lapack lib:
#
liblapack = -llapack -lblas 

#
#  Directories where the executable, the object files and the module files
#  will be placed:
#
direxe = ../bin/$(comp)
dirmod = ../mod/$(comp)
dirobj = ../obj/$(comp)

absdirexe := $(abspath $(direxe))
#
#  Create these directories if not present:
#
$(shell mkdir -p $(direxe))
$(shell mkdir -p $(dirmod))
$(shell mkdir -p $(dirobj))
 

################################################################################
#  Compilation options:
#-------------------------------------------------------------------------------

ifeq ($(opt),)
#
#  No options given, set them to "-O3":
#   
   opt_optim = -O3 
   opt_check =
else
     
   ifeq ($(opt),check)
#
#     Default options for checking (compiler dependent):
#
      ifeq ($(comp),gfortran)
         opt_optim = -Og
         opt_check = -fcheck=all -fbacktrace -Wall -fimplicit-none
      endif
      ifeq ($(comp),ifort)
         opt_optim = -O0
         opt_check = -check all -traceback -gen-interfaces -warn interfaces
      endif
      ifeq ($(comp),nagfor)
         opt_optim = -O0
         opt_check = -C=all
      endif
   else
#
#     User's options:
#
      opt_optim =$(opt)
      opt_check =
   endif
endif

flags = $(opt_optim) $(opt_check) 
libs =  -L$(dirlibcalmat) -lcalmat -L$(dirlibpk2) -lpk2 $(liblapack) #caution: the order may matter 

cpflags = $(opt_optim) $(opt_check) 

ifeq ($(comp),gfortran)
   flags += -cpp -J $(dirmod) -I $(dirmodpk2) -I $(dirmodcalmat) $(libs) 
endif
ifeq ($(comp),ifort)
   flags += -fpp -module $(dirmod) -I $(dirmodpk2) -I $(dirmodcalmat) $(libs)  #-heap-arrays 200 # -assume realloc-lhs -assume buffered_io
endif
ifeq ($(comp),nagfor)
   flags += -fpp -kind=byte -ieee=full -mdir $(dirmod) -I $(dirmod) -I $(dirmodpk2) -I $(dirmodcalmat) $(libs) 
   cpflags += -kind=byte -ieee=full    
endif

flags += -D__CDATE=\""$(shell date)"\" -D__COPTS=\""$(cpflags)\""


################################################################################
# Compilation:
#-------------------------------------------------------------------------------


src = constantsmarker units file femval conn msh    \
      globalParameters utilmarker  \
      marker main version elm markerForOldPfile

src := $(patsubst %,%.f90,$(src))
obj = $(patsubst %.f90,$(dirobj)/%.o,$(src))

src_fileDep   = file_fbaseReadAscii.inc file_fbaseReadBin.inc
src_femvalDep = femval_alloc.inc femval_getValues.inc
src_mshDep    = msh_alloc.inc msh_readConnect.inc 

fileDep   = $(patsubst %,inc/%,$(src_fileDep))
femvalDep = $(patsubst %,inc/%,$(src_femvalDep))
mshDep    = $(patsubst %,inc/%,$(src_mshDep))

marker: $(obj)
	$(comp) $(obj) $(flags) -o $(direxe)/$(exec)
	@echo " "
	$(call colorecho, "==> Successful compilation of marker")
	@echo " "
	@echo " The executable '$(exec)' is in the directory: $(absdirexe)"
	@echo " (you can move it to the location of your choice)"
	@echo " "

dummy:
	@#to force version.f90 to be always compiled (as it contains compilation date)

$(dirobj)/version.o: version.f90 \
                     dummy
	$(comp) $(flags) -c version.f90 -o $(dirobj)/version.o

$(dirobj)/constantsmarker.o: constantsmarker.f90 
	$(comp) $(flags) -c constantsmarker.f90 -o $(dirobj)/constantsmarker.o

$(dirobj)/units.o: units.f90 \
                    $(dirobj)/constantsmarker.o
	$(comp) $(flags) -c units.f90 -o $(dirobj)/units.o
	
$(dirobj)/file.o: file.f90    \
                  $(fileDep)  \
                  $(dirobj)/constantsmarker.o
	$(comp) $(flags) -c file.f90 -o $(dirobj)/file.o

$(dirobj)/femval.o: femval.f90                  \
                    $(femvalDep)                \
                    $(dirobj)/constantsmarker.o \
                    $(dirobj)/file.o            \
                    $(dirobj)/units.o
	$(comp) $(flags) -c femval.f90 -o $(dirobj)/femval.o

$(dirobj)/conn.o: conn.f90                     \
                  $(dirobj)/constantsmarker.o  \
                  $(dirobj)/file.o
	$(comp) $(flags) -c conn.f90 -o $(dirobj)/conn.o

$(dirobj)/msh.o: msh.f90                      \
                 $(mshDep)                    \
                 $(dirobj)/constantsmarker.o  \
                 $(dirobj)/file.o             \
                 $(dirobj)/conn.o
	$(comp) $(flags) -c msh.f90 -o $(dirobj)/msh.o
		
$(dirobj)/globalParameters.o: globalParameters.f90        \
                              $(deps)                     \
                              $(dirobj)/constantsmarker.o \
                              $(dirobj)/units.o           \
                              $(dirobj)/file.o            \
                              $(dirobj)/femval.o          \
                              $(dirobj)/elm.o             \
                              $(dirobj)/msh.o 
	$(comp) $(flags) -c globalParameters.f90 -o $(dirobj)/globalParameters.o

$(dirobj)/elm.o: elm.f90                
	$(comp) $(flags) -c elm.f90 -o $(dirobj)/elm.o

$(dirobj)/utilmarker.o: utilmarker.f90                \
                        $(dirobj)/globalParameters.o
	$(comp) $(flags) -c utilmarker.f90 -o $(dirobj)/utilmarker.o

$(dirobj)/markerForOldPfile.o: markerForOldPfile.f90  \
                    $(dirobj)/globalParameters.o  \
                    $(dirobj)/utilmarker.o
	$(comp) $(flags) -c markerForOldPfile.f90 -o $(dirobj)/markerForOldPfile.o

$(dirobj)/marker.o: marker.f90                    \
                    $(dirobj)/globalParameters.o  \
                    $(dirobj)/markerForOldPfile.o \
                    $(dirobj)/utilmarker.o
	$(comp) $(flags) -c marker.f90 -o $(dirobj)/marker.o

$(dirobj)/main.o: main.f90            \
                  $(dirobj)/marker.o  \
                  $(dirobj)/version.o
	$(comp) $(flags) -c main.f90 -o $(dirobj)/main.o

	
################################################################################
# Cleaning:
#-------------------------------------------------------------------------------

clean: 
	/bin/rm -f $(dirobj)/*.o $(dirmod)/*.mod *.o *.mod
	
